#ifndef __DRV_MY_OPENMV_H
#define __DRV_MY_OPENMV_H

//==引用
#include "SysConfig.h"

/*
	** OPMV识别
		STA = 0xFF   P点
		      0x01   蓝色圆
				0x02   红色圆
		      0x03   蓝色三角形
				0x04   红色三角形
				0x05   蓝色正方形
				0x06   红色正方形				
*/

#define  P_stop             0xFF
#define  Blue_Circle        0x01
#define  Red_Circle         0x02
#define  Blue_Triangle      0x03
#define  Red_Triangle       0x04
#define  Blue_Square        0x05
#define  Red_Square         0x06

typedef struct
{
	//
	u8 color_flag;
	u8 sta;
	s16 pos_x;
	s16 pos_y;
	u8 dT_ms;

}_openmv_color_block_st;

typedef struct
{
	//
	u8 sta;	
	u8 angle;
	u8 deviation;
	u8 pos_x;
	u8 pos_y;
	u8 p_flag;
	u8 dT_ms;

}_openmv_line_tracking_st;

typedef struct
{
	s16 openmv_data[100];
	s16 openmv_sta;
	
	u8 posX;
	u8 posY;
	u8 posX_A;
	u8 posY_A;
	u8 state;
	u8 state_A;
	u8 angle;
	u8 angle_A;
	u8 angle_A_out;
	u8 angle_out;
	
	u8 offline;
	u8 mode_cmd;
	u8 mode_sta;
	
	u8 en;
	u8 height_flag;
	u8 reset_flag;
	
	_openmv_color_block_st cb;
	_openmv_line_tracking_st lt;
} openmv_st;

extern openmv_st openmv;

void OPENMV_GetOneByte(uint8_t data);
static void OPENMV_DataAnl(uint8_t *data, uint8_t len1);

#endif

