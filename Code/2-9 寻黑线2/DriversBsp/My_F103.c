#include "My_F103.h"

//全局变量
static uint8_t _datatemp[50];

void F407_Send(u8 a,s16 b,float c)
{
	u8 DateSendBuffer[100];
	u8 cnt=0;
	u8 sumcheck = 0;
	u8 addcheck = 0;
	
	DateSendBuffer[cnt++]=0xBB;
	DateSendBuffer[cnt++]=0xFF;
	DateSendBuffer[cnt++]=7;
	
	DateSendBuffer[cnt++]=a;
	
	DateSendBuffer[cnt++]=BYTE0(b);
	DateSendBuffer[cnt++]=BYTE1(b);
	
	DateSendBuffer[cnt++]=BYTE0(c);
	DateSendBuffer[cnt++]=BYTE1(c);
	DateSendBuffer[cnt++]=BYTE2(c);
	DateSendBuffer[cnt++]=BYTE3(c);
	
	for(u8 i=0; i < DateSendBuffer[2]+3; i++)
	{
		sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
	}
	
	DateSendBuffer[cnt++]=sumcheck;
	DateSendBuffer[cnt++]=addcheck;
				
	DrvUart2SendBuf(DateSendBuffer,cnt);
}

void F407_Receive(u8 data)
{
	static u8 _data_len = 0, _data_cnt = 0;
	static u8 rxstate = 0;

	if (rxstate == 0 && data == 0xBB)                        //帧头0XBB
	{
		rxstate = 1;
		_datatemp[0] = data;
	}
	else if (rxstate == 1 && data == 0XFF)   //OXFF
	{
		rxstate = 2;
		_datatemp[1] = data;
	}
	else if (rxstate == 2 && data < 250)                   //数据长度
	{
		rxstate = 3;
		_datatemp[2] = data;
		_data_len = data;
		_data_cnt = 0;
	}
	else if (rxstate == 3 && _data_len > 0)
	{
		_data_len--;
		_datatemp[3 + _data_cnt++] = data;                   //有效数据接收
		if (_data_len == 0)
			rxstate = 4;
	}
	else if (rxstate == 4)                              //和校验
	{
		rxstate = 5;
		_datatemp[3 + _data_cnt++] = data;
	}
	else if (rxstate == 5)                              //和校验
	{
		rxstate = 0;
		_datatemp[3 + _data_cnt] = data;
		Usart2_DataAnl(_datatemp, _data_cnt + 4); //
	}
	else
	{
		rxstate = 0;
	}	
}



static void Usart2_DataAnl(uint8_t *data, uint8_t len1)     
{
	u8 Usart2_a;
	s16 Usart2_b;
	float Usart2_c;
	u8 check_sum3 = 0, check_sum4 = 0;
	if (*(data + 2) != (len1 - 5 )) //判断数据长度是否正确
		return;
	for (u8 i = 0; i < len1 - 2; i++)
	{
		check_sum3 += *(data + i);
		check_sum4 += check_sum3;
	}
	if ((check_sum3 != *(data + len1 - 2)) || (check_sum4 != *(data + len1 - 1))) //判断sum校验
		return;
	//================================================================================	
		 Usart2_a = *(data+3);
		 Usart2_b = *(data + 4 + 1)<<8 + *(data + 4 + 0);
	    Usart2_c =((*(data + 6 + 3))<<24) + ((*(data + 6 + 2))<<16) + ((*(data + 6 + 1))<<8) + (*(data + 6 + 0));		
}

