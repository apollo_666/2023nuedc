# Untitled - By: Best.jiejie - 周一 7月 24 2023

import sensor, image, time, math, sys
from pyb import Pin, Timer, LED, UART
from image import SEARCH_EX, SEARCH_DS
from pid import PID
rho_pid = PID(p=0.4, i=0)
theta_pid = PID(p=0.001, i=0)

Line_Thr = 1000
Line_Theta_margin = 25
Line_Margin = 25
Line_Min_degree = 0
Line_Max_degree = 179
clock = time.clock()
a = 0
# 配置摄像头
right_angle_threshold = (70, 90)


def find_all_Line(img):
    Line_All = img.find_lines(threshold = 2000, theta_margin = Line_Theta_margin, rho_margin = Line_Margin)
    return Line_All

def calculate_angle(line1, line2):
    # 利用四边形的角公式， 计算出直线夹角
    angle  = (180 - abs(line1.theta() - line2.theta()))
    if angle > 90:
        angle = 180 - angle
    return angle


def is_right_angle(line1, line2):
    global right_angle_threshold
    # 判断两个直线之间的夹角是否为直角
    angle = calculate_angle(line1, line2)

    if angle >= right_angle_threshold[0] and angle <=  right_angle_threshold[1]:
        # 判断在阈值范围内
        return True
    return False
def calculate_intersection(line1, line2):
    # 计算两条线的交点
    a1 = line1.y2() - line1.y1()
    b1 = line1.x1() - line1.x2()
    c1 = line1.x2()*line1.y1() - line1.x1()*line1.y2()

    a2 = line2.y2() - line2.y1()
    b2 = line2.x1() - line2.x2()
    c2 = line2.x2() * line2.y1() - line2.x1()*line2.y2()

    if (a1 * b2 - a2 * b1) != 0 and (a2 * b1 - a1 * b2) != 0:
        cross_x = int((b1*c2-b2*c1)/(a1*b2-a2*b1))
        cross_y = int((c1*a2-c2*a1)/(a1*b2-a2*b1))
        return (cross_x, cross_y)
    return (-1, -1)
def line_info_process(lines):
    Cross_X = -1
    Cross_Y = -1
    line_info_list = [0,0,0,0,0,0,0]
    line_info_list[0] = -1

    # 如果有两条直线，此处不严谨，应该多判定两条直线
    if len(lines) == 2:
        if is_right_angle(lines[0],lines[1]):
            Cross_X , Cross_Y = calculate_intersection(lines[0],lines[1])
            line_info_list[0] = 2
            line_info_list[1] = Cross_X
            line_info_list[2] = Cross_Y
    return line_info_list
# 设置阈值
THRESHOLD = (0, 34)                # 设置阈值范围，在此例中将黑色设置为0-64之间的灰度值

uart_buf1 = bytearray([0xAA,0xFF, 0XBB, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
uart_buf2 = bytearray([0xAA,0xFF, 0XBB, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])

uart = UART(1,500000)
uart.init(500000, bits=8, parity=None, stop=1)

def uartsend(timer):
    uart.write(uart_buf1)
    print(uart_buf1)

tim = Timer(2, freq=10)
tim.callback(uartsend)

sensor.reset()                      # 复位摄像头
sensor.set_pixformat(sensor.RGB565 )   # 设置图像为灰度模式
sensor.set_framesize(sensor.QQQVGA)      # 设置图像分辨率为160x120
sensor.skip_frames(30)             # 跳过10帧以使摄像头稳定
sensor.set_auto_gain(False) # must be turned off for color tracking
sensor.set_auto_whitebal(False) # must be turned off for color tracking

while True:
    clock.tick()
    theta_err = 0
    rho_err = 0
    img = sensor.snapshot().binary([THRESHOLD])
    All_Line = find_all_Line(img)
    #返回线的类型，与坐标
    Line_Info = line_info_process(All_Line)
    #print(Line_Info[0])
    line = img.get_regression([(100,100)], robust = True)
    if (line):
        rho_err = int(abs(line.rho())-img.width()/2)
        theta_err = line.theta()
        img.draw_line(line.line(), color = 127)
        print(rho_err,line.magnitude(),theta_err)
        uart_buf1 = bytearray([0xAA,0xFF, 0XBB, 0x00, Line_Info[0], theta_err, rho_err, Line_Info[1], Line_Info[2], 0x00,0x00])


    if Line_Info[0] == 1:
            img.draw_circle(Line_Info[1],Line_Info[2], 3, color = 200, thickness = 2, fill = False)
            uart_buf2 = bytearray([0xAA,0xFF, 0XBB, 0x00, Line_Info[0], theta_err, rho_err, Line_Info[1], Line_Info[2], 0x00, 0x00])
            #uart.write(uart_buf1)
            a = 1
            print(uart_buf1)
            print("x:",Line_Info[1],"y:",Line_Info[2])
    lens = len(uart_buf1)#数据包大小
    uart_buf1[3] = lens-6;#有效数据个数
    i = 0
    sum = 0
    sum1 = 0
    #和校验
    while i<(lens-2):
        sum = sum + uart_buf1[i]
        sum1 = sum1 + sum
        i = i+1
    uart_buf1[lens-2] = sum;
    uart_buf1[lens-1] = sum1;
