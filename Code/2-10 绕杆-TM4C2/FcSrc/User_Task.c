#include "User_Task.h"
#include "Drv_RcIn.h"
#include "LX_FC_Fun.h"
#include "Ano_Math.h"
#include "Drv_Uart.h"
#include "Drv_MY_OpenMv.h"
#include "Drv_UP_OpenMv.h"
#include "Math.h"
#include "My_F103.h"
 
 
s16 cal_X,cal_Y;
s16 cal_X_2,cal_Y_2;
s16 cal_X_3,cal_Y_3;

Speed speed;
u8 confirm_cnt; //重复标志位
u8 tan_flag=0;
u8 offset_Distance;//补偿角度
u8 cmd_send_state = 0;/*实时控制帧确认发送标志位*/ 


s16 Second_Distance;
s16 Second_angle;

s16 CMDvalue[4]; //实时控制帧x y z deg发送数据 

s16 PLANE_YAW;   //飞机当前航向角,每100ms自动获取数值
s16 yaw_start;
s16 yaw_target[2] = 0;
u8 yaw_target_cnt = 0;


s32 Ano_Alt;   //飞机对地高度,自动获取数值,单位cm
s32 Ano_TOBoard_distiance=100;   //飞机对地高度,自动获取数值,单位cm
s32 POS_X,POS_Y;
s16 angle_bis;     //角度补偿的角度误差

s16 Dcx = 0,Dcy = 0,Dcx_A = 0,Dcy_A = 0; //通用OPENMV返回坐标XY偏差值
s16 Pole_Dcx = 0;  //OPMV杆子误差
s16 Pole_Dcy = 0;  //OPMV杆子误差
s16 Pole_Dcz = 0;  //OPMV杆子误差
u8 check_flag_1;    //case7 标志位
u8 check_flag_2;    //case6 标志位 
s16 check_point_1;
s16 check_point_2;
s16 check_point_3;
float rate;

u8 Num_1=3;
u8 Num_2=10;

                   // 0    1    2    3    4    5    6    7    8    9   10   11   12 
s16 position_X[13] = {0,  50, 200, 275, 350, 350, 275, 125, 125,  50, 200, 200, 350};           //目标点 与起点 X轴距离  
s16 position_Y[13] = {0, 275, 125, 200, -25, 275,  50,  50, 200, 125, -25, 275, 125};           //目标点 与起点 y轴距离



	                  // 0    1    2    3*   4    5    6    7    8    9*  10   11   12 
s16 position_dis[13] = {0, 280, 236, 265-15, 351, 445, 279, 135, 236, 125, 202, 340, 371};         //目标点 与起点距离  0表示起点
s16 position_ang[13] = {0,  10,  58,  64,  94,  52,  80,  68,  32,  22,  97,  36,  70};         //目标点 与起点角度




							 //       0    1    2    3*     4     5    6    7    8    9*  10     11   12 
s16 position_dis_GoHome[13] = {0, 280, 236, 280-30, 351, 445, 279, 135, 236, 125, 202-2, 340, 371};         //目标点 与起点距离  0表示起点
s16 position_ang_GoHome[13] = {0,  10+180,  58+180-2,  64+180,  94+180,  52+180,  80+180,  68+180,  32+180,  22+180,  97+180-6,  36+180,  70+180};         //目标点 与起点角度
							 //		 0       1        2    		3*  	 4    		5   	  6        7        8        9*      10        11       12 

	
void UserTask_OneKeyCmd(void)
{
    //////////////////////////////////////////////////////////////////////
	static u8 one_key_takeoff_f = 1, one_key_land_f = 1, one_key_mission_f = 0;  //确认次数
	static int case7_time=0;

    //判断有遥控信号才执行
    if (rc_in.no_signal == 0)
    {
        //判断第5通道拨杆位置 800<CH_6<1200
        if (rc_in.rc_ch.st_data.ch_[ch_5_aux1] > 800 && rc_in.rc_ch.st_data.ch_[ch_5_aux1] < 1200)
        {
            //还没有执行
            if (one_key_land_f == 0)
            {
                //标记已经执行
                one_key_land_f =
                    //执行一键降落
                    OneKey_Land();
            }
        }
        else
        {
            //复位标记，以便再次执行
            one_key_land_f = 0;
        }
		  	  
		  
		  
		  
	//判断第5通道拨杆位置 1700<CH_6<2200 
		if(rc_in.rc_ch.st_data.ch_[ch_5_aux1]>1700 && rc_in.rc_ch.st_data.ch_[ch_5_aux1]<2200)
		{
			//还没有执行
			if(one_key_mission_f ==0)
			{
				//标记已经执行
				one_key_mission_f = 1;
				//开始流程
				mission_step = 0;
			}
		}
		else
		{
			//复位标记，以便再次执行
			one_key_mission_f = 0;		
		}
		//
		if(one_key_mission_f==1)
		{
			static u16 time_dly_cnt_ms;
			//
			switch(mission_step)
			{
				case 0:
				{
					//reset
					time_dly_cnt_ms = 0;
					mission_step++;
			
				}
				break;
				
				case 1:
				{
					mission_step++;
						confirm_cnt=0;
				}
				break;
				
				case 2:
				{
					//解锁
					mission_step += FC_Unlock();
					
				}
				break;
				
				case 3:
				{
					//等5秒
					if(time_dly_cnt_ms<5000)
					{
						time_dly_cnt_ms+=20;//ms
					}
					else
					{
						time_dly_cnt_ms = 0;
						mission_step += 1;
					}
				}
				break;
				
				case 4:
				{
					//起飞
					mission_step += OneKey_Takeoff(85);//参数单位：厘米； 0：默认上位机设置的高度。
				}
				break;
				
				case 5:
				{
					//等5秒
					if(time_dly_cnt_ms<5000)
					{
						time_dly_cnt_ms+=20;//ms
					}
					else
					{
						time_dly_cnt_ms = 0;
						mission_step += 1;						
					}			
				}
				break;
				
					
	
				case 6:  //OPMV 误差弥补
				{
					time_dly_cnt_ms += 20;
					
					if(openmv_up.pole_state==1)
					{
						float PIXpcm,PIYpcm;
						s16 Distance_to_cross;
						if( Ano_TOBoard_distiance<155 && Ano_TOBoard_distiance>145 )  
							{
								PIXpcm = 2.0f;PIYpcm=1.66f;
							}						
						else if(Ano_TOBoard_distiance<155&&Ano_TOBoard_distiance>170)
							{
								PIXpcm = 2.4f;PIYpcm=1.82f;
							}						
						else if(Ano_TOBoard_distiance>130&&Ano_TOBoard_distiance<145)
							{
								PIXpcm = 1.62f;PIYpcm=1.50f;
							}
						
						else if(Ano_TOBoard_distiance>90&&Ano_TOBoard_distiance<130)
							{
								PIXpcm = 1.2f;PIYpcm=1.0f;
							}
						else 
							{
								PIXpcm = 0.5f;PIYpcm = 0.4f;
							}
							frame_send_16(Pole_Dcx,0xf7);
							frame_send_16(Pole_Dcy,0xf8);
						if((Pole_Dcx<-6||Pole_Dcx>6)||(Pole_Dcz<-6||Pole_Dcz>6))       
							{
								
								if(time_dly_cnt_ms % 100 == 0)
								{	
										if(Pole_Dcx>0)
										{
											Horizontal_Move((Pole_Dcx*PIXpcm),10,90);  
										}
										else
										{
											Horizontal_Move((Pole_Dcx*PIXpcm),10,268);
										}
								}
								if(time_dly_cnt_ms % 200 == 0)	
										{
											Vertical_(-(Pole_Dcz*PIYpcm),10);
										}
								}
							}
						else if( (Pole_Dcx>-6 && Pole_Dcx < 6 ) && (Pole_Dcz > -6 && Pole_Dcz < 6) &&openmv_up.pole_state==1)   
						{
							mission_step = 18;
							Horizontal_Move(0,0,0);
							time_dly_cnt_ms = 0;
						}
						if(time_dly_cnt_ms > 20000)
					{
						mission_step = 18;
						Horizontal_Move(0,0,0);
						time_dly_cnt_ms = 0;
					}
					}								
				break;
				
				case 18:
				{
					//等2秒
					if(time_dly_cnt_ms<4000)
					{
						time_dly_cnt_ms+=20;//ms
					}
					else
					{
						time_dly_cnt_ms = 0;
						mission_step ++;
					}
				}
				break;
				
				case 19:
				{
					//执行一键降落
					OneKey_Land();	
				}
				break;				
				default:break;
			}
		}
		else
		{
			mission_step = 0;
		}
	}
}
 
//辅助函数


//向上位机发送数据
void frame_send_8(u8 a,u8 num)
{
u8 DateSendBuffer[100];
u8 cnt=0;
u8 sumcheck = 0;
u8 addcheck = 0;
		DateSendBuffer[cnt++]=0xAA;
		DateSendBuffer[cnt++]=0xFF;
		DateSendBuffer[cnt++]=num;
		DateSendBuffer[cnt++]=1;
		DateSendBuffer[cnt++]=a;
for(u8 i=0; i < (DateSendBuffer[3]+4); i++)
{
sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
}
		DateSendBuffer[cnt++]=sumcheck;
		DateSendBuffer[cnt++]=addcheck;
	
	
DrvUart5SendBuf(DateSendBuffer,cnt);
}

void frame_send_16(s16 a,u8 num)
{
u8 DateSendBuffer[100];
u8 cnt=0;
u8 sumcheck = 0;
u8 addcheck = 0;
		DateSendBuffer[cnt++]=0xAA;
		DateSendBuffer[cnt++]=0xFF;
		DateSendBuffer[cnt++]=num;
		DateSendBuffer[cnt++]=2;
		DateSendBuffer[cnt++]=BYTE0(a);
	   DateSendBuffer[cnt++]=BYTE1(a);
	

for(u8 i=0; i < (DateSendBuffer[3]+4); i++)
{
sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
}
		DateSendBuffer[cnt++]=sumcheck;
		DateSendBuffer[cnt++]=addcheck;
	
	
DrvUart5SendBuf(DateSendBuffer,cnt);
}

void frame_send_float(float a,u8 num)
{
	u8 DateSendBuffer[100];
	u8 cnt=0;
	u8 sumcheck = 0;
	u8 addcheck = 0;
			DateSendBuffer[cnt++]=0xAA;
			DateSendBuffer[cnt++]=0xFF;
			DateSendBuffer[cnt++]=num;
			DateSendBuffer[cnt++]=4;
			DateSendBuffer[cnt++]=BYTE0(a);
			DateSendBuffer[cnt++]=BYTE1(a);
			DateSendBuffer[cnt++]=BYTE2(a);
			DateSendBuffer[cnt++]=BYTE3(a);
		

	for(u8 i=0; i < (DateSendBuffer[3]+4); i++)
	{
	sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
	addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
	}
			DateSendBuffer[cnt++]=sumcheck;
			DateSendBuffer[cnt++]=addcheck;
		
		
	DrvUart5SendBuf(DateSendBuffer,cnt);
}

void frame_send_32(s32 a,u8 num)
{
	u8 DateSendBuffer[100];
	u8 cnt=0;
	u8 sumcheck = 0;
	u8 addcheck = 0;
	
	DateSendBuffer[cnt++]=0xAA;
	DateSendBuffer[cnt++]=0xFF;
	DateSendBuffer[cnt++]=num;
	DateSendBuffer[cnt++]=4;
	DateSendBuffer[cnt++]=BYTE0(a);
	DateSendBuffer[cnt++]=BYTE1(a);
	DateSendBuffer[cnt++]=BYTE2(a);
	DateSendBuffer[cnt++]=BYTE3(a);
	
	for(u8 i=0; i < (DateSendBuffer[3]+4); i++)
	{
		sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
	}
	
	DateSendBuffer[cnt++]=sumcheck;
	DateSendBuffer[cnt++]=addcheck;

   DrvUart5SendBuf(DateSendBuffer,cnt);
}

//OPMV 下视 数据 发送上位机
void Userdata_send_OPENMV()
{
	static u8 User_send_buffer[20];
	u8 _cnt = 0;
	User_send_buffer[_cnt++] = 0XAA;
	User_send_buffer[_cnt++] = 0XFF;
	if(openmv.mode_sta==1)
	{
//		User_send_buffer[_cnt++] = 0Xf2;
//		User_send_buffer[_cnt++] = 0;
//		User_send_buffer[_cnt++] = openmv.mode_sta;
//		User_send_buffer[_cnt++] = openmv.cb.color_flag;
//		User_send_buffer[_cnt++] = openmv.cb.sta;
//		User_send_buffer[_cnt++] = BYTE0(openmv.cb.pos_x);
//		User_send_buffer[_cnt++] = BYTE1(openmv.cb.pos_x);
//		User_send_buffer[_cnt++] = BYTE0(openmv.cb.pos_y);
//		User_send_buffer[_cnt++] = BYTE1(openmv.cb.pos_y);
//		User_send_buffer[_cnt++] = openmv.cb.dT_ms;
//		User_send_buffer[3] =  _cnt - 4;
	}
	else if(openmv.mode_sta==2)
	{
		User_send_buffer[_cnt++] = 0XF3;
		User_send_buffer[_cnt++] = 0;
		User_send_buffer[_cnt++] = openmv.lt.sta;
		User_send_buffer[_cnt++] = openmv.lt.angle;
		User_send_buffer[_cnt++] = openmv.lt.deviation;
		User_send_buffer[_cnt++] = openmv.lt.pos_x;
		User_send_buffer[_cnt++] = openmv.lt.pos_y;
		User_send_buffer[3] =  _cnt - 4;
		
		if(openmv.lt.sta==2)//直角
		{
			Dcx = openmv.lt.pos_x-35;
			Dcy = openmv.lt.pos_y-35;						
		}
	}
	
	else if(openmv.mode_sta==3)
	{
		User_send_buffer[_cnt++] = 0XF4;
		User_send_buffer[_cnt++] = 0;
		User_send_buffer[_cnt++] = openmv.state;
		User_send_buffer[_cnt++] = openmv.posX;
		User_send_buffer[_cnt++] = openmv.posY;
		User_send_buffer[_cnt++] = openmv.angle;
		
		if (openmv.state == 255) //P点
		{
			Dcx = openmv.posX - (80+5);                       
			Dcy = openmv.posY - (70-10);					
		}
		
		
		else                      //其他点
		{
			Dcx = openmv.posX - (70+5);                       
			Dcy = openmv.posY - (30-3);
		}
		
		if(Dcx>=0&&Dcy<=0)openmv.angle_out = openmv.angle;
		else if(Dcx>0&&Dcy>0)openmv.angle_out = 180 - openmv.angle;
		else if(Dcx<0&&Dcy>0)openmv.angle_out = openmv.angle + 180;
		else if(Dcx<0&&Dcy<0)openmv.angle_out = 360 - openmv.angle;	   
		
		User_send_buffer[_cnt++] = BYTE0(openmv.angle_out);
		User_send_buffer[_cnt++] = BYTE1(openmv.angle_out);
		
		User_send_buffer[3] =  _cnt - 4;
	}
//校验
	u8 check_sum1 = 0, check_sum2 = 0;
	for (u8 i = 0; i < _cnt; i++)
	{
		check_sum1 += User_send_buffer[i];
		check_sum2 += check_sum1;
	}
	User_send_buffer[_cnt++] =  check_sum1;
	User_send_buffer[_cnt++] =  check_sum2;
	
	//
	DrvUart5SendBuf(User_send_buffer,_cnt);		
}

/* 
	OPMV前视 数据上传上位机
*/
void Userdata_send_OPENMV_UP()
{
	static u8 User_send_buffer[20];
	u8 _cnt = 0;
	User_send_buffer[_cnt++] = 0XAA;
	User_send_buffer[_cnt++] = 0XFF;
	if(openmv_up.up_sta==1)
	{
		User_send_buffer[_cnt++] = 0Xf5;
		User_send_buffer[_cnt++] = 0;
		User_send_buffer[_cnt++] = openmv_up.pole_state;                              
		User_send_buffer[_cnt++] = openmv_up.pole_posX;
		User_send_buffer[_cnt++] = openmv_up.pole_posY ;
		User_send_buffer[_cnt++] = openmv_up.SR04_Distance;
		User_send_buffer[_cnt++] = openmv_up.pole_areo;
		User_send_buffer[_cnt++] = BYTE0(Pole_Dcx);
		User_send_buffer[_cnt++] = BYTE1(Pole_Dcx);
		User_send_buffer[_cnt++] = BYTE0(Pole_Dcz);
		User_send_buffer[_cnt++] = BYTE1(Pole_Dcz);
		
		User_send_buffer[3] =  _cnt - 4;
	}
	if (openmv_up.pole_state == 1)
	{
			Pole_Dcx = openmv_up.pole_posX - 80;
			Pole_Dcz = openmv_up.pole_posY - 60;
//			Ano_TOBoard_distiance=openmv_up.SR04_Distance;
	}
//校验
	u8 check_sum1 = 0, check_sum2 = 0;
	for (u8 i = 0; i < _cnt; i++)
	{
		check_sum1 += User_send_buffer[i];
		check_sum2 += check_sum1;
	}
	User_send_buffer[_cnt++] =  check_sum1;
	User_send_buffer[_cnt++] =  check_sum2;
	
	//
	DrvUart5SendBuf(User_send_buffer,_cnt);		
}	
/* 
	下视
	位置误差弥补函数
   输出：弥补距离--offset_Distance
			弥补角度--offset_Angle

*/
void offset_error_xy(s16 target_x,s16 target_y,s16 real_x,s16 real_y)
{	
	s16 error_x,error_y;
	error_x = (real_x - target_x);
	error_y = (real_y - target_y);
	offset_Distance = (u16) (my_sqrt((error_x*error_x)+(error_y*error_y)));
	if(error_x>0&&error_y>0)//1象限
	{
	offset_Angle = 360-fast_atan2(ABS(error_y),ABS(error_x));
	}
	else	if(error_x<0&&error_y>0)//2象限
	{
	offset_Angle = 360-(180-fast_atan2(ABS(error_y),ABS(error_x)));
	}
	else	if(error_x<0&&error_y<0)//3象限
	{
	offset_Angle =360-(180+fast_atan2(ABS(error_y),ABS(error_x)));
	}
	else	if(error_x>0&&error_y<0)//4象限
	{
	offset_Angle = fast_atan2(ABS(error_y),ABS(error_x));
	}
}


/*
	Second_angle角度计算
	return：Second_angle
   XY坐标系（非机头坐标系）
*/

s16 Angle_cal(s16 bis_X,s16 bis_Y,u8 xiangxian)
{

	s16 angle;
	rate = ABS(bis_X)*100/ABS(bis_Y);
	if (rate == 300)                    //3:1
	{
		switch (xiangxian)
		{
			case 1:
				angle = 72;
				break;
			
			case 2:
				angle = 360 - 72;
				break;
			
			case 3:
				angle = 180 + 72;
				break;
			
			case 4:
				angle = 180 - 72;
				break;
			default:
				break;
		}
	}
	else if (rate == 200)                     //2:1
	{
		switch (xiangxian)
		{
			case 1:
				angle = 63;
				break;
			
			case 2:
				angle = 360 - 63;
				break;
			
			case 3:
				angle = 180 + 63;
				break;
			
			case 4:
				angle = 180 - 63;
				break;
			default:
				break;
		}
	}
	else if (rate == 100)                //1:1
	{
		switch (xiangxian)
		{
			case 1:
				angle = 45;
				break;
			
			case 2:
				angle = 360 - 45;
				break;
			
			case 3:
				angle = 180 + 45;
				break;
			
			case 4:
				angle = 180 - 45;
				break;
			default:
				break;
		}
	}
	else if (rate == 50)                  //  1:2
	{
		switch (xiangxian)
		{
			case 1:
				angle = 27;
				break;
			
			case 2:
				angle = 360 - 27;
				break;
			
			case 3:
				angle = 180 + 27;
				break;
			
			case 4:
				angle = 180 - 27;
				break;
			default:
				break;
		}
	}
	else if (rate < 40 && rate > 0)                //1:3
	{
		switch (xiangxian)
		{
			case 1:
				angle = 18;
				break;
			
			case 2:
				angle = 360 - 18;
				break;
			
			case 3:
				angle = 180 + 18-5;
				break;
			
			case 4:
				angle = 180 - 18;
				break;
			default:
				break;
		}
	}
	
	return angle;	
}

/*
	象限分析
	可以得出 Second_angle，Second_Distance
*/
//花椒算法 撰写人：
void Quadrant_anl(u8 num_1,u8 num_2)   
{
	s16 bis_X,bis_Y;
	u8 xiangxian;
	s16 distance;

	
	bis_X = position_X[num_2] - position_X[num_1];
	bis_Y = position_Y[num_2] - position_Y[num_1];
	
	
	if(bis_X>0 && bis_Y>0)
	{
		xiangxian=1;
		Second_angle = Angle_cal(bis_X,bis_Y,xiangxian);
	}
	else if(bis_X<0 && bis_Y>0)
	{
		xiangxian=2;
		Second_angle = Angle_cal(bis_X,bis_Y,xiangxian);
	}
	else if(bis_X<0 && bis_Y<0)
	{
		xiangxian=3;
		Second_angle = Angle_cal(bis_X,bis_Y,xiangxian);
	}
	else if(bis_X>0 && bis_Y<0)
	{
		xiangxian=4;
		Second_angle = Angle_cal(bis_X,bis_Y,xiangxian);
	}
	else if(bis_X>0 && bis_Y==0)
	{
		xiangxian=5;
		Second_angle = 90;
	}
	else if(bis_X==0 && bis_Y>0)
	{
		xiangxian=6;
		Second_angle = 0;
	}
	else if(bis_X<0 && bis_Y==0)
	{
		xiangxian=7;
		Second_angle = 270;
	}
	else if(bis_X==0 && bis_Y<0)
	{
		xiangxian=8;
		Second_angle = 180;
	}
	
	distance = my_sqrt((bis_X*bis_X)+(bis_Y*bis_Y));
	
	if(distance < 116 && distance > 96)
	{
		Second_Distance=106;
	}
	else if(distance<160&&distance>140)
	{
		Second_Distance=150;
	}
		else if(distance<222&&distance>202)
	{
		Second_Distance=212;
	}
		else if(distance<247&&distance>210 )     //例如： 3 -> 10  
	{
		Second_Distance=237 - 37-5;
	}
		else if(distance<310&&distance>290)
	{
		Second_Distance=300;
	}
		else if(distance<328&&distance>308)
	{
		Second_Distance=318;
	}
		else if(distance<345&&distance>325)
	{
		Second_Distance=335;
	}
		else if(distance<434&&distance>414)
	{
		Second_Distance=424;
	}
	frame_send_16(Second_Distance,0XF1);
}

s16 target_yaw_cal(s16 yaw_start,s16 angle)
{
	s16 yaw_target;
	if (yaw_start - angle >= 0) 
	{
		yaw_target = yaw_start - angle;
	}
	else 
	{
		yaw_target = yaw_start - angle + 360;
	}
	return yaw_target;
}
