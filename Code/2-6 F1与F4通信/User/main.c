#include "stm32f10x.h"                  // Device header
#include "Delay.h"
#include "OLED.h"
#include "Serial.h"
#include "LED.h"
#include "string.h"
#include "Timer.h"

u8 Usart2_a;
s16 Usart2_b;
float Usart2_c;

int main(void)
{
	OLED_Init();
	LED_Init();
	Serial_Init();
	Timer_Init();
	
	while (1)
	{
		if (Usart2_a == 1)
		{
			LED3_ON();
		}
		else if (Usart2_a == 0)
		{
			LED3_OFF();
		}
	}
}
