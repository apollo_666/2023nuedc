#ifndef __MY_F407_H
#define __MY_F407_H

#define BYTE0(dwTemp) (*((char *)(&dwTemp)))
#define BYTE1(dwTemp) (*((char *)(&dwTemp) + 1))
#define BYTE2(dwTemp) (*((char *)(&dwTemp) + 2))
#define BYTE3(dwTemp) (*((char *)(&dwTemp) + 3))

//==����
#include "stm32f10x.h" 
#include "Serial.h"

extern u8 Usart2_a;
extern s16 Usart2_b;
extern float Usart2_c;

void F103_Send(u8 a,s16 b,float c);
void F103_Receive(u8 data);
static void Usart1_DataAnl(uint8_t *data, uint8_t len1);

#endif

