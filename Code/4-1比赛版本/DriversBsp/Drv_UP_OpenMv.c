#include "Drv_UP_OpenMv.h"

openmv_st openmv_up;

//全局变量
static uint8_t _datatemp[50];

//	串口2 OPENMV_UP
void OPENMV_UP_GetOneByte(uint8_t data)
{
	static u8 _data_len = 0, _data_cnt = 0;
	static u8 rxstate = 0;

	if (rxstate == 0 && data == 0xAA)
	{
		rxstate = 1;
		_datatemp[0] = data;
	}
	else if (rxstate == 1 && data == HW_ALL)   //OXFF
	{
		rxstate = 2;
		_datatemp[1] = data;
	}
	else if (rxstate == 2)
	{
		rxstate = 3;
		_datatemp[2] = data;
	}
	else if (rxstate == 3 && data < 250)
	{
		rxstate = 4;
		_datatemp[3] = data;
		_data_len = data;
		_data_cnt = 0;
	}
	else if (rxstate == 4 && _data_len > 0)
	{
		_data_len--;
		_datatemp[4 + _data_cnt++] = data;
		if (_data_len == 0)
			rxstate = 5;
	}
	else if (rxstate == 5)
	{
		rxstate = 6;
		_datatemp[4 + _data_cnt++] = data;
	}
	else if (rxstate == 6)
	{
		rxstate = 0;
		_datatemp[4 + _data_cnt] = data;
		OPENMV_UP_DataAnl(_datatemp, _data_cnt + 5); //
	}
	else
	{
		rxstate = 0;
	}	

}

//OPENMV_UP数据处理
static void OPENMV_UP_DataAnl(uint8_t *data, uint8_t len1)
{
	u8 check_sum5 = 0, check_sum6 = 0;
	if (*(data + 3) != (len1 - 6)) //判断数据长度是否正确
		return;
	for (u8 i = 0; i < len1 - 2; i++)
	{
		check_sum5 += *(data + i);
		check_sum6 += check_sum5;
	}
	if ((check_sum5 != *(data + len1 - 2)) || (check_sum6 != *(data + len1 - 1))) //判断sum校验
		return;
	//================================================================================	

	if (*(data + 2) == 0XAA) //OPENMV 信息
	{
		openmv_up.pole_state = *(data+4);       //是否检测到紫色色块
		openmv_up.pole_posX = *(data+5);        //OPMV视角中  X位置 
		openmv_up.pole_posY = *(data+6);        //OPMV视角中 Y位置
    openmv_up.pole_areo = *(data+7);        //色块面积
			
		openmv_up.up_sta = 1;            //识别杆子 模式
	}
	if (*(data + 2) == 0XBB) //OPENMV 信息
	{
		openmv_up.polezi_state = *(data+4);       //是否检测到杆子
		openmv_up.polezi_posX = *(data+5);        //OPMV视角中 杆子 X位置 
    openmv_up.polezi_areo = *(data+6);        //杆子面积
		
		openmv_up.up_sta = 2;            //识别杆子 模式
	}
	
}

