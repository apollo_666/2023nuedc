#include "Drv_MY_OpenMv.h"

openmv_st openmv;

s8 check_point_1;
u8 check_point_2;
u8 check_point_3;
//全局变量
static uint8_t _datatemp[50];



//OPENMV获取一字节数据
void OPENMV_GetOneByte(u8 data)  // 0XAA OXFF 长度 
{
	static u8 _data_len = 0, _data_cnt = 0;
	static u8 rxstate = 0;

	if (rxstate == 0 && data == 0xAA)                        //帧头0XAA
	{
		rxstate = 1;
		_datatemp[0] = data;
	}
	else if (rxstate == 1 && data == HW_ALL)   //OXFF
	{
		rxstate = 2;
		_datatemp[1] = data;
	}
	else if (rxstate == 2)                                      //数据类型
	{
		rxstate = 3;
		_datatemp[2] = data;
	}
	else if (rxstate == 3 && data < 250)                   //数据长度
	{
		rxstate = 4;
		_datatemp[3] = data;
		_data_len = data;
		_data_cnt = 0;
	}
	else if (rxstate == 4 && _data_len > 0)
	{
		_data_len--;
		_datatemp[4 + _data_cnt++] = data;                   //有效数据接收
		if (_data_len == 0)
			rxstate = 5;
	}
	else if (rxstate == 5)                              //和校验
	{
		rxstate = 6;
		_datatemp[4 + _data_cnt++] = data;
	}
	else if (rxstate == 6)                              //和校验
	{
		rxstate = 0;
		_datatemp[4 + _data_cnt] = data;
		OPENMV_DataAnl(_datatemp, _data_cnt + 5); //
	}
	else
	{
		rxstate = 0;
	}	
}

//串口3  OPENMV数据处理
static void OPENMV_DataAnl(uint8_t *data, uint8_t len1)     
{
	u8 check_sum3 = 0, check_sum4 = 0;
	if (*(data + 3) != (len1 - 6 )) //判断数据长度是否正确
		return;
	for (u8 i = 0; i < len1 - 2; i++)
	{
		check_sum3 += *(data + i);
		check_sum4 += check_sum3;
	}
	if ((check_sum3 != *(data + len1 - 2)) || (check_sum4 != *(data + len1 - 1))) //判断sum校验
		return;
	//================================================================================	

	if (*(data + 2) == 0XAA) //OPENMV 信息     点数据
	{
		openmv.cb.color_flag = *(data+4);
		openmv.cb.sta = *(data+5);
		openmv.cb.pos_x = (s16)((*(data+6)<<8)|*(data+7));
		openmv.cb.pos_y = (s16)((*(data+8)<<8)|*(data+9));
		openmv.cb.dT_ms = *(data+10);
		//
		openmv.mode_sta = 1;
	}
	else if (*(data + 2) == 0XBB) //OPENMV 信息      线数据    
	{	
		
		
		openmv.lt.sta = *(data+4);
		openmv.lt.angle = *(data+5);
		openmv.lt.deviation = *(data+6);
		check_point_2 = openmv.lt.pos_x = *(data+7);
		check_point_3 = openmv.lt.pos_y = *(data+8);
		
		openmv.mode_sta = 2;
		
	}
	else if(*(data + 2) == 0XCC)
	{			
		openmv.state = *(data+4);
		openmv.posX = *(data+5);
		openmv.posY = *(data+6);
		openmv.angle = *(data+7);
		
		openmv.mode_sta = 3;
	}
}

/*
	0XAA 0XFF 0XBB len   sta      angle   error   cross_x  cross_y   sum1 sum2 
                      0：直线     锐角右转                直角中心坐标
                      1：直角     钝角左转
*/