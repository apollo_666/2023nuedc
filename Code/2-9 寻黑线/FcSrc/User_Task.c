#include "User_Task.h"
#include "Drv_RcIn.h"
#include "LX_FC_Fun.h"
#include "Ano_Math.h"
#include "Drv_Uart.h"
#include "Drv_MY_OpenMv.h"
#include "Math.h"
#include "My_F103.h"
 
 


 
s16 cal_X,cal_Y;
s16 cal_X_2,cal_Y_2;
s16 cal_X_3,cal_Y_3;
//巡线变量
static u8 step_pro_sta;
Speed speed;

u8 offset_Distance;
u8 cmd_send_state = 0;/*实时控制帧确认发送标志位*/ 

s16 Second_Distance;
s16 Second_angle;

s16 CMDvalue[4]; //实时控制帧x y z deg发送数据 

s16 PLANE_YAW;   //飞机当前航向角,每100ms自动获取数值
s32 Ano_Alt;   //飞机对地高度,自动获取数值,单位cm
s32 POS_X,POS_Y;
s16 angle_bis;     //角度补偿的角度误差

s16 Dcx = 0,Dcy = 0,Dcx_A = 0,Dcy_A = 0; //通用OPENMV返回坐标XY偏差值

float rate;

u8 Num_1=3;
u8 Num_2=10;

                   // 0    1    2    3    4    5    6    7    8    9   10   11   12 
s16 position_X[13] = {0,  50, 200, 275, 350, 350, 275, 125, 125,  50, 200, 200, 350};           //目标点 与起点 X轴距离  
s16 position_Y[13] = {0, 275, 125, 200, -25, 275,  50,  50, 200, 125, -25, 275, 125};           //目标点 与起点 y轴距离



	                  // 0    1    2    3*   4    5    6    7    8    9*  10   11   12 
s16 position_dis[13] = {0, 280, 236, 265-15, 351, 445, 279, 135, 236, 125, 202, 340, 371};         //目标点 与起点距离  0表示起点
s16 position_ang[13] = {0,  10,  58,  64,  94,  52,  80,  68,  32,  22,  97,  36,  70};         //目标点 与起点角度




							 //       0    1    2    3*     4     5    6    7    8    9*  10     11   12 
s16 position_dis_GoHome[13] = {0, 280, 236, 280-30, 351, 445, 279, 135, 236, 125, 202-2, 340, 371};         //目标点 与起点距离  0表示起点
s16 position_ang_GoHome[13] = {0,  10+180,  58+180-2,  64+180,  94+180,  52+180,  80+180,  68+180,  32+180,  22+180,  97+180-6,  36+180,  70+180};         //目标点 与起点角度
							 //		 0       1        2    		3*  	 4    		5   	  6        7        8        9*      10        11       12 

 //实时控制帧赋值函数 	
void CMD_set(s16 xpcm, s16 ypcm, s16 zpcm, s16 degpres, u8 cmdstate)
{
	CMDvalue[0] = degpres;  //航向转动角速度，度每秒，逆时针为正
	CMDvalue[1] = xpcm;    //头向速度，厘米每秒
	CMDvalue[2] = ypcm;    //左向速度，厘米每秒
	CMDvalue[3] = zpcm;	 //天向速度，厘米每秒
	cmd_send_state = cmdstate;  //实时控制帧发送标志
}  

	
void UserTask_OneKeyCmd(void)
{
    //////////////////////////////////////////////////////////////////////
	static u8 one_key_takeoff_f = 1, one_key_land_f = 1, one_key_mission_f = 0;

    //判断有遥控信号才执行
    if (rc_in.no_signal == 0)
    {
        //判断第5通道拨杆位置 800<CH_6<1200
        if (rc_in.rc_ch.st_data.ch_[ch_5_aux1] > 800 && rc_in.rc_ch.st_data.ch_[ch_5_aux1] < 1200)
        {
            //还没有执行
            if (one_key_land_f == 0)
            {
                //标记已经执行
                one_key_land_f =
                    //执行一键降落
                    OneKey_Land();
            }
        }
        else
        {
            //复位标记，以便再次执行
            one_key_land_f = 0;
        }
		  	  
		  
		  
		  
	//判断第5通道拨杆位置 1700<CH_6<2200 
		if(rc_in.rc_ch.st_data.ch_[ch_5_aux1]>1200 && rc_in.rc_ch.st_data.ch_[ch_5_aux1]<1700)
		{
			//还没有执行
			if(one_key_mission_f ==0)
			{
				//标记已经执行
				one_key_mission_f = 1;
				//开始流程
				mission_step = 1;
			}
		}
		else
		{
			//复位标记，以便再次执行
			one_key_mission_f = 0;		
		}
		//
		if(one_key_mission_f==1)
		{
			static u16 time_dly_cnt_ms;
			//
			switch(mission_step)
			{
				case 0:
				{
					//reset
					time_dly_cnt_ms = 0;
				}
				break;
				
				case 1:
				{
					mission_step += 1;				
				}
				break;
				
				case 2:
				{
					//解锁
					mission_step += FC_Unlock();
					
				}
				break;
				
				case 3:
				{
					//等5秒
					if(time_dly_cnt_ms<5000)
					{
						time_dly_cnt_ms+=20;//ms
					}
					else
					{
						time_dly_cnt_ms = 0;
						mission_step += 1;
					}
				}
				break;
				
				case 4:
				{
					//起飞
					mission_step += OneKey_Takeoff(100);//参数单位：厘米； 0：默认上位机设置的高度。
				}
				break;
				
				case 5:
				{
					//等5秒
					if(time_dly_cnt_ms<5000)
					{
						time_dly_cnt_ms+=20;//ms
					}
					else
					{
						time_dly_cnt_ms = 0;
						mission_step += 1;						
					}					
				}
				break;
				
				case 6: 
				{  				                                          
					if(time_dly_cnt_ms < 1500)
					{
						time_dly_cnt_ms += 20;
						if(time_dly_cnt_ms - 500==0)    CMD_set(15,0,0,0,1); 
					}
					else 
					{
						mission_step ++;
						time_dly_cnt_ms = 0;
					}
				}
				break;
				
				case 7: 
				{  				                                          
					if(time_dly_cnt_ms < 50000)
					{
						time_dly_cnt_ms += 20;
						if(time_dly_cnt_ms - 500==0)   
						{
							My_Followlane(); 
							
						}
					}
					else 
					{
						mission_step = 18;
						time_dly_cnt_ms = 0;
					}
				}
				break;
				
				case 18:
				{
					//等2秒
					if(time_dly_cnt_ms<4000)
					{
						time_dly_cnt_ms+=20;//ms
					}
					else
					{
						time_dly_cnt_ms = 0;
						mission_step ++;
					}
				}
				break;
				
				case 19:
				{
					//执行一键降落
					OneKey_Land();	
				}
				break;				
				default:break;
			}
		}
		else
		{
			mission_step = 0;
		}
	}
}


void frame_send_8(u8 a,u8 num)
{
u8 DateSendBuffer[100];
u8 cnt=0;
u8 sumcheck = 0;
u8 addcheck = 0;
		DateSendBuffer[cnt++]=0xAA;
		DateSendBuffer[cnt++]=0xFF;
		DateSendBuffer[cnt++]=num;
		DateSendBuffer[cnt++]=1;
		DateSendBuffer[cnt++]=a;
for(u8 i=0; i < (DateSendBuffer[3]+4); i++)
{
sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
}
		DateSendBuffer[cnt++]=sumcheck;
		DateSendBuffer[cnt++]=addcheck;
	
	
DrvUart5SendBuf(DateSendBuffer,cnt);
}

void frame_send_16(s16 a,u8 num)
{
u8 DateSendBuffer[100];
u8 cnt=0;
u8 sumcheck = 0;
u8 addcheck = 0;
		DateSendBuffer[cnt++]=0xAA;
		DateSendBuffer[cnt++]=0xFF;
		DateSendBuffer[cnt++]=num;
		DateSendBuffer[cnt++]=2;
		DateSendBuffer[cnt++]=BYTE0(a);
	   DateSendBuffer[cnt++]=BYTE1(a);
	

for(u8 i=0; i < (DateSendBuffer[3]+4); i++)
{
sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
}
		DateSendBuffer[cnt++]=sumcheck;
		DateSendBuffer[cnt++]=addcheck;
	
	
DrvUart5SendBuf(DateSendBuffer,cnt);
}

void frame_send_float(float a,u8 num)
{
	u8 DateSendBuffer[100];
	u8 cnt=0;
	u8 sumcheck = 0;
	u8 addcheck = 0;
			DateSendBuffer[cnt++]=0xAA;
			DateSendBuffer[cnt++]=0xFF;
			DateSendBuffer[cnt++]=num;
			DateSendBuffer[cnt++]=4;
			DateSendBuffer[cnt++]=BYTE0(a);
			DateSendBuffer[cnt++]=BYTE1(a);
			DateSendBuffer[cnt++]=BYTE2(a);
			DateSendBuffer[cnt++]=BYTE3(a);
		

	for(u8 i=0; i < (DateSendBuffer[3]+4); i++)
	{
	sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
	addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
	}
			DateSendBuffer[cnt++]=sumcheck;
			DateSendBuffer[cnt++]=addcheck;
		
		
	DrvUart5SendBuf(DateSendBuffer,cnt);
}

void frame_send_32(s32 a,u8 num)
{
	u8 DateSendBuffer[100];
	u8 cnt=0;
	u8 sumcheck = 0;
	u8 addcheck = 0;
	
	DateSendBuffer[cnt++]=0xAA;
	DateSendBuffer[cnt++]=0xFF;
	DateSendBuffer[cnt++]=num;
	DateSendBuffer[cnt++]=4;
	DateSendBuffer[cnt++]=BYTE0(a);
	DateSendBuffer[cnt++]=BYTE1(a);
	DateSendBuffer[cnt++]=BYTE2(a);
	DateSendBuffer[cnt++]=BYTE3(a);
	
	for(u8 i=0; i < (DateSendBuffer[3]+4); i++)
	{
		sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
	}
	
	DateSendBuffer[cnt++]=sumcheck;
	DateSendBuffer[cnt++]=addcheck;

   DrvUart5SendBuf(DateSendBuffer,cnt);
}

//OPMV数据 发送上位机
void Userdata_send_OPENMV()
{
	static u8 User_send_buffer[20];
	u8 _cnt = 0;
	User_send_buffer[_cnt++] = 0XAA;
	User_send_buffer[_cnt++] = 0XFF;
	if(openmv.mode_sta==1)
	{
		User_send_buffer[_cnt++] = 0Xf2;
		User_send_buffer[_cnt++] = 0;
		User_send_buffer[_cnt++] = openmv.mode_sta;
		User_send_buffer[_cnt++] = openmv.cb.color_flag;
		User_send_buffer[_cnt++] = openmv.cb.sta;
		User_send_buffer[_cnt++] = BYTE0(openmv.cb.pos_x);
		User_send_buffer[_cnt++] = BYTE1(openmv.cb.pos_x);
		User_send_buffer[_cnt++] = BYTE0(openmv.cb.pos_y);
		User_send_buffer[_cnt++] = BYTE1(openmv.cb.pos_y);
		User_send_buffer[_cnt++] = openmv.cb.dT_ms;
		User_send_buffer[3] =  _cnt - 4;
	}
	else if(openmv.mode_sta==2)
	{
		User_send_buffer[_cnt++] = 0XF3;
		User_send_buffer[_cnt++] = 0;
		User_send_buffer[_cnt++] = openmv.lt.sta;
		User_send_buffer[_cnt++] = openmv.lt.angle;
		User_send_buffer[_cnt++] = openmv.lt.deviation;
		User_send_buffer[_cnt++] = openmv.lt.pos_x;
		User_send_buffer[_cnt++] = openmv.lt.pos_y;
		User_send_buffer[3] =  _cnt - 4;
	}
	else if(openmv.mode_sta==3)
	{
		User_send_buffer[_cnt++] = 0XF4;
		User_send_buffer[_cnt++] = 0;
		User_send_buffer[_cnt++] = openmv.state;
		User_send_buffer[_cnt++] = openmv.posX;
		User_send_buffer[_cnt++] = openmv.posY;
		User_send_buffer[_cnt++] = openmv.angle;
		
		if (openmv.state == 255) //P点
		{
			Dcx = openmv.posX - (80+5);                       
			Dcy = openmv.posY - (70-10);
		}
		else                      //其他点
		{
			Dcx = openmv.posX - (70+5);                       
			Dcy = openmv.posY - (30-3);
		}
			
			if(Dcx>=0&&Dcy<=0)openmv.angle_out = openmv.angle;
				else if(Dcx>0&&Dcy>0)openmv.angle_out = 180 - openmv.angle;
				else if(Dcx<0&&Dcy>0)openmv.angle_out = openmv.angle + 180;
				else if(Dcx<0&&Dcy<0)openmv.angle_out = 360 - openmv.angle;
		
		User_send_buffer[_cnt++] = BYTE0(openmv.angle_out);
		User_send_buffer[_cnt++] = BYTE1(openmv.angle_out);
		
//		User_send_buffer[_cnt++] = openmv.posX_A;
//		User_send_buffer[_cnt++] = openmv.posY_A;
		
//			Dcx_A = openmv.posX_A - 80;
//			Dcy_A = openmv.posY_A - 60;
//			if(Dcx_A>=0&&Dcy_A<=0)openmv.angle_A_out = openmv.angle_A;
//				else if(Dcx_A>0&&Dcy_A>0)openmv.angle_A_out = 180 - openmv.angle_A;
//				else if(Dcx_A<0&&Dcy_A>0)openmv.angle_A_out = openmv.angle_A + 180;
//				else if(Dcx_A<0&&Dcy_A<0)openmv.angle_A_out = 360 - openmv.angle_A;
//				else openmv.angle_A_out = 0;
				

		
//		User_send_buffer[_cnt++] = openmv.angle_A;
//		User_send_buffer[_cnt++] = BYTE0(openmv.angle_A_out);
//		User_send_buffer[_cnt++] = BYTE1(openmv.angle_A_out);

		User_send_buffer[3] =  _cnt - 4;
	}
//校验
	u8 check_sum1 = 0, check_sum2 = 0;
	for (u8 i = 0; i < _cnt; i++)
	{
		check_sum1 += User_send_buffer[i];
		check_sum2 += check_sum1;
	}
	User_send_buffer[_cnt++] =  check_sum1;
	User_send_buffer[_cnt++] =  check_sum2;
	
	//
	DrvUart5SendBuf(User_send_buffer,_cnt);		
}

/* 
	位置误差弥补函数
   输出：弥补距离--offset_Distance
			弥补角度--offset_Angle

*/
void offset_error_xy(s16 target_x,s16 target_y,s16 real_x,s16 real_y)
{	
	s16 error_x,error_y;
	error_x = (real_x - target_x);
	error_y = (real_y - target_y);
	offset_Distance = (u16) (my_sqrt((error_x*error_x)+(error_y*error_y)));
	if(error_x>0&&error_y>0)//1象限
	{
	offset_Angle = 360-fast_atan2(ABS(error_y),ABS(error_x));
	}
	else	if(error_x<0&&error_y>0)//2象限
	{
	offset_Angle = 360-(180-fast_atan2(ABS(error_y),ABS(error_x)));
	}
	else	if(error_x<0&&error_y<0)//3象限
	{
	offset_Angle =360-(180+fast_atan2(ABS(error_y),ABS(error_x)));
	}
	else	if(error_x>0&&error_y<0)//4象限
	{
	offset_Angle = fast_atan2(ABS(error_y),ABS(error_x));
	}
}

/*
	Second_angle角度计算
	return：Second_angle
   XY坐标系（非机头坐标系）
*/

s16 Angle_cal(s16 bis_X,s16 bis_Y,u8 xiangxian)
{

	s16 angle;
	rate = ABS(bis_X)*100/ABS(bis_Y);
	if (rate == 300)                    //3:1
	{
		switch (xiangxian)
		{
			case 1:
				angle = 72;
				break;
			
			case 2:
				angle = 360 - 72;
				break;
			
			case 3:
				angle = 180 + 72;
				break;
			
			case 4:
				angle = 180 - 72;
				break;
			default:
				break;
		}
	}
	else if (rate == 200)                     //2:1
	{
		switch (xiangxian)
		{
			case 1:
				angle = 63;
				break;
			
			case 2:
				angle = 360 - 63;
				break;
			
			case 3:
				angle = 180 + 63;
				break;
			
			case 4:
				angle = 180 - 63;
				break;
			default:
				break;
		}
	}
	else if (rate == 100)                //1:1
	{
		switch (xiangxian)
		{
			case 1:
				angle = 45;
				break;
			
			case 2:
				angle = 360 - 45;
				break;
			
			case 3:
				angle = 180 + 45;
				break;
			
			case 4:
				angle = 180 - 45;
				break;
			default:
				break;
		}
	}
	else if (rate == 50)                  //  1:2
	{
		switch (xiangxian)
		{
			case 1:
				angle = 27;
				break;
			
			case 2:
				angle = 360 - 27;
				break;
			
			case 3:
				angle = 180 + 27;
				break;
			
			case 4:
				angle = 180 - 27;
				break;
			default:
				break;
		}
	}
	else if (rate < 40 && rate > 0)                //1:3
	{
		switch (xiangxian)
		{
			case 1:
				angle = 18;
				break;
			
			case 2:
				angle = 360 - 18;
				break;
			
			case 3:
				angle = 180 + 18-5;
				break;
			
			case 4:
				angle = 180 - 18;
				break;
			default:
				break;
		}
	}
	
	return angle;	
}

/*
	象限分析
	可以得出 Second_angle，Second_Distance
*/
//花椒算法 撰写人：
void Quadrant_anl(u8 num_1,u8 num_2)   
{
	s16 bis_X,bis_Y;
	u8 xiangxian;
	s16 distance;

	
	bis_X = position_X[num_2] - position_X[num_1];
	bis_Y = position_Y[num_2] - position_Y[num_1];
	
	
	if(bis_X>0 && bis_Y>0)
	{
		xiangxian=1;
		Second_angle = Angle_cal(bis_X,bis_Y,xiangxian);
	}
	else if(bis_X<0 && bis_Y>0)
	{
		xiangxian=2;
		Second_angle = Angle_cal(bis_X,bis_Y,xiangxian);
	}
	else if(bis_X<0 && bis_Y<0)
	{
		xiangxian=3;
		Second_angle = Angle_cal(bis_X,bis_Y,xiangxian);
	}
	else if(bis_X>0 && bis_Y<0)
	{
		xiangxian=4;
		Second_angle = Angle_cal(bis_X,bis_Y,xiangxian);
	}
	else if(bis_X>0 && bis_Y==0)
	{
		xiangxian=5;
		Second_angle = 90;
	}
	else if(bis_X==0 && bis_Y>0)
	{
		xiangxian=6;
		Second_angle = 0;
	}
	else if(bis_X<0 && bis_Y==0)
	{
		xiangxian=7;
		Second_angle = 270;
	}
	else if(bis_X==0 && bis_Y<0)
	{
		xiangxian=8;
		Second_angle = 180;
	}
	
	distance = my_sqrt((bis_X*bis_X)+(bis_Y*bis_Y));
	
	if(distance < 116 && distance > 96)
	{
		Second_Distance=106;
	}
	else if(distance<160&&distance>140)
	{
		Second_Distance=150;
	}
		else if(distance<222&&distance>202)
	{
		Second_Distance=212;
	}
		else if(distance<247&&distance>210 )     //例如： 3 -> 10  
	{
		Second_Distance=237 - 37-5;
	}
		else if(distance<310&&distance>290)
	{
		Second_Distance=300;
	}
		else if(distance<328&&distance>308)
	{
		Second_Distance=318;
	}
		else if(distance<345&&distance>325)
	{
		Second_Distance=335;
	}
		else if(distance<434&&distance>414)
	{
		Second_Distance=424;
	}
	frame_send_16(Second_Distance,0XF1);
}

/**********************************************************************************************************
*函 数 名: ANO_LT_StepProcedure
*功能说明: 匿名科创寻线循迹分步控制任务
*参    数: 周期时间(ms,形参)
*返 回 值: 无
**********************************************************************************************************/


void My_Followlane(void)
{
	static u8 confirm_cnt;
	static u16 elapsed_time_ms;
	switch(step_pro_sta)
	{
		case 0:
		{
			//reset
			speed.Vxpcm = 0;	
			speed.Vypcm = 0;
			speed.Vdps = 0;
			elapsed_time_ms = 0;
			step_pro_sta = 1;
		}
		break;
		case 1:
		{
			//复位确认次数
			confirm_cnt = 0;
			//识别直线
			if(openmv.lt.sta == 0)
			{
				//
							speed.Vxpcm = 15;
				CMD_set(speed.Vxpcm,speed.Vypcm,0,speed.Vdps,1);
			}
			//识别到直角左转
			else if(openmv.lt.sta == 1 && openmv.lt.angle > 90)
			{
				step_pro_sta = 2;
			}
			//识别到直角右转
			else if(openmv.lt.sta == 1 && openmv.lt.angle < 90)
			{
				step_pro_sta = 3;
			}
			else if(openmv.lt.sta == 0)
			{
				step_pro_sta = 0;
			}
				
		}
		break;
		case 2:
		{
			//确认是否识别到，否则返回
			if(openmv.lt.sta != 1 || openmv.lt.angle <= 90)
			{
				step_pro_sta = 1;

			}
			else
			{
				//再确认n次
				if(confirm_cnt<CONFIRM_TIMES)
				{
					confirm_cnt++;
				}
				else
				{
					confirm_cnt = 0;
					//下一步
					step_pro_sta = 4;
				}		
			}
		}
		break;
		case 3:
		{
			//确认是否识别到，否则返回
			if(openmv.lt.sta != 1 || openmv.lt.angle >= 90)
			{
				step_pro_sta = 1;

			}
			else
			{
				//再确认n次
				if(confirm_cnt<CONFIRM_TIMES)
				{
					confirm_cnt++;
				}
				else
				{
					confirm_cnt = 0;
					//下一步
					step_pro_sta = 5;
				}		
			}		
		}
		break;
		case 4:
		{
			//刹车减速
			speed.Vxpcm = 5;//识别到直角后减速前进的速度cm/s
			speed.Vypcm = 0;//左右纠正速度复位
			CMD_set(speed.Vxpcm,speed.Vypcm,0,speed.Vdps,1);
			//持续时间，转弯后偏内测，则加长时间;反之减少时间
			if(elapsed_time_ms<2000)
			{
				elapsed_time_ms+= 20;
			}
			else
			{
				elapsed_time_ms = 0;
//				if(opmv.lt.sta == 2)
				step_pro_sta = 12;
			}		
		}
		break;
		case 5:
		{
			//刹车减速
			speed.Vxpcm = 5;//识别到直角后减速前进的速度cm/s
			speed.Vypcm = 0;//左右纠正速度复位
			CMD_set(speed.Vxpcm,speed.Vypcm,0,speed.Vdps,1);
			//持续时间，转弯后偏内测，则加长时间;反之减少时间
			if(elapsed_time_ms<2000)
			{
				elapsed_time_ms+= 20;
			}
			else
			{
				elapsed_time_ms = 0;
//				if(opmv.lt.sta == 3)
				step_pro_sta = 13;
			}			
		}
		break;
		case 12://左转90度
		{
			//
			speed.Vxpcm = 0;//边旋转边给前进速度，走出一定圆弧
			speed.Vypcm = 0;//左右纠正速度复位
			speed.Vdps = YAW_PAL_DPS;
			CMD_set(speed.Vxpcm,speed.Vypcm,0,speed.Vdps,1);
			//
			if(elapsed_time_ms<90000/YAW_PAL_DPS)
			{
				elapsed_time_ms += 20;
			}
			else
			{
				speed.Vdps = 0;
				elapsed_time_ms = 0;
				step_pro_sta = 20;
			}
		}
		break;
		case 13://右转90度
		{
			//
			speed.Vxpcm = 0;//边旋转边给前进速度，走出一定圆弧
			speed.Vypcm = 0;//左右纠正速度复位
			speed.Vdps = -YAW_PAL_DPS;
			CMD_set(speed.Vxpcm,speed.Vypcm,0,speed.Vdps,1);
			//
			if(elapsed_time_ms<90000/YAW_PAL_DPS)
			{
				elapsed_time_ms += 20;
			}
			else
			{
				speed.Vdps = 0;
				elapsed_time_ms = 0;
				step_pro_sta = 20;
			}			
		}
		break;		
		case 20://衔接，因旋转后有可能没有看到直线，所以先设定前进500ms
		{
			//
			elapsed_time_ms+= 20;
			//
			if(elapsed_time_ms<100)
			{
				elapsed_time_ms+= 20;
			}
			else if(elapsed_time_ms<600)
			{
				elapsed_time_ms+= 20;
				speed.Vxpcm = 15;
				speed.Vypcm = 0;
				CMD_set(speed.Vxpcm,speed.Vypcm,0,speed.Vdps,1);
			}
			else
			{
				elapsed_time_ms = 0;
				step_pro_sta = 1;			
			}
		}
		break;
		default:
		{
			
		}
		break;
	}
	

}


