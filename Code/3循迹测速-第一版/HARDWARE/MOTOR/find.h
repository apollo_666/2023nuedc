#ifndef __FIND_H
#define __FIND_H

#include "stm32f10x.h"

//灰度寻迹传感器从左到右以此为Find_O1,Find_O2,Find_O3,Find_O4,Find_O5
//依次对应主控板上的PA4  PA5  PA6  PA7  PA8
#define Find_O1 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_4)
#define Find_O2 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_5)
#define Find_O3 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_6)
#define Find_O4 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_7)
#define Find_O5 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_8)

//外接的红外传感器，表示放进药物
#define Find_O6 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_11)

extern unsigned int FindCount[4];

void Find_IO_Init(void);
void Find(void);

#endif
