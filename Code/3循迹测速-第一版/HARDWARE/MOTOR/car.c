#include "car.h"
#include "pid.h"
#include "bsp_sys.h"
#include "bsp_usart.h"
#include "bsp_usart2.h"
#include "find.h"

unsigned int FindCount[4]={0,0,0,0};
int Encoder_A,Encoder_B,Encoder_C,Encoder_D;//编码器脉冲数
int Moto_A,Moto_B,Moto_C,Moto_D;//PWM 
int para_A,para_B,para_C,para_D;//增量
//unsigned int FindCount[4];

//这个设置很重要
int SetPoint=70;//设置目标值单位RPM，如果转换成线速度  = setopint*轮子周长 = setpoint*3.14*0.065(轮子直径65mm)单位就是m/min  一定要搞清楚车轮转速和线速的关系

//使用减速比是1：120的减速箱。根据实际电机的减速比的配置修改以下参数
//6240=13*4*120：霍尔编码器13线，STM32编码器模式 4倍频，减速箱1：120
//2496=13*4*48： 霍尔编码器13线，STM32编码器模式 4倍频，减速箱1：48
#define SetPointA SetPoint*6240/600         //换算成编码器脉冲数，因为最终pid控制的是编码器的脉冲数量
#define SetPointB SetPoint*6240/600         //换算成编码器脉冲数，因为最终pid控制的是编码器的脉冲数量
#define SetPointC SetPoint*6240/600         //换算成编码器脉冲数，因为最终pid控制的是编码器的脉冲数量
#define SetPointD SetPoint*6240/600         //换算成编码器脉冲数，因为最终pid控制的是编码器的脉冲数量

//各种变量
u32 temp1,temp2,temp3,temp4;
float sudu;
char  set_speed[5]; //车速显示小数
char  speed[5];     //车速显示小数

u8 speed11[5];      //车速显示小数

//Time1定时器1中断服务函数
//100ms定时
void TIM6_IRQHandler(void)
{
	if(TIM_GetFlagStatus(TIM6, TIM_IT_Update) != RESET) //时间到了
	{
		TIM_ClearITPendingBit(TIM6, TIM_FLAG_Update);     //清中断
			LED1_Flash(5);                                   //500ms闪烁一次
			Encoder_A=Read_Encoder(5)-30000;                   //读取编码器 
			Encoder_B=Read_Encoder(2)-30000;                  //读取编码器
			Encoder_C=Read_Encoder(3)-30000;                   //读取编码器 
			Encoder_D=Read_Encoder(4)-30000;                  //读取编码器
			//小车的移动速度，即轮子线速度
			sudu = (Encoder_A+Encoder_B+Encoder_C+Encoder_D)/4*600/6240*0.065*3.1415;//计算车速，左轮和右轮速度和除以2 单位 m/min
			sprintf(speed,"%2.2f",sudu);
		  OLED_ShowString(48,0, speed,16);  //OLED显示车速
			
			if(Encoder_A<0)//如果电机反转了
			{
				Encoder_A = -Encoder_A;
			}
			if(Encoder_B<0)
			{
				Encoder_B = -Encoder_B;
			}
			if(Encoder_C<0)
			{
				Encoder_C = -Encoder_C;
			}
			if(Encoder_D<0)
			{
				Encoder_D = -Encoder_D;
			}
			para_A=PID_Calc_A(Encoder_A,SetPointA);	  //A电机，计数得到增量式PID的增量数值 
		  para_B=PID_Calc_B(Encoder_B,SetPointB);	  //B电机，计数得到增量式PID的增量数值 
			para_C=PID_Calc_C(Encoder_C,SetPointC);	  //C电机，计数得到增量式PID的增量数值 
		  para_D=PID_Calc_D(Encoder_D,SetPointD);	  //D电机，计数得到增量式PID的增量数值 
			if((para_A<-5)||(para_A>5))                     //不做 PID 调整，避免误差较小时频繁调节引起震荡。
			{
				Moto_A +=para_A;  
			}   
			if((para_B<-5)||(para_B>5))                     //不做 PID 调整，避免误差较小时频繁调节引起震荡。
			{
				Moto_B +=para_B;  
			}		
			if((para_C<-5)||(para_C>5))                     //不做 PID 调整，避免误差较小时频繁调节引起震荡。
			{
				Moto_C +=para_C;  
			}   
			if((para_D<-5)||(para_D>5))                     //不做 PID 调整，避免误差较小时频繁调节引起震荡。
			{
				Moto_D +=para_D;  
			}
//  		if(Moto_Left>3500)Moto_Left=3000;    //限幅 防止烧毁电机
//			if(Moto_Right>3500)Moto_Right=3000;  //限幅
			
			TIM8->CCR1=Moto_A;  //更新pwm
			TIM8->CCR2=Moto_B; //更新pwm
			TIM8->CCR3=Moto_C;  //更新pwm
			TIM8->CCR4=Moto_D; //更新pwm
			//显示pid参数
			showPID();
			//显示左轮和右轮的输出转速 单位RPM 
			
			temp1 = Encoder_A*600/6240;
			OLED_ShowNum(50,2,FindCount[1],1,12);      //左上显示路口的状态标志位FindCount
			//temp2 = Encoder_B*600/6240;
			OLED_ShowNum(115,2,Lor,1,12);            	 //右上显示openmv发的地址位Lor
			//temp3 = Encoder_C*600/6240;
			OLED_ShowNum(50,4,Num2,1,12);              //左下显示k210发的数据位Num2
			temp4 = Encoder_D*600/6240;
			OLED_ShowNum(115,4,Lor2,1,12);             //右下显示k210发的地址位Lor2
//			USART2_Send("Speed:", 6);
//			sprintf(speed11,"%3d",temp1);
//			USART2_Send(&speed11[0], 5);
//			USART2_Send("RPM", 3);
//			USART2_Send("\n", 2);
			//设定值显示
			//OLED_ShowNum(48,6,SetPoint,3,12);    //显示设定的车轮转速 单位RPM
//			temp4 = SetPoint*3.1415*0.065;       //计算设定的线速度
//			sprintf(set_speed,"%2.2f",temp4);
//		  OLED_ShowString(48,7, set_speed,12);  //设定的线速度 单位m/min
		}
}

