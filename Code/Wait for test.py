import sensor, image, lcd, time
import KPU as kpu
from machine import UART
from fpioa_manager import fm
import gc, sys
sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.set_windowing((224,224))
sensor.set_hmirror(0)
lcd.init(type=1)
lcd.rotation(3)
lcd.clear(lcd.WHITE)
#sensor.set_vflip(True)
sensor.run(1)
#uart = UART(3, baudrate=115200)
fm.register(4, fm.fpioa.UART1_RX,force=True)
fm.register(5, fm.fpioa.UART1_TX,force=True)
uart = UART(UART.UART1, baudrate=115200, bits=8, parity=None,stop=1 , timeout=1000, read_buf_len=4096)
#uart.init(baudrate=115200, bits=8, parity=None, stop=1)
#uart = UART(0, baudrate=115200)
#uart.init(115200, , , )
labels = ['1', '2', '3', '4', '5', '6', '7', '8']
anchors = [1.40625, 1.8125000000000002, 5.09375, 5.28125, 3.46875, 3.8124999999999996, 2.0, 2.3125, 2.71875, 2.90625]
model_addr="/sd/m.kmodel"
if not labels:
    with open('labels.txt','r') as f:
        exec(f.read())
task = kpu.load(model_addr)
kpu.init_yolo2(task, 0.5, 0.3, 5, anchors) # threshold:[0,1], nms_value: [0, 1]
LoR=0
Task=1
Target_Num=0
FRAME_START = bytes([0x2C, 0x1C])  # 帧头
FRAME_END = 0x5B  # 帧尾
DATA_LENGTH = 2  # 数据长度
rx_buff=[]
state = 0
tx_flag = 0
'''def Receive_Prepare(data):
    global state
    global tx_flag
    if state==0:
        if data == 0x2C:#帧头
            state = 1
        else:
            state = 0
            rx_buff.clear()
    if state==1:
        if data == 0x1C:#帧头
            state = 2
        else:
            state = 0
            rx_buff.clear()
    elif state==2:
        rx_buff.append(data)
        state = 3
    elif state==3:
        rx_buff.append(data)
        state = 4
        int.from_bytes(rx_buff[1])
    elif state == 4:
        if data == 0x5B:
            tx_flag = int(rx_buff[0])
            state = 4
    else:
        state = 0
        rx_buff.clear()'''
while 1:
    img = sensor.snapshot()
    t = time.ticks_ms()
    objects = kpu.run_yolo2(task, img)
    t = time.ticks_ms() - t
    if Task==1:
        if objects:
            for obj in objects:
                pos = obj.rect()
                img.draw_rectangle(pos)
                img.draw_string(pos[0], pos[1], "%s : %.2f" %(labels[obj.classid()], obj.value()), scale=2, color=(255, 0, 0))

                Target_Num=int (labels[obj.classid()])
            FH = bytearray([0x2C,0x12,Target_Num, LoR,0x5B])
#            print(Target_Num)
            uart.write(FH)
        img.draw_string(0, 200, "t:%dms" %(t), scale=2, color=(255, 0, 0))
        lcd.display(img)
        if Target_Num!=0:
            time.sleep(2)
            Task+=1
    if Task==2:
        img = sensor.snaps hot()
        objects = kpu.run_yolo2(task, img)
        if objects:
            for obj in objects:
                pos = obj.rect()
                img.draw_rectangle(pos)
                img.draw_string(pos[0], pos[1], "%s : %.2f" %(labels[obj.classid()], obj.value()), scale=2, color=(255, 0, 0))
#                print(labels[obj.classid()])
                Num=int (labels[obj.classid()])
                if Num==Target_Num:
                    LoR=1
#                    print(LoR)
                    FH = bytearray([0x2C,0x12,Target_Num, LoR,0x5B])
                    uart.write(FH)
                    Task+=1
                    time.sleep(1)

        img.draw_string(0, 200, "t:%dms" %(t), scale=2, color=(255, 0, 0))
        lcd.display(img)

    if Task==3:
        img = sensor.snapshot()
        objects = kpu.run_yolo2(task, img)
        if objects:
            for obj in objects:
                pos = obj.rect()
                img.draw_rectangle(pos)
                img.draw_string(pos[0], pos[1], "%s : %.2f" %(labels[obj.classid()], obj.value()), scale=2, color=(255, 0, 0))
                Num=int (labels[obj.classid()])
                LoR=0
                if Num==Target_Num:
                    LoR=1
#                    print(LoR)
                    FH = bytearray([0x2C,0x12,Target_Num, LoR,0x5B])
                    uart.write(FH)
                    time.sleep(1)
        img.draw_string(0, 200, "t:%dms" %(t), scale=2, color=(255, 0, 0))
        lcd.display(img)
#    if(uart.any()>0):
#        c=uart.readchar()
#        Receive_Prepare(c)


