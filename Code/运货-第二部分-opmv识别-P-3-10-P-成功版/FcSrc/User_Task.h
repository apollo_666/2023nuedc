#ifndef __USER_TASK_H
#define __USER_TASK_H

#include "SysConfig.h"

#define FLYSPEED 30

extern float offset_Angle;
extern s16 PLANE_YAW;
extern s32 POS_X,POS_Y;

void UserTask_OneKeyCmd(void);
void frame_send_8(u8 a,u8 num);
void frame_send_16(s16 a,u8 num);
void frame_send_float(float a,u8 num);
void frame_send_32(s32 a,u8 num);
void offset_error_xy(s16 target_x,s16 target_y,s16 real_x,s16 real_y);
void Quadrant_anl(u8 num_1,u8 num_2);
s16 Angle_cal(s16 bis_X,s16 bis_Y,u8 xiangxian);


extern u8 mission_step;
extern float start_yaw;

void Userdata_send_OPENMV();



#endif
