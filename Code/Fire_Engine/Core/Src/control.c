#include "control.h"

/* ϵͳͳһת��Ϊ���������д������������ֱ��ʸߣ����ƾ��ȸ� */


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//2�����ӵ�PID����

float Position_KP_L=0.5,Position_KI_L=0,Position_KD_L=0.5; 
float Incremental_KP_L=120,Incremental_KI_L=3,Incremental_KD_L=0.1;   

float Position_KP_R=0.5,Position_KI_R=0,Position_KD_R=0.5; 
float Incremental_KP_R=120,Incremental_KI_R=3,Incremental_KD_R=0.1; 


//float Position_KP_L=1,Position_KI_L=0.001,Position_KD_L=7.7; 
//float Incremental_KP_L=80.0,Incremental_KI_L=4,Incremental_KD_L=0.0;   

//float Position_KP_R=1,Position_KI_R=0.0009,Position_KD_R=8; 
//float Incremental_KP_R=135.0,Incremental_KI_R=1.5,Incremental_KD_R=0.0; 


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Go_Forward(int16_t unit)
{
	Motor_V(16,16);
	Motor_P(unit*40,unit*40);
	if(((Target_Position_L - unit*40) < 1) && ((Target_Position_R - unit*40) < 1))
	{ 
		Clean_Task_Encoder_val();
		Clear_All_Motor_Accumulated_Encoder_val();
		
	}
}

void Go_Back(int16_t unit)
{
	Motor_V(16,16);
	Motor_P(-unit*40,-unit*40);
	if(((Target_Position_L + unit*40) < 1) && ((Target_Position_R + unit*40) < 1))
	{ 
		Clean_Task_Encoder_val();
		Clear_All_Motor_Accumulated_Encoder_val();
	}
}

void Turn_Left_90(int16_t unit)
{
	Motor_V(16,16);
	Motor_P(-unit*564,unit*564);
	if(((Target_Position_L + unit*564) < 1) && ((Target_Position_R - unit*564) < 1))
	{ 
		Clean_Task_Encoder_val();
		Clear_All_Motor_Accumulated_Encoder_val();
	}
}

void Turn_Right_90(int16_t unit)
{
	Motor_V(16,16);
	Motor_P(unit*564,-unit*564);
	if(((Target_Position_L - unit*564) < 1) && ((Target_Position_R + unit*564) < 1))
	{
		Clean_Task_Encoder_val();
		Clear_All_Motor_Accumulated_Encoder_val();
	}
}

void Turn_fix_Left(int16_t unit)
{
	Motor_V(16,16);
	Motor_P(-unit*2,unit*2);
	if(((Target_Position_L + unit*2) < 1) && ((Target_Position_R - unit*2) < 1))
	{ 
		Clean_Task_Encoder_val();
		Clear_All_Motor_Accumulated_Encoder_val();
	}
}

void Turn_fix_Right(int16_t unit)
{
	Motor_V(16,16);
	Motor_P(unit*2,-unit*2);
	if(((Target_Position_L - unit*2) < 1) && ((Target_Position_R + unit*2) < 1))
	{ 
		Clean_Task_Encoder_val();
		Clear_All_Motor_Accumulated_Encoder_val();
	}
}

//���λ��ʾ��ͼ
//1 L  2 R

//1 L
/* Ŀ���ٶȣ�ʵ���ٶ� */
int Target_Velocity_L=0,Reality_Velocity_L=0;   
/* Ŀ��λ�ã�ʵ��λ�� */
int Target_Position_L=0,Reality_Position_L=0;   

/**
  * @brief  ����PID�㷨ʵ��
  * @param  ��
	*	@note 	��
  * @retval ͨ��PID���������
  */
	
	
int L_Pwm = 0;
	
void L_Position_Incremental_PID(void)
{

  /* ��ȡʵ��������static  */  
  Reality_Velocity_L = Read_Speed_Encoder(2);
  /* ʵ��λ�������� */
  Reality_Position_L += Reality_Velocity_L;
  /* λ��ʽλ�ÿ��� */    
  L_Pwm = L_Position_PID(Reality_Position_L,Target_Position_L);
  /* λ�û�����޷� */
  L_Pwm = Xianfu(L_Pwm,Target_Velocity_L);
  /* �˳����ָ��� */
  if(Abs(Reality_Position_L-Target_Position_L)<5)             
  {
      /* ֹͣ��� */
      motor_pwm(1,0);                                         
  }
  else
  {   
      /* ����ʽ�ٶȿ��� */
//      L_Pwm = L_Incremental_PID(Reality_Velocity_L,Target_Velocity_L);      

      L_Pwm = L_Incremental_PID(Reality_Velocity_L,L_Pwm);      
      /* ��ֵ */
      motor_pwm(1,L_Pwm);
  }
}

/**
  * @brief  λ��PID�㷨ʵ��
  * @param  ʵ��λ�ã�Ŀ��λ��
	*	@note 	��
  * @retval ͨ��PID���������
  */
int L_Position_PID(int reality,int target)
{ 	
    static float err,err_1,err_sum,Pwm=0;
    err = target-reality;                        /* ����ƫ�� */
    err_sum += err;	                             /* ƫ���ۻ� */
    if(err_sum> 5000) err_sum = 5000;            /* �����޷� */
    if(err_sum<-5000) err_sum =-5000;            
    Pwm = (Position_KP_L*err)                   /* �������� */
         +(Position_KI_L*err_sum)               /* ���ֻ��� */
         +(Position_KD_L*(err-err_1));          /* ΢�ֻ��� */
    err_1=err;                                   /* �����ϴ�ƫ��*/
    return Pwm;                                 /* ������ */
}
int L_Incremental_PID(int reality,int target)
{ 	
	 static float err,err_1,err_2,Pwm;
    
	 err=target-reality;                                /* ����ƫ�� */
	 Pwm += (Incremental_KP_L*(err-err_1))             /* �������� */
           +(Incremental_KI_L*err)                   /* ���ֻ��� */
           +(Incremental_KD_L*(err-2*err_1+err_2));  /* ΢�ֻ��� */ 
   err_2=err_1;                                       /* �������ϴ�ƫ�� */
	 err_1=err;	                                        /* ������һ��ƫ�� */
	 return Pwm;                                       /* ������ */
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//���λ��ʾ��ͼ
//1 L  2 R

/* Ŀ���ٶȣ�ʵ���ٶ� */
int Target_Velocity_R=0,Reality_Velocity_R=0;   
/* Ŀ��λ�ã�ʵ��λ�� */
int Target_Position_R=0,Reality_Position_R=0;   

/**
  * @brief  ����PID�㷨ʵ��
  * @param  ��
	*	@note 	��
  * @retval ͨ��PID���������
  */

int R_Pwm = 0;

void R_Position_Incremental_PID(void)
{

  /* ��ȡʵ�������� static */  
  Reality_Velocity_R = Read_Speed_Encoder(3);
  /* ʵ��λ�������� */
  Reality_Position_R += Reality_Velocity_R;
  /* λ��ʽλ�ÿ��� */    
  R_Pwm = R_Position_PID(Reality_Position_R,Target_Position_R);
  /* λ�û�����޷� */
  R_Pwm = Xianfu(R_Pwm,Target_Velocity_R);
  /* �˳����ָ��� */
  if(Abs(Reality_Position_R-Target_Position_R)<5)             
  {
      /* ֹͣ��� */
      motor_pwm(2,0);                                         
  }
  else
  {   
      /* ����ʽ�ٶȿ��� */
//	    R_Pwm = R_Incremental_PID(Reality_Velocity_R,Target_Velocity_R);      

      R_Pwm = R_Incremental_PID(Reality_Velocity_R,R_Pwm);      
      /* ��ֵ */
      motor_pwm(2,R_Pwm);                                      
  }
}
 
int R_Position_PID(int reality,int target)
{ 	
    static float err,err_1,err_sum,Pwm=0;
    err = target-reality;                        /* ����ƫ�� */
    err_sum += err;	                             /* ƫ���ۻ� */
    if(err_sum> 5000) err_sum = 5000;            /* �����޷� */
    if(err_sum<-5000) err_sum =-5000;            
    Pwm = (Position_KP_R*err)                   /* �������� */
         +(Position_KI_R*err_sum)               /* ���ֻ��� */
         +(Position_KD_R*(err-err_1));          /* ΢�ֻ��� */
    err_1=err;                                   /* �����ϴ�ƫ��*/
    return Pwm;                                 /* ������ */
}

int R_Incremental_PID(int reality,int target)
{ 	
	 static float err,err_1,err_2,Pwm;
    
	 err=target-reality;                                /* ����ƫ�� */
	 Pwm += (Incremental_KP_R*(err-err_1))             /* �������� */
           +(Incremental_KI_R*err)                   /* ���ֻ��� */
           +(Incremental_KD_R*(err-2*err_1+err_2));  /* ΢�ֻ��� */ 
   err_2=err_1;                                       /* �������ϴ�ƫ�� */
	 err_1=err;	                                        /* ������һ��ƫ�� */
	 return Pwm;                                       /* ������ */
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**************************************************************************
�������ܣ�����PWM��ֵ 
��ڲ�����int value,int Amplitude
����  ֵ����
**************************************************************************/
int Xianfu(int value,int Amplitude)
{	
	float temp;
	if(value>Amplitude) temp = Amplitude;
	else if(value<-Amplitude) temp = -Amplitude;
	else temp = value;
	return temp;
}

/**************************************************************************
�������ܣ�ȡ����ֵ
��ڲ�����int
����  ֵ��int
**************************************************************************/
int Abs(int a)
{ 		   
	  int temp;
		if(a<0)  temp=-a;  
	  else temp=a;
	  return temp;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


