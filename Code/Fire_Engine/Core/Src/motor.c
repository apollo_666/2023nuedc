#include "motor.h"

//电机位置示意图
//1 L  2 R

/*电机PWM赋值
*motor：选择电机(1,2)
*speed：PWM[-1000--+1000]
*/

void motor_pwm(uint8_t motor,int16_t pwm)
{
//motor：1（左电机） 2（右电机），pwm[-1000,1000]
	if(motor == 1)
	{
		if(pwm<0)//clockwise
		{
			if(pwm<-1000)
				pwm=-1000;
			HAL_GPIO_WritePin(IN1_GPIO_Port,IN1_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(IN2_GPIO_Port,IN2_Pin,GPIO_PIN_SET);
			__HAL_TIM_SetCompare(&htim1,TIM_CHANNEL_1,-pwm);
		}
		else//anticlockwise
		{
			if(pwm>1000)
				pwm=1000;
			HAL_GPIO_WritePin(IN1_GPIO_Port,IN1_Pin,GPIO_PIN_SET);
			HAL_GPIO_WritePin(IN2_GPIO_Port,IN2_Pin,GPIO_PIN_RESET);
			__HAL_TIM_SetCompare(&htim1,TIM_CHANNEL_1,pwm);
		}
	}
	else if(motor == 2)
	{
		if(pwm<0)//clockwise
		{
			if(pwm<-1000)
				pwm=-1000;
			HAL_GPIO_WritePin(IN3_GPIO_Port,IN3_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(IN4_GPIO_Port,IN4_Pin,GPIO_PIN_SET);
			__HAL_TIM_SetCompare(&htim1,TIM_CHANNEL_2,-pwm);
		}
		else//anticlockwise
		{
			if(pwm>1000)
				pwm=1000;
			HAL_GPIO_WritePin(IN3_GPIO_Port,IN3_Pin,GPIO_PIN_SET);
			HAL_GPIO_WritePin(IN4_GPIO_Port,IN4_Pin,GPIO_PIN_RESET);
			__HAL_TIM_SetCompare(&htim1,TIM_CHANNEL_2,pwm);
		}
	}
}
void motor_1khz(int16_t motor_L,int16_t motor_R)
{
	motor_pwm(1,motor_L);
	motor_pwm(2,motor_R);
}



/**************************************************************************
函数功能：把所有电机都停止
入口参数：无
返回  值：无
**************************************************************************/
void Clean_All_Motor_Pwm(void)
{
  motor_1khz(0,0);
}
  
/**************************************************************************
函数功能：清除每次任务后的编码器累计值
入口参数：无
返回  值：无
**************************************************************************/
void Clean_Task_Encoder_val(void)
{
  Reality_Position_L = 0;
  Reality_Position_R = 0;
}

void Clear_All_Motor_Accumulated_Encoder_val(void)
{
  TIM2 -> CNT = 0;
  TIM3 -> CNT = 0;
}

/**************************************************************************
函数功能：
入口参数：
返回  值：
**************************************************************************/
void Motor_P(int16_t MotorP_L,int16_t MotorP_R)
{
   Target_Position_L = MotorP_L;
   Target_Position_R = MotorP_R;
}

/**************************************************************************
函数功能：
入口参数：
返回  值：
**************************************************************************/
void Motor_V(int16_t MotorV_L,int16_t MotorV_R)
{
  Target_Velocity_L = MotorV_L;
  Target_Velocity_R = MotorV_R;
}

/**************************************************************************
函数功能：单位时间读取编码器计数
入口参数：定时器
返回  值：单位时间编码器读数
**************************************************************************/
short Read_Speed_Encoder(uint8_t TIMX)
{
   short Encoder_TIM;    
   switch(TIMX)
	 {  
      //L
	    case 2:   Encoder_TIM = (short)TIM2 -> CNT;  TIM2 -> CNT = 0;  break;
      //R
      case 3:   Encoder_TIM = -(short)TIM3 -> CNT;  TIM3 -> CNT = 0; 	break;	
     
      default:  Encoder_TIM = 0;	break;
	 }
		return Encoder_TIM;
}


/**************************************************************************
函数功能：读取位置信息
入口参数：定时器
返回  值：累计时间编码器读数
**************************************************************************/
short Read_Position_Encoder(uint8_t TIMX)
{
   short Encoder_TIM;
   switch(TIMX)
	 {
      //L
	    case 2:   Encoder_TIM = (short)TIM2 -> CNT;   	 break;
      //R
      case 3:   Encoder_TIM = -(short)TIM3 -> CNT;   	 break;
     
      default:  Encoder_TIM = 0;break;
	 }
		return Encoder_TIM;
}

