/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "motor.h"
#include "control.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define encoder  40
#define ACW      565
#define CW       559

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
//用于定时器6中的变量，用于PID控制
volatile uint32_t TimerCount    = 0;
//用于定时器7中的变量，控制不同的动作
volatile uint32_t Task1  = 0;

uint8_t Run_Flag=0,Run_Flag_Start=0,Chassis_mode=0,Back_mode=0,ARM_mode=6;
uint8_t Clean_Flag=0;		//清除多余编码器数值标志位
int32_t encoder_Counter_L = 0,encoder_Counter_R = 0;   /* 保存编码器反馈的脉冲总数 */
int8_t encoder_Direction_L = 0,encoder_Direction_R = 0;  /* 保存编码器反馈的脉冲方向 */
int Timer = 0;			//时间
uint8_t speed = 0;		//当前PWM
short TargetSpeed = 10; //目标速度

uint32_t t_ms_stop = 0;
uint16_t t_ms = 0;
uint16_t t_100us = 0;
uint16_t t_Run_Flag = 0,t_Debug_Flag = 0,Pipe_Flag = 0;
uint16_t oled_t_ms=0;
uint16_t t_100ms = 0;
int iButtonCount_UKEY;//i代表int型变量，ButtonCount表示按键计数变量
int iButtonCount_Button1,iButtonCount_Button2,iButtonCount_Button3;
int iButtonFlag_UKEY;//i代表int型变量，ButtonFlag表示重按键标志，1代表重新按键，0为没有重新按键
int iButtonFlag_Button1,iButtonFlag_Button2,iButtonFlag_Button3,OLED_RST_Flag=1,OLED_Refresh=0;
int g_iButtonState_UKEY=0;//g是globle代表全局变量，会在其他地方引用；i代表int型变量，ButtonState表示按键标志，1代表按下，0代表松开
int g_iButtonState_Button1=0,g_iButtonState_Button2=0,g_iButtonState_Button3=0;

int iButtonCount_T,iButtonCount_M;//i代表int型变量，ButtonCount表示按键计数变量
int iButtonFlag_T,iButtonFlag_M;//i代表int型变量，ButtonFlag表示重按键标志，1代表重新按键，0为没有重新按键
int g_iButtonState_T=0,g_iButtonState_M=0;
//g是globle代表全局变量，会在其他地方引用；i代表int型变量，ButtonState表示按键标志，1代表按下，0代表松开

uint8_t mission_step = 1;
int16_t Encoder_cnt;
uint8_t CAR_Task = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void Go_To_Area_1(void);
void Go_To_Area_2(void);
void Go_To_Area_3(void);
void Go_To_Area_4(void);
void Go_To_Area_5(void);
void Go_To_Area_6(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM1_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_TIM10_Init();
  /* USER CODE BEGIN 2 */
	HAL_TIM_Base_Start_IT(&htim1);//打开定时器1中断
  HAL_TIM_Base_Start_IT(&htim4);//打开定时器4中断
  HAL_TIM_Base_Start_IT(&htim5);//打开定时器5中断
	HAL_TIM_Base_Start_IT(&htim10);//打开定时器10中断
  HAL_TIM_PWM_Start_IT(&htim1,TIM_CHANNEL_1);//打开PWM通道1中断
  HAL_TIM_PWM_Start_IT(&htim1,TIM_CHANNEL_2);//打开PWM通道2中断
	HAL_TIM_Encoder_Start(&htim2,TIM_CHANNEL_ALL);//打开编码器2捕获
	HAL_TIM_Encoder_Start(&htim3,TIM_CHANNEL_ALL);//打开编码器3捕获

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
//			HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin,GPIO_PIN_RESET);
		if(g_iButtonState_Button2 == 1)
	  {
		  HAL_GPIO_TogglePin(LED_GPIO_Port,LED_Pin);
		  g_iButtonState_Button2=0;
	  }
		 if(Run_Flag == 0)
		{
			if(g_iButtonState_Button1 == 1||g_iButtonState_Button2 == 1)
			{
				Run_Flag=1;
				if(g_iButtonState_Button1 == 1)
        {
					Chassis_mode  = 1;
        }
				if(g_iButtonState_Button2 == 1)
        {
          Back_mode = 1;
        }
          g_iButtonState_Button1=0;
          g_iButtonState_Button2=0;
			}
		}

		if(Chassis_mode)	//+90度P:565;		-90度P:559; 	8分米P:3273;		50cm 2s;	10cm 0.3s	拐90度 1.5S
    { 
			if(Clean_Flag==0)
			{
				Clear_All_Motor_Accumulated_Encoder_val();
				Clean_Task_Encoder_val();
				Clean_Flag = 1;
				Task1=1;
			}
/*******************************************************************************************************************/				

			
//			Motor_V(16,16);
//			Motor_P(2600,2600);

/*******************************************************************************************************************/				
//			if(Task1>=6000000)
//			{
//				Clean_Task_Encoder_val();
//				Motor_V(0,0);
//			}
/*******************************************************************************************************************/
			if(HAL_GPIO_ReadPin(Area1_GPIO_Port,Area1_Pin)==GPIO_PIN_SET&&
					HAL_GPIO_ReadPin(Area2_GPIO_Port,Area2_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area3_GPIO_Port,Area3_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area4_GPIO_Port,Area4_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area5_GPIO_Port,Area5_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area6_GPIO_Port,Area6_Pin)==GPIO_PIN_RESET)
			{
				Go_To_Area_1();
				
			}
			if(HAL_GPIO_ReadPin(Area1_GPIO_Port,Area1_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area2_GPIO_Port,Area2_Pin)==GPIO_PIN_SET&&
					HAL_GPIO_ReadPin(Area3_GPIO_Port,Area3_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area4_GPIO_Port,Area4_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area5_GPIO_Port,Area5_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area6_GPIO_Port,Area6_Pin)==GPIO_PIN_RESET)
			{
				Go_To_Area_2();
				
			}
			if(HAL_GPIO_ReadPin(Area1_GPIO_Port,Area1_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area2_GPIO_Port,Area2_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area3_GPIO_Port,Area3_Pin)==GPIO_PIN_SET&&
					HAL_GPIO_ReadPin(Area4_GPIO_Port,Area4_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area5_GPIO_Port,Area5_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area6_GPIO_Port,Area6_Pin)==GPIO_PIN_RESET)
			{
				Go_To_Area_3();
				
			}
			if(HAL_GPIO_ReadPin(Area1_GPIO_Port,Area1_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area2_GPIO_Port,Area2_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area3_GPIO_Port,Area3_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area4_GPIO_Port,Area4_Pin)==GPIO_PIN_SET&&
					HAL_GPIO_ReadPin(Area5_GPIO_Port,Area5_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area6_GPIO_Port,Area6_Pin)==GPIO_PIN_RESET)
			{
				Go_To_Area_4();
				
			}
			if(HAL_GPIO_ReadPin(Area1_GPIO_Port,Area1_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area2_GPIO_Port,Area2_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area3_GPIO_Port,Area3_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area4_GPIO_Port,Area4_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area5_GPIO_Port,Area5_Pin)==GPIO_PIN_SET&&
					HAL_GPIO_ReadPin(Area6_GPIO_Port,Area6_Pin)==GPIO_PIN_RESET)
			{
				Go_To_Area_5();
				
			}
			if(HAL_GPIO_ReadPin(Area1_GPIO_Port,Area1_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area2_GPIO_Port,Area2_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area3_GPIO_Port,Area3_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area4_GPIO_Port,Area4_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area5_GPIO_Port,Area5_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area6_GPIO_Port,Area6_Pin)==GPIO_PIN_SET)
			{
				Go_To_Area_6();
				
			}
			if(HAL_GPIO_ReadPin(Area1_GPIO_Port,Area1_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area2_GPIO_Port,Area2_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area3_GPIO_Port,Area3_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area4_GPIO_Port,Area4_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area5_GPIO_Port,Area5_Pin)==GPIO_PIN_RESET&&
					HAL_GPIO_ReadPin(Area6_GPIO_Port,Area6_Pin)==GPIO_PIN_RESET)
			{
					Task1=1;
			}
				
				
		}

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{	
	/*判断当进入定时器4中断时*/
	if(htim->Instance == TIM4)
	{
		    TimerCount++;   
    //串级PID20ms处理1次
    if(TimerCount >10)
    {
      L_Position_Incremental_PID();
      R_Position_Incremental_PID();
      TimerCount=0;
    }

	}
	/*判断当进入定时器5中断时*/
	if(htim->Instance == TIM5)
	{
		t_ms++;
		t_Run_Flag++;
		t_Debug_Flag++;
		t_ms_stop++;
		if(t_ms>=100)
		{
//			printf("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
//			L_Pwm,Reality_Position_L,Target_Position_L,Reality_Velocity_L,Target_Velocity_L,
//			R_Pwm,Reality_Position_R,Target_Position_R,Reality_Velocity_R,Target_Velocity_R);
			t_100ms++;
			t_ms=0;
			HAL_GPIO_TogglePin(LED_GPIO_Port,LED_Pin);
//			printf("hello world\n")
//			printf("%d,%d,%d,%d\n",encoder_Counter_L,encoder_Direction_L,encoder_Counter_R,encoder_Direction_R);
//			printf("误差为%d\n",ERROR);
		}


			
		
/*******************************************************************************************************************/
		if( HAL_GPIO_ReadPin(UKEY_GPIO_Port,UKEY_Pin) == GPIO_PIN_RESET )//如果引脚检测到          低电平
		{
			iButtonCount_UKEY++; //按键按下，计数iButtonCount加1
			if(iButtonCount_UKEY>=7) //1ms中断服务函数里运行一次，iButtonCount大于等于7，即按键已稳定按下7ms
			{
				if(iButtonFlag_UKEY==0) //判断有没有重按键，1为有，0为没有
				{
					g_iButtonState_UKEY=1; //设置按键标志
					iButtonCount_UKEY=0;
					iButtonFlag_UKEY=1; //设置重按键标志
				}
				else //如果重按键，则重新计数
					iButtonCount_UKEY=0;
			}
			else //如果没有稳定按下7ms，则代表没有按下按键
				g_iButtonState_UKEY=0;
		}
		else //如果一直无检测到低电平，即一直无按键按下
		{
			iButtonCount_UKEY=0; //清零iButtonCount
			g_iButtonState_UKEY=0; //清除按键标志
			iButtonFlag_UKEY=0; //清除重按键标志
		}
		if( HAL_GPIO_ReadPin(Button1_GPIO_Port,Button1_Pin) == GPIO_PIN_RESET )//如果引脚检测到          低电平
		{
			iButtonCount_Button1++; //按键按下，计数iButtonCount加1
			if(iButtonCount_Button1>=7) //1ms中断服务函数里运行一次，iButtonCount大于等于7，即按键已稳定按下7ms
			{
				if(iButtonFlag_Button1==0) //判断有没有重按键，1为有，0为没有
				{
					g_iButtonState_Button1=1; //设置按键标志
					iButtonCount_Button1=0;
					iButtonFlag_Button1=1; //设置重按键标志
				}
				else //如果重按键，则重新计数
					iButtonCount_Button1=0;
			}
			else //如果没有稳定按下7ms，则代表没有按下按键
				g_iButtonState_Button1=0;
		}
		else //如果一直无检测到低电平，即一直无按键按下
		{
			iButtonCount_Button1=0; //清零iButtonCount
			g_iButtonState_Button1=0; //清除按键标志
			iButtonFlag_Button1=0; //清除重按键标志
		}
		if( HAL_GPIO_ReadPin(Button1_GPIO_Port,Button2_Pin) == GPIO_PIN_RESET )//如果引脚检测到          低电平
		{
			iButtonCount_Button2++; //按键按下，计数iButtonCount加1
			if(iButtonCount_Button2>=7) //1ms中断服务函数里运行一次，iButtonCount大于等于7，即按键已稳定按下7ms
			{
				if(iButtonFlag_Button2==0) //判断有没有重按键，1为有，0为没有
				{
					g_iButtonState_Button2=1; //设置按键标志
					iButtonCount_Button2=0;
					iButtonFlag_Button2=1; //设置重按键标志
				}
				else //如果重按键，则重新计数
					iButtonCount_Button2=0;
			}
			else //如果没有稳定按下7ms，则代表没有按下按键
				g_iButtonState_Button2=0;
		}
		else //如果一直无检测到低电平，即一直无按键按下
		{
			iButtonCount_Button2=0; //清零iButtonCount
			g_iButtonState_Button2=0; //清除按键标志
			iButtonFlag_Button2=0; //清除重按键标志
		}
//		if( HAL_GPIO_ReadPin(Button3_GPIO_Port,Button3_Pin) == GPIO_PIN_SET )//如果引脚检测到          高电平
//		{
//			iButtonCount_Button3++; //按键按下，计数iButtonCount加1
//			if(iButtonCount_Button3>=7) //1ms中断服务函数里运行一次，iButtonCount大于等于7，即按键已稳定按下7ms
//			{
//				if(iButtonFlag_Button3==0) //判断有没有重按键，1为有，0为没有
//				{
//					g_iButtonState_Button3=1; //设置按键标志
//					iButtonCount_Button3=0;
//					iButtonFlag_Button3=1; //设置重按键标志
//				}
//				else //如果重按键，则重新计数
//					iButtonCount_Button3=0;
//			}
//			else //如果没有稳定按下7ms，则代表没有按下按键
//				g_iButtonState_Button3=0;
//		}
//		else //如果一直无检测到低电平，即一直无按键按下
//		{
//			iButtonCount_Button3=0; //清零iButtonCount
//			g_iButtonState_Button3=0; //清除按键标志
//			iButtonFlag_Button3=0; //清除重按键标志
//		}
	}
		/*判断当进入定时器10中断时*/
	if(htim->Instance == TIM10)
	{
	    Task1++;
	}
}

/*******************去区域一，前进65，左拐90度，前进30**************/
void Go_To_Area_1()
{
	//去区域一，前进65，左拐90度，前进30
	if(Task1>=200&&mission_step==1)	//前进 70
	{
		Encoder_cnt = 2800;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=3000&&mission_step==2)	//左拐90度
	{
		Encoder_cnt = 565;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L + Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=4500&&mission_step==3)	//前进 30
	{
		Encoder_cnt = 1200;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
			HAL_GPIO_WritePin(Laser_GPIO_Port,Laser_Pin,GPIO_PIN_SET);
		}
	}
//回家
	if(Task1>=6000+5000&&mission_step==4)	//后退 30
	{
		HAL_GPIO_WritePin(Laser_GPIO_Port,Laser_Pin,GPIO_PIN_RESET);
		Encoder_cnt = -1200;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R +Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
			
		}
	}
	if(Task1>=7500+5000&&mission_step==5)	//右拐90度
	{
		Encoder_cnt = 520;
		Motor_V(6,6);
		Motor_P(Encoder_cnt,-Encoder_cnt);
		if(((Target_Position_L -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=9000+5000&&mission_step==6)	//后退 70
	{
		Encoder_cnt = -2800;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R +Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
}


/*******************去区域二，前进70，右拐90度，前进50,前进50**************/
void Go_To_Area_2(void)
{
	if(Task1>=200&&mission_step==1)	//前进 70
	{
		Encoder_cnt = encoder * 70;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}

	if(Task1>=3000&&mission_step==2)	//右拐90度
	{
		Encoder_cnt = 515;
		Motor_V(6,6);
		Motor_P(Encoder_cnt,-Encoder_cnt);
		if(((Target_Position_L -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=4500&&mission_step==3)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=6000&&mission_step==4)	//fix
	{
		Motor_V(6,6);
		Motor_P(-10,10);
		if(((Target_Position_L +10) < 1) && ((Target_Position_R -10) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	if(Task1>=6500&&mission_step==5)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
			HAL_GPIO_WritePin(Laser_GPIO_Port,Laser_Pin,GPIO_PIN_SET);
		}
	}
	//回家
	if(Task1>=18500+5000&&mission_step==6)	//右拐90度
	{
		HAL_GPIO_WritePin(Laser_GPIO_Port,Laser_Pin,GPIO_PIN_RESET);
		Encoder_cnt = 515;
		Motor_V(6,6);
		Motor_P(Encoder_cnt,-Encoder_cnt);
		if(((Target_Position_L -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=20000+5000&&mission_step==7)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	if(Task1>=21500+5000&&mission_step==8)	//右拐90度
	{
		Encoder_cnt = 515;
		Motor_V(6,6);
		Motor_P(Encoder_cnt,-Encoder_cnt);
		if(((Target_Position_L -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;     
		}
	}
	if(Task1>=23000+5000&&mission_step==9)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=24500+5000&&mission_step==10)	//fix
	{
		Motor_V(6,6);
		Motor_P(-10,10);
		if(((Target_Position_L +10) < 1) && ((Target_Position_R -10) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	if(Task1>=25000+5000&&mission_step==11)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
}

/*******************去区域三，前进65，右拐90度，前进100**************/
void Go_To_Area_3(void)
{
	if(Task1>=60500&&mission_step==1)	//前进 70     mission_step
	{
		Encoder_cnt = encoder * 70;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}

	if(Task1>=63000&&mission_step==2)	//右拐90度 && ((Target_Position_R +Encoder_cnt) < 1)
	{
		Encoder_cnt = 515;
		Motor_V(6,6);
		Motor_P(Encoder_cnt,-Encoder_cnt);
		if(((Target_Position_L -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=64500&&mission_step==3)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=66000&&mission_step==4)	//fix
	{
		Motor_V(6,6);
		Motor_P(-10,10);
		if(((Target_Position_L +10) < 1) && ((Target_Position_R -10) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=66500&&mission_step==5)	//前进55
	{
		Encoder_cnt = 40*55;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=68000&&mission_step==6)	//fix
	{
		Motor_V(6,6);
		Motor_P(-10,10);
		if(((Target_Position_L +10) < 1) && ((Target_Position_R -10) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=68500&&mission_step==7)	//前进55
	{
		Encoder_cnt = 40*55;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=70000&&mission_step==8)	//fix
	{
		Encoder_cnt = 12;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=70500&&mission_step==9)	//前进55
	{
		Encoder_cnt = 55*40;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=72000&&mission_step==10)	//左拐90度
	{
		Encoder_cnt = ACW;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L + Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
			HAL_GPIO_WritePin(Laser_GPIO_Port,Laser_Pin,GPIO_PIN_SET);
		}
	}
	
//回家
	
	if(Task1>=82000+5000&&mission_step==11)	//后退50
	{
		HAL_GPIO_WritePin(Laser_GPIO_Port,Laser_Pin,GPIO_PIN_RESET);
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(-Encoder_cnt,-Encoder_cnt);	
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R +Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	if(Task1>=83500+5000&&mission_step==12)	//左拐90度
	{
		Encoder_cnt = 530;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L + Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=86000+5000&&mission_step==13)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) &&((Target_Position_R -Encoder_cnt) < 1) ) //
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=87500+5000&&mission_step==14)	//fix  
	{
		Encoder_cnt =50;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	if(Task1>=88000+5000&&mission_step==15)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=89500+5000&&mission_step==16)	//fix
	{				
		Encoder_cnt = 50;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	
	if(Task1>=90000+5000&&mission_step==17)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=91500+5000&&mission_step==18)	//fix
	{
		Encoder_cnt = 20;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	
	if(Task1>=92000+5000&&mission_step==19)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}	
}

/*******************去区域四*************/
void Go_To_Area_4(void)
{
	if(Task1>=500&&mission_step==1)	//前进 70     mission_step
	{
		Encoder_cnt = encoder * 70;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}

	if(Task1>=3000&&mission_step==2)	//右拐90度 && ((Target_Position_R +Encoder_cnt) < 1)
	{
		Encoder_cnt = 515;
		Motor_V(6,6);
		Motor_P(Encoder_cnt,-Encoder_cnt);
		if(((Target_Position_L -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=4500&&mission_step==3)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=6000&&mission_step==4)	//fix
	{
		Encoder_cnt = 5;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=6500&&mission_step==5)	//前进55
	{
		Encoder_cnt = 40*55;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=8000&&mission_step==6)	//fix
	{
		Encoder_cnt = 5;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=8500&&mission_step==7)	//前进55
	{
		Encoder_cnt = 40*55;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=10000&&mission_step==8)	//fix
	{
		Encoder_cnt = 5;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=10500&&mission_step==9)	//前进55
	{
		Encoder_cnt = 55*40;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=12000&&mission_step==10)	//左拐90度
	{
		Encoder_cnt = ACW;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L + Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}

	if(Task1>=13500&&mission_step==11)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step+=1;    
		}
	}
	
	if(Task1>=15000&&mission_step==12)	//fix  
	{
		Encoder_cnt =15;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	
	if(Task1>=15500&&mission_step==13)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) &&((Target_Position_R -Encoder_cnt) < 1) ) //
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step+=1;
		}
	}
	if(Task1>=17000&&mission_step==14)	//fix  
	{
		Encoder_cnt =15;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	if(Task1>=17500&&mission_step==15)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step+=1;   
		}
	}
	
	if(Task1>=19000&&mission_step==16)	//fix
	{				
		Encoder_cnt = 15;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	
	if(Task1>=19500&&mission_step==17)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=21000&&mission_step==18)	//fix
	{
		Encoder_cnt = 15;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++; 
			HAL_GPIO_WritePin(Laser_GPIO_Port,Laser_Pin,GPIO_PIN_SET);
	
		}
	}
//回家
	if(Task1>=26000+5000&&mission_step==19)	//左拐180度
	{
		HAL_GPIO_WritePin(Laser_GPIO_Port,Laser_Pin,GPIO_PIN_RESET);
		Encoder_cnt = ACW*2;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L + Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	
	if(Task1>=29000+5000&&mission_step==20)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=30500+5000&&mission_step==21)	//fix
	{
		Encoder_cnt = 15;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	if(Task1>=31000+5000&&mission_step==22)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=32500+5000&&mission_step==23)	//fix
	{
		Encoder_cnt = 15;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	if(Task1>=33000+5000&&mission_step==24)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=34500+5000&&mission_step==25)	//fix
	{
		Encoder_cnt = 15;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	if(Task1>=35000+5000&&mission_step==26)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=36500+5000&&mission_step==27)	//fix
	{
		Encoder_cnt = 15;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	if(Task1>=37000+5000&&mission_step==28)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=38500+5000&&mission_step==29)	//fix
	{
		Encoder_cnt = 15;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}

	if(Task1>=39000+5000&&mission_step==30)	//右拐90度
	{
		Encoder_cnt = CW;
		Motor_V(6,6);
		Motor_P(Encoder_cnt,-Encoder_cnt);
		if(((Target_Position_L - Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}	
	
	if(Task1>=40500+5000&&mission_step==31)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=42000+5000&&mission_step==32)	//fix
	{
		Encoder_cnt = 15;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	
	if(Task1>=42500+5000&&mission_step==33)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=44000+5000&&mission_step==34)	//fix
	{
		Encoder_cnt = 15;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	
	if(Task1>=44500+5000&&mission_step==35)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=46000+5000&&mission_step==36)	//fix
	{
		Encoder_cnt = 15;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);  //1->0.16°
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
		}
	}
	
	if(Task1>=47500+5000&&mission_step==37)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	
	if(Task1>=49000+5000&&mission_step==38)	//左拐90度
	{
		Encoder_cnt = ACW;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L + Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	
	if(Task1>=50500+5000&&mission_step==39)	//前进30
	{
		Encoder_cnt = 40*30;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
}
/*******************去区域五，右拐90度，前进35，左拐90度，前进200，右拐90度，前进35，左拐90度，前进60**************/
void Go_To_Area_5(void)
{
	if(Task1>=200&&mission_step==1)	//右拐90度     mission_step
	{
		Encoder_cnt = 559;
		Motor_V(6,6);
		Motor_P(Encoder_cnt,-Encoder_cnt);
		if((Target_Position_L -Encoder_cnt) < 1)
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}

	if(Task1>=2000&&mission_step==2)	//前进35
	{
		Encoder_cnt = 40*35;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=3500&&mission_step==3)	//左拐90度
	{
		Encoder_cnt = 565;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if((Target_Position_L +Encoder_cnt) < 1)
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=5000&&mission_step==4)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=7000&&mission_step==5)	//fix
	{
		Encoder_cnt = 10;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=7500&&mission_step==6)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=9500&&mission_step==7)	//fix
	{
		Encoder_cnt = 10;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=10000&&mission_step==8)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=12000&&mission_step==9)	//fix
	{
		Encoder_cnt = 10;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=12500&&mission_step==10)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	
	if(Task1>=15500&&mission_step==11)	//右拐90度
	{
		Encoder_cnt = 515;
		Motor_V(6,6);
		Motor_P(Encoder_cnt,-Encoder_cnt);
		if((Target_Position_L -Encoder_cnt) < 1)
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=17000&&mission_step==12)	//前进37
	{
		Encoder_cnt = 1480;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=18500&&mission_step==13)	//左拐90度
	{
		Encoder_cnt = 570;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if((Target_Position_L +Encoder_cnt) < 1)
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=20000&&mission_step==14)	//前进70
	{
		Encoder_cnt = 2800;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
			HAL_GPIO_WritePin(Laser_GPIO_Port,Laser_Pin,GPIO_PIN_SET);
		}
	}
	//回库
	if(Task1>=23000+5000&&mission_step==15)	//后退70
	{
		HAL_GPIO_WritePin(Laser_GPIO_Port,Laser_Pin,GPIO_PIN_RESET);
		Encoder_cnt = -2800;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L +Encoder_cnt) < 1)&&((Target_Position_R +Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=26000+5000&&mission_step==16)	//左拐90度
	{
		Encoder_cnt = 580;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if((Target_Position_L +Encoder_cnt) < 1)
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=27500+5000&&mission_step==17)	//前进37
	{
		Encoder_cnt = 1480;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=29000+5000&&mission_step==18)	//左拐90度
	{
		Encoder_cnt = 565;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if((Target_Position_L +Encoder_cnt) < 1)
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=30500+5000&&mission_step==19)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=32500+5000&&mission_step==20)	//fix
	{
		Encoder_cnt = 10;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=33000+5000&&mission_step==21)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=35000+5000&&mission_step==22)	//fix
	{
		Encoder_cnt = 10;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=35500+5000&&mission_step==23)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=37500+5000&&mission_step==24)	//fix
	{
		Encoder_cnt = 10;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=38000+5000&&mission_step==25)	//前进30
	{
		Encoder_cnt = 1200;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
		if(Task1>=40000+5000&&mission_step==26)	//右拐90度
	{
		Encoder_cnt = 515;
		Motor_V(6,6);
		Motor_P(Encoder_cnt,-Encoder_cnt);
		if((Target_Position_L -Encoder_cnt) < 1)
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
		if(Task1>=41500+5000&&mission_step==27)	//前进35
	{
		Encoder_cnt = 40*35;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}

}

/*******************去区域六，右拐90度，前进35，左拐90度，前进200，左拐90度，前进50**************/
void Go_To_Area_6(void)
{
	if(Task1>=200&&mission_step==1)	//右拐90度     mission_step
	{
		Encoder_cnt = 559;
		Motor_V(6,6);
		Motor_P(Encoder_cnt,-Encoder_cnt);
		if((Target_Position_L -Encoder_cnt) < 1)
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}

	if(Task1>=2000&&mission_step==2)	//前进35
	{
		Encoder_cnt = 40*35;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=3500&&mission_step==3)	//左拐90度
	{
		Encoder_cnt = 565;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if((Target_Position_L +Encoder_cnt) < 1)
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=5000&&mission_step==4)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=7000&&mission_step==5)	//fix
	{
		Encoder_cnt = 10;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=7500&&mission_step==6)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=9500&&mission_step==7)	//fix
	{
		Encoder_cnt = 10;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=10000&&mission_step==8)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=12000&&mission_step==9)	//fix
	{
		Encoder_cnt = 10;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	if(Task1>=12500&&mission_step==10)	//前进65
	{
		Encoder_cnt = 2600;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=14500&&mission_step==11)	//左拐90度
	{
		Encoder_cnt = 570;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if((Target_Position_L +Encoder_cnt) < 1)
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=16000&&mission_step==12)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;  
			HAL_GPIO_WritePin(Laser_GPIO_Port,Laser_Pin,GPIO_PIN_SET);	
		}
	}
//回家
	if(Task1>=18000+5000&&mission_step==13)	//后退50
	{
		HAL_GPIO_WritePin(Laser_GPIO_Port,Laser_Pin,GPIO_PIN_RESET);		
		Encoder_cnt = -2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L +Encoder_cnt) < 1)&&((Target_Position_R +Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
	if(Task1>=20000+5000&&mission_step==14)	//左拐90度
	{
		Encoder_cnt = 570;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if((Target_Position_L +Encoder_cnt) < 1)
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}
	if(Task1>=21500+5000&&mission_step==15)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=23500+5000&&mission_step==16)	//fix
	{
		Encoder_cnt = 10;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=24000+5000&&mission_step==17)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=26000+5000&&mission_step==18)	//fix
	{
		Encoder_cnt = 10;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=26500+5000&&mission_step==19)	//前进50
	{
		Encoder_cnt = 2000;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;    
		}
	}
	
	if(Task1>=28500+5000&&mission_step==20)	//fix
	{
		Encoder_cnt = 10;
		Motor_V(6,6);
		Motor_P(-Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L +Encoder_cnt) < 1) && ((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
		mission_step++;   
		}
	}
	
	if(Task1>=29000+5000&&mission_step==21)	//前进40
	{
		Encoder_cnt = 1600;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);	
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
		if(Task1>=30500+5000&&mission_step==22)	//右拐90度
	{
		Encoder_cnt = 515;
		Motor_V(6,6);
		Motor_P(Encoder_cnt,-Encoder_cnt);
		if((Target_Position_L -Encoder_cnt) < 1)
		{
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;   
		}
	}
		if(Task1>=32000+5000&&mission_step==23)	//前进35
	{
		Encoder_cnt = 40*35;
		Motor_V(16,16);
		Motor_P(Encoder_cnt,Encoder_cnt);
		if(((Target_Position_L -Encoder_cnt) < 1)&&((Target_Position_R -Encoder_cnt) < 1))
		{ 
			Clean_Task_Encoder_val();
			Clear_All_Motor_Accumulated_Encoder_val();
			mission_step++;
		}
	}

}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
