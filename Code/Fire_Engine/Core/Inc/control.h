#ifndef __CONTROL_H_
#define __CONTROL_H_

#include "main.h"
#include "motor.h"
#include "math.h"

//电机位置示意图
//1 L  2 R

extern int L_Pwm,R_Pwm;

//L
extern int Target_Velocity_L,Reality_Velocity_L;   
extern int Target_Position_L,Reality_Position_L;   
void L_Position_Incremental_PID(void);
int L_Position_PID(int reality,int target);
int L_Incremental_PID(int reality,int target);

//R
extern int Target_Velocity_R,Reality_Velocity_R;   
extern int Target_Position_R,Reality_Position_R;   
void R_Position_Incremental_PID(void);
int R_Position_PID(int reality,int target);
int R_Incremental_PID(int reality,int target);



void Go_Forward(int16_t unit);
void Go_Back(int16_t unit);
void Turn_Left_90(int16_t unit);
void Turn_Right_90(int16_t unit);
void Turn_fix_Left(int16_t unit);
void Turn_fix_Right(int16_t unit);



//PID处理函数
int Xianfu(int value,int Amplitude);
int Abs(int a);
#endif
