#ifndef __MOTOR_H_
#define __MOTOR_H_

#include "main.h"
#include "tim.h"
#include "control.h"
#include "math.h"

//进行PWM滤波
#define MAX_Speed 1000
#define min_speed 0

#define MIN_Speed 0

//电机位置示意图
//1 L  2 R

//编码器线数
#define PPR   11.0
//减速比
#define RATIO 18.0
//倍频数
#define BP    4.0

//这些是可以使用的函数
void motor_pwm(uint8_t motor,int16_t pwm);
void motor_1khz(int16_t motor_L,int16_t motor_R);
void Clean_All_Motor_Pwm(void);
void Clean_Task_Encoder_val(void);
void Clear_All_Motor_Accumulated_Encoder_val(void);
void Motor_P(int16_t MotorP_L,int16_t MotorP_R);
void Motor_V(int16_t MotorV_T,int16_t MotorV_R);

//下面的函数在上面的函数中调用
short Read_Speed_Encoder(uint8_t TIMX);
short Read_Position_Encoder(uint8_t TIMX);

#endif
