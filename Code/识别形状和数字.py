# 判断颜色和形状 - By: gorgeous - 周四 7月 28 2022

import sensor
import image
import time
import network
import usocket
import sys
import sensor
import image
import time
import network
import usocket
import sys
import math
from pyb import UART
from pyb import LED

# 变量分配区
# 绿色追踪
threshold1 = (22, 46, 24, 73, -5, 51) #red
threshold2 = (10, 33, 1, 49, -49, -10) #blue
Frame_Cnt = 0
fCnt_tmp = [0, 0]
count = 0
rangefinder = 0
# 局部函数定义区

red_led = LED(1)
green_led = LED(2)
blue_led = LED(3)
ir_led = LED(4)

def find_max(blobs):
    max_size=0
    for blob in blobs:
        if blob[2]*blob[3] > max_size:
            max_blob=blob
            max_size = blob[2]*blob[3]
    return max_blob

def get_circle(img,r):
    circles = img.find_circles(threshold=4000, x_margin=100,y_margin=100, r_margin=10, r_min=r, r_max=100, r_step=2)
    if circles:
        #green_led.on()
        for circle_ in circles:
            img.draw_circle(circle_.x(), circle_.y(),
                            circle_.r()) #画圆
            img.draw_cross(circle_.x(), circle_.y())        #画十字
            P0 = circle_.x()
            P1 = circle_.y()
            Type = 200
    else:
        #print("no find circle")
        #green_led.off()
        Type = 0xFF
        P0 = -1
        P1 = -1

    return Type,P0,P1
    #print(Type, P0, P1)

def get_rectangle(img):
    type = 0xFF
    rectangles = img.find_rects(threshold = 25000)
    for r in rectangles :
        #green_led.on()
        img.draw_rectangle(r.rect(), color = (0, 255, 0))
        for p in r.corners(): img.draw_circle(p[0], p[1], 5, color = (0, 255, 0))
        type = 202
        #print(r)
    return type

def get_red(img):
    blobs = img.find_blobs([threshold1], pixels_threshold=100,
                           area_threshold=100, merge=True, margin=10)
    if blobs:
           # red_led.on()
        #for blob in blobs:  #blobs是一个列表
            blob = find_max(blobs)
            img.draw_rectangle(blob.rect(),color=(255,0,0))
            img.draw_cross(blob.cx(), blob.cy())
            max_size = blob[2]*blob[3]
            Type = 100
            if max_size > 500:
              P0 = blob.cx()
              P1 = blob.cy()
            else:
              Type = 155
              P0 = 1
              P1 = 1
            #print(max_size)
    else:
        #print("no find circle")
       # red_led.off()
        Type = 155
        P0 = -1
        P1 = -1
        max_size = 6
    return Type,P0,P1,max_size

def get_blue(img):
    blobs = img.find_blobs([threshold2], pixels_threshold=100,
                           area_threshold=100, merge=True, margin=10)
    if blobs:
           # blue_led.on()
        #for blob in blobs:  #blobs是一个列表
            blob = find_max(blobs)
            img.draw_rectangle(blob.rect(),color=(0,0,255))
            img.draw_cross(blob.cx(), blob.cy())
            Type = 102
            max_size = blob[2]*blob[3]
            if max_size > 500:
              P0 = blob.cx()
              P1 = blob.cy()
            else:
              Type = 155
              P0 = 1
              P1 = 1

            #print(Type, P0, P1)
    else:
        #print("no find circle")
       # blue_led.off()
        Type = 155
        P0 = -1
        P1 = -1
        max_size = 6

    return Type,P0,P1,max_size

def get_rangefinder(uart_line):#与激光通信
    try:
        if (len(uart_line) < 8):
            return 0xFF
        if(uart_line[0] != 0x59):
            return 0xFF
        if(uart_line[1] != 0x59):
            return 0xFF
        if(uart_line[2] != None and uart_line[3] != None):
            return uart_line[3] * 256 | uart_line[2]
    except:
        return 20
        pass

def ExceptionVar(var):
    data = []
    data.append(0)
    data.append(0)

    if var == -1:
        data[0] = 0
        data[1] = 0
    else:
        data[0] = var & 0xFF
        data[1] = var >> 8
    return data

# 串口3 P4 P5 = TX RX
# 串口1 P1 P2 = TX RX     P0  RX


def UART_Send(FormType, Loaction0, Location1, range_finder=0):
    global Frame_Cnt
    global fCnt_tmp
    Frame_Head = [170, 170]
    Frame_End = [85, 85]
    fFormType_tmp = [FormType]
    Frame_Cnt += 1

    if Frame_Cnt > 65534:
        FrameCnt = 0

    fHead = bytes(Frame_Head)

    fCnt_tmp[0] = range_finder & 0xFF
    fCnt_tmp[1] = range_finder >> 8
    # if(range_finder != 0):
    #     fCnt_tmp[1] = range_finder
    fCnt = bytes(fCnt_tmp)

    fFormType = bytes(fFormType_tmp)
    fLoaction0 = bytes(ExceptionVar(Loaction0))
    fLoaction1 = bytes(ExceptionVar(Location1))
    fEnd = bytes(Frame_End)
    FrameBuffe = fHead + fCnt + fFormType + fLoaction0 + fLoaction1 + fEnd
    return FrameBuffe
sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QQVGA)
sensor.skip_frames(time=2000)
sensor.set_auto_gain(False)  # must be turned off for color tracking
sensor.set_auto_whitebal(False)  # must be turned off for color tracking
#sensor.set_hmirror(True)
#sensor.set_vflip(True)
clock = time.clock()
uart = UART(3, 115200)
uart_Rangefinder = UART(1, 115200)
TYPE = 0  #判断红色三角形
TYPE1 = 0 #判断红色蓝色
TYPE3 = 0 #判断红色蓝色
L1 = -1
L2 = -1
A1 = 2
A2 = 2
j = 0
h = True
#CountDown =1000
Find_blob = False
while(True):
   clock.tick()
   #CountDown -= 1
   img = sensor.snapshot()
   (Type1,C0,C1) = get_circle(img,40) #20 圆的阈值
   Type2=get_rectangle(img)

   if Type1 == 0xFF and Type2 == 0xFF and j == 0:
       Type3 = 204  #三角形
   else :
      Type3 = 0xFF #255

   (type1,R1,R2,size1)= get_red(img)
   (type2,B1,B2,size2)= get_blue(img)

    #print("red",size1)
    #print("blue",size2)
   if Type1 == 200 and Type2 == 202 :
       Type2 = 255     #圆矩形 则是圆  矩形为中间，圆形识别不到
   blob = 10
   while(h):
    if (Type1 == 200) :
       if type1 == 100:
          TYPE = 300    #红色圆形
          L1 = C0       #中心坐标
          L2 = C1
          A1 = 6        #所对应数字
          A2 = 5
          red_led.on()  #红灯
          TYPE1 = 202   #红色
       if type2 == 102:
          TYPE = 302    #蓝色圆形
          L1 = C0
          L2 = C1
          A1 = 7
          A2 = 8
          blue_led.on() #蓝灯
          TYPE1 = 204   #蓝色
    if Type2 == 202 or j == 1:
       if type1 == 100:
          TYPE = 304    #红色矩形
          L1 = R1
          L2 = R2
          A1 = 9
          A2 = 10
          j = 1
          green_led.on()#白灯
          blue_led.on()
          red_led.on()
          TYPE1 = 202
       if type2 == 102:
          TYPE = 306    #蓝色矩形
          L1 = B1
          L2 = B2
          A1 = 12
          A2 = 11
          j = 1
          blue_led.on()
          green_led.on() #蓝绿灯
          TYPE1 = 204
    if Type3 == 204:
       if type1 == 100:
          TYPE = 308    #红色三角形
          L1 = R1
          L2 = R2
          A1 = 2
          A2 = 1
          green_led.on() #黄灯
          red_led.on()
          TYPE1 = 202
       if type2 == 102:
          TYPE = 310    #蓝色三角形
          L1 = B1
          L2 = B2
          A1 = 4
          A2 = 3
          red_led.on()  #紫灯
          blue_led.on()
          TYPE1 =204
    if TYPE!= 0 :
       blob -=1
    if blob <=0:
       h = False
       break

   TYPE2 = 200
   print(TYPE2,A1,A2)
    #print("ok")
   TYPE1 = 0
   uart.write(UART_Send(TYPE2, A1, A2))

   #if TYPE!= 0 :
     #  h = 1
    #print (TYPE,L1,L2)
    #print(clock.fps())
    #if CountDown <= 5:
       #uart.write(UART_Send(TYPE2, A1, A2,TYPE1))
       #print(TYPE2,A1,A2,TYPE1)
    #if CountDown <= 0:
        #red_led.off()
        #green_led.off()
       # blue_led.off()
        #Find_blob = True
        #break

    #print (Type1,C0,C1)   #圆  200   白色圈圈 中心十字
    #print (Type2)         #矩形 202  绿色框框 和四个角
    #print (Type3)         #三角形 204
    #print (type1,R1,R2)   #红色 100  红色框框  中心十字
    #print (type2,B1,B2)   #蓝色 102 蓝色框框  中心十字
    #300 红色圆形 302蓝色圆形 304红色矩形 306蓝色矩形
    #308 红色三角形 310蓝色三角形
if Find_blob :
   while(True):
    clock.tick()
    img = sensor.snapshot()

    (type1,R1,R2,size1)= get_red(img)
    (type2,B1,B2,size2)= get_blue(img)

    if type1 == 100: #红色
       TYPE3 = 202
       L1 = R1
       L2 = R2
       red_led.on()
    else:
       red_led.off()

    if type2 == 102: #蓝色
       TYPE3 = 204
       L1 = B1
       L2 = B2
       blue_led.on()
    else:
       blue_led.off()

    #print("red",size1)
    #print("blue",size2)

    print (TYPE3,L1,L2)
    #print ("yes")
    #print(clock.fps())
    uart.write(UART_Send(TYPE3, L1, L2,0))














