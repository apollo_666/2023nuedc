import time, image,sensor,math,pyb,ustruct
from image import SEARCH_EX, SEARCH_DS
from pyb import UART,delay
from pyb import Pin, Timer,LED
sensor.reset()
sensor.set_contrast(3)
sensor.set_gainceiling(16)
sensor.set_pixformat(sensor.GRAYSCALE)
sensor.set_framesize(sensor.QVGA)
sensor.set_windowing((320, 240))
sensor.set_vflip(True)
sensor.set_hmirror(True)
sensor.set_auto_gain(False, value=100)
uart = UART(3, 115200, timeout_char = 1000)
blue_led = LED(3)
kpts1 = image.load_descriptor("/desc.orb")
clock = time.clock()
LoR=0
rx_buff=[]
state = 0
tx_flag = 0
Target_Num=0
def Receive_Prepare(data):
    global state
    global tx_flag
    if state==0:
        if data == 0x2C:#帧头
            state = 1
        else:
            state = 0
            rx_buff.clear()
    if state==1:
        if data == 0x1C:#帧头
            state = 2
        else:
            state = 0
            rx_buff.clear()
    elif state==2:
        rx_buff.append(data)
        state = 3
        int.from_bytes(rx_buff[0])
        global Target_Num
        Target_Num=rx_buff[0]
    elif state==3:
        rx_buff.append(data)
        state = 4
        int.from_bytes(rx_buff[1])
    elif state == 4:
        if data == 0x5B:
            tx_flag = int(rx_buff[0])
            state = 4
    else:
        state = 0
        rx_buff.clear()
while True:
    clock.tick()
    img = sensor.snapshot()
    kpts2 = img.find_keypoints(max_keypoints=150, threshold=10, normalized=True)
    if (kpts2):
        match = image.match_descriptor(kpts1, kpts2, threshold=100)
        if (match.count()>15):
            LoR=3
            FH = bytearray([0x2C,0x12,0x00, LoR, 0x5B])
            uart.write(FH)
            print("Stop")
            LED(2).on()
            delay(2000)
            LED(2).off()
            delay(3000)

    if(uart.any()>0):
        c=uart.readchar()
        Receive_Prepare(c)

    pyb.usb_mode('CDC+MSC')
