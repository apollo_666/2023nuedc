#ifndef __DRV_MY_OPENMV_H
#define __DRV_MY_OPENMV_H

//==����
#include "SysConfig.h"


typedef struct
{
	//
	u8 color_flag;
	u8 sta;
	s16 pos_x;
	s16 pos_y;
	u8 dT_ms;

}_openmv_color_block_st;

typedef struct
{
	//
	u8 sta;	
	s16 angle;
	s16 deviation;
	u8 p_flag;
	s16 pos_x;
	s16 pos_y;
	u8 dT_ms;

}_openmv_line_tracking_st;

typedef struct
{
	s16 openmv_data[100];
	s16 openmv_sta;
	
	u16 posX;
	u16 posY;
	u16 posX_A;
	u16 posY_A;
	u8 state;
	u8 state_A;
	u16 angle;
	u16 angle_A;
	u16 angle_A_out;
	u16 angle_out;
	
	u8 offline;
	u8 mode_cmd;
	u8 mode_sta;
	
	u8 en;
	u8 height_flag;
	u8 reset_flag;
	
	_openmv_color_block_st cb;
	_openmv_line_tracking_st lt;
} openmv_st;

extern openmv_st openmv;

void OPENMV_GetOneByte(uint8_t data);
static void OPENMV_DataAnl(uint8_t *data, uint8_t len1);

#endif

