#include "User_Task.h"
#include "Drv_RcIn.h"
#include "LX_FC_Fun.h"
#include "Ano_Math.h"
#include "Drv_Uart.h"
#include "Drv_MY_OpenMv.h"
#include "Math.h"
 
s16 cal_X,cal_Y;
s16 cal_X_2,cal_Y_2;
s16 cal_X_3,cal_Y_3;

u8 offset_Distance;

s16 Second_Distance;
s16 Second_angle;

s16 PLANE_YAW;   //飞机当前航向角,每100ms自动获取数值
s32 Ano_Alt;   //飞机对地高度,自动获取数值,单位cm
s32 POS_X,POS_Y;
s16 angle_bis;     //角度补偿的角度误差

s16 Dcx = 0,Dcy = 0,Dcx_A = 0,Dcy_A = 0; //通用OPENMV返回坐标XY偏差值

float rate;

u8 Num_1=3;
u8 Num_2=10;
                   // 0    1    2    3    4    5    6    7    8    9   10   11   12 
s16 position_X[13] = {0,  50, 200, 275-30 , 350, 350, 275, 125, 125,  50, 200, 200, 350};           //目标点 与起点 X轴距离  
s16 position_Y[13] = {0, 275, 125, 200, -25, 275,  50,  50, 200, 125, -25, 275, 125};           //目标点 与起点 y轴距离
	                  // 0    1    2    3*   4    5    6    7    8    9*  10   11   12 
s16 position_dis[13] = {0, 280, 236, 265, 351, 445, 279, 135, 236, 125, 202, 340, 371};         //目标点 与起点距离  0表示起点
s16 position_ang[13] = {0,  10,  58,  64,  94,  52,  80,  68,  32,  22,  97,  36,  70};         //目标点 与起点角度

							 //       0    1    2    3*   4    5    6    7    8    9*  10   11   12 
s16 position_dis_GoHome[13] = {0, 280, 236, 280-20, 351, 445, 279, 135, 236, 125, 202-2, 340, 371};         //目标点 与起点距离  0表示起点
s16 position_ang_GoHome[13] = {0,  10+180,  58+180,  64+180,  94+180,  52+180,  80+180,  68+180,  32+180,  22+180,  97+180-6,  36+180,  70+180};         //目标点 与起点角度
							 //		 0       1        2    		3*  	 4    		5   	  6        7        8        9*      10        11       12 
void UserTask_OneKeyCmd(void)
{
    //////////////////////////////////////////////////////////////////////
	static u8 one_key_takeoff_f = 1, one_key_land_f = 1, one_key_mission_f = 0;

    //判断有遥控信号才执行
    if (rc_in.no_signal == 0)
    {
        //判断第5通道拨杆位置 800<CH_6<1200
        if (rc_in.rc_ch.st_data.ch_[ch_5_aux1] > 800 && rc_in.rc_ch.st_data.ch_[ch_5_aux1] < 1200)
        {
            //还没有执行
            if (one_key_land_f == 0)
            {
                //标记已经执行
                one_key_land_f =
                    //执行一键降落
                    OneKey_Land();
            }
        }
        else
        {
            //复位标记，以便再次执行
            one_key_land_f = 0;
        }
	//判断第5通道拨杆位置 1700<CH_6<2200 
		if(rc_in.rc_ch.st_data.ch_[ch_5_aux1]>1700 && rc_in.rc_ch.st_data.ch_[ch_5_aux1]<2200)
		{
			//还没有执行
			if(one_key_mission_f ==0)
			{
				//标记已经执行
				one_key_mission_f = 1;
				//开始流程
				mission_step = 1;
			}
		}
		else
		{
			//复位标记，以便再次执行
			one_key_mission_f = 0;		
		}
		//
		if(one_key_mission_f==1)
		{
			static u16 time_dly_cnt_ms;
			//
			switch(mission_step)
			{
				case 0:
				{
					//reset
					time_dly_cnt_ms = 0;
				}
				break;
				
				case 1:
				{
					//切换程控模式
					mission_step += 1;				
				}
				break;
				
				case 2:
				{
					//解锁
					mission_step += FC_Unlock();
					
				}
				break;
				
				case 3:
				{
					//等5秒
					if(time_dly_cnt_ms<5000)
					{
						time_dly_cnt_ms+=20;//ms
					}
					else
					{
						time_dly_cnt_ms = 0;
						mission_step += 1;
					}
				}
				break;
				
				case 4:
				{
					//起飞
					mission_step += OneKey_Takeoff(140);//参数单位：厘米； 0：默认上位机设置的高度。
				}
				break;
				
				case 5:
				{
					//等5秒
					if(time_dly_cnt_ms<7000)
					{
						time_dly_cnt_ms+=20;//ms
					}
					else
					{
						time_dly_cnt_ms = 0;
						mission_step += 1;
						
					}					
				}
				break;
				
				case 6:                                               //角度校准
				{                                     
					if(time_dly_cnt_ms<5500)
					{
						time_dly_cnt_ms += 20;               
						if(time_dly_cnt_ms - 500 == 0)  
						{
							angle_bis = fc_att.st_data.yaw - start_yaw;
							Rotate((-angle_bis),1);
						}
					}
					else 
					{
						mission_step++;
						time_dly_cnt_ms = 0;
					}
				}
				break;
				
				case 7: //前进到 3附近
				{  				                                          
					if(time_dly_cnt_ms < (((position_dis[Num_1]/FLYSPEED)+3)*1000))
					{
						time_dly_cnt_ms += 20;
						if(time_dly_cnt_ms - 500==0)    Horizontal_Move(position_dis[Num_1],FLYSPEED,position_ang[Num_1]);  
					}
					else 
					{
						mission_step ++;
						time_dly_cnt_ms = 0;
					}
				}
				break;
				
				case 8:  //弥补误差
				{
					offset_error_xy(position_Y[Num_1],-position_X[Num_1],cal_X,cal_Y);					
					if(time_dly_cnt_ms < 3000)
					{
						time_dly_cnt_ms += 20;        //3 位置弥补
						if(time_dly_cnt_ms - 20==0)    Horizontal_Move(offset_Distance,4,offset_Angle);
						
						frame_send_16(offset_Distance,0XF8);	
						frame_send_16(offset_Angle,0XF9);
					}
					else 
					{
						mission_step ++;
						time_dly_cnt_ms = 0;
					}					
				}
				break;
				
				case 9:  //OPMV 误差弥补
				{
					time_dly_cnt_ms += 20;
					
					if(openmv.state==0X03)
					{
						float PIXpcm,PIYpcm;
						s16 Distance_to_cross;
						if( Ano_Alt<155 && Ano_Alt>145 )  
							{
								PIXpcm = 2.0f;PIYpcm=1.66f;
							}						
						else if(Ano_Alt<155&&Ano_Alt>170)
							{
								PIXpcm = 2.4f;PIYpcm=1.82f;
							}						
						else if(Ano_Alt>130&&Ano_Alt<145)
							{
								PIXpcm = 1.62f;PIYpcm=1.50f;
							}
						
						else if(Ano_Alt>90&&Ano_Alt<130)
							{
								PIXpcm = 1.2f;PIYpcm=1.0f;
							}
						else 
							{
								PIXpcm = 0.5f;PIYpcm = 0.4f;
							}
						
						offset_error_xy(0,0,-(Dcy*PIYpcm),-(Dcx*PIXpcm));		
							
						   frame_send_float((Dcy*PIYpcm),0xF5);	
							frame_send_float((Dcx*PIXpcm),0xF6);	
							
						if((Dcx<-6||Dcx>6)||(Dcy<-6||Dcy>6))       
							{
								if(time_dly_cnt_ms % 1000 == 0)
								{
									Horizontal_Move(offset_Distance,5,openmv.angle_out);  
									
									frame_send_16(offset_Distance,0XF3);									
								}
							}
						else if( (Dcx>-6&&Dcx<6) && (Dcy>-6&&Dcy<6) && openmv.state == 0XFF )   
						{
							mission_step = 40;
							time_dly_cnt_ms = 0;
						}
						if(time_dly_cnt_ms > 10000)
					{
						mission_step = 40;
						time_dly_cnt_ms = 0;
					}
					}								
				}
				break;
				
				case 40:
				{
					if(time_dly_cnt_ms < 500)
					{
						time_dly_cnt_ms += 20;
						if(time_dly_cnt_ms - 20==0)        FC_stand();
					}
					else 
					{
						mission_step ++;
						time_dly_cnt_ms = 0;
					}

				}
				break;
				
				case 41:
				{
					mission_step ++;
				}
				break;
				
				
				case 42:
				{
					if(time_dly_cnt_ms < 10000)
					{
						time_dly_cnt_ms += 20;
						if(time_dly_cnt_ms - 20==0)        DOWN_(50,20);
						else if(time_dly_cnt_ms - 6000==0)   UP_(50,20);
					}
					else 
					{
						mission_step ++;
						time_dly_cnt_ms = 0;
					}

				}
				break;
				
				case 43:  //弥补误差
				{
					offset_error_xy(0,0,cal_X_2,cal_Y_2);					
					if(time_dly_cnt_ms < 4000)
					{
						time_dly_cnt_ms += 20;        //3 位置弥补
						if(time_dly_cnt_ms - 20==0)    Horizontal_Move(offset_Distance,3,offset_Angle);
						
						frame_send_16(offset_Distance,0XF8);	
						frame_send_16(offset_Angle,0XF9);
					}
					else 
					{
						mission_step =10;
						time_dly_cnt_ms = 0;
					}					
				}
				break;
				
				case 10:                                               //角度校准
				{                                     
					if(time_dly_cnt_ms<2000)
					{
						time_dly_cnt_ms += 20;               
						if(time_dly_cnt_ms - 500 == 0)  
						{
							angle_bis = fc_att.st_data.yaw - start_yaw;
							Rotate((-angle_bis),1);
						}
					}
					else 
					{
						mission_step=45;
						time_dly_cnt_ms = 0;
					}
				}
				break;	
						
				case 45:
				{
					mission_step =11;
				}
				break;
				
				
																																															
				case 11: // 3 前进到 P
				{  				                                          
					if(time_dly_cnt_ms < (((position_dis_GoHome[Num_1]/FLYSPEED)+3)*1000))
					{
						time_dly_cnt_ms += 20;
						if(time_dly_cnt_ms - 500==0)    Horizontal_Move(position_dis_GoHome[Num_1],FLYSPEED,position_ang_GoHome[Num_1]);  
					}
					else 
					{
						mission_step  = 44;
						time_dly_cnt_ms = 0;
					}
				}
				break;
				
				case 44:  //弥补误差
				{
					offset_error_xy(position_Y[Num_1],-position_X[Num_1],cal_X,cal_Y);					
					if(time_dly_cnt_ms < 2000)
					{
						time_dly_cnt_ms += 20;        //3 位置弥补
						if(time_dly_cnt_ms - 20==0)    Horizontal_Move(offset_Distance,3,offset_Angle);
						
						frame_send_16(offset_Distance,0XF8);	
						frame_send_16(offset_Angle,0XF9);
					}
					else 
					{
						mission_step =12;
						time_dly_cnt_ms = 0;
					}					
				}
				break;
				
				case 12:     //OPMV矫正
				{
					time_dly_cnt_ms += 20;
					
					if(openmv.state==0XFF)
					{
						float PIXpcm,PIYpcm;
						s16 Distance_to_cross;
						if( Ano_Alt<155 && Ano_Alt>145 )  
							{
								PIXpcm = 2.0f;PIYpcm=1.66f;
							}						
						else if(Ano_Alt<155&&Ano_Alt>170)
							{
								PIXpcm = 2.4f;PIYpcm=1.82f;
							}						
						else if(Ano_Alt>130&&Ano_Alt<145)
							{
								PIXpcm = 1.62f;PIYpcm=1.50f;
							}
						
						else if(Ano_Alt>90&&Ano_Alt<130)
							{
								PIXpcm = 1.2f;PIYpcm=1.0f;
							}
						else 
							{
								PIXpcm = 0.5f;PIYpcm = 0.4f;
							}
						
						offset_error_xy(0,0,-(Dcy*PIYpcm),-(Dcx*PIXpcm));
//						Distance_to_cross = (u16)my_sqrt( (Dcy*PIYpcm)*(Dcy*PIYpcm) + (Dcx*PIXpcm)*(Dcx*PIXpcm) );
						
						   frame_send_float((Dcy*PIYpcm),0xF5);	
							frame_send_float((Dcx*PIXpcm),0xF6);	
							
						if(( Dcx < -6 || Dcx > 6 ) || ( Dcy < -6 || Dcy > 6 ) )
							{
								if(time_dly_cnt_ms % 1000 == 0)
								{
									Horizontal_Move(offset_Distance,10,openmv.angle_out);  
									frame_send_16(offset_Distance,0XF3);
									frame_send_float(offset_Angle,0XF1);
								}
							}
						else if( (Dcx > -6 && Dcx < 6 ) && ( Dcy > -6 && Dcy < 6 ) && openmv.state == 0XFF )
						{
							mission_step = 18;
							time_dly_cnt_ms = 0;
						}
						if(time_dly_cnt_ms > 10000)
					{
						mission_step = 18;//Horizontal_Move(100, 100, 0);
						time_dly_cnt_ms = 0;
					}
					}				
				}
				break;
				
				
				
				
				
			
//				case 9:  //弥补误差
//				{
//					offset_error_xy(position_Y[Num_1],-position_X[Num_1],cal_X,cal_Y);					
//					if(time_dly_cnt_ms < 3000)
//					{
//						time_dly_cnt_ms += 20;        //3 位置弥补
//						if(time_dly_cnt_ms - 20==0)    Horizontal_Move(offset_Distance,2,offset_Angle);    
//					}
//					else 
//					{
//						mission_step = 30;
//						time_dly_cnt_ms = 0;
//					}					
//				}
//				break;
				
//				case 30:
//				{
//					mission_step = 9;
//				}
//				break;
//								
//				case 9:
//				{
//					if(time_dly_cnt_ms < 10000)
//					{
//						time_dly_cnt_ms += 20;
//						if(time_dly_cnt_ms - 20==0)        DOWN_(50,20);
//						else if(time_dly_cnt_ms - 6000==0)   UP_(50,20);
//					}
//					else 
//					{
//						mission_step = 31;
//						time_dly_cnt_ms = 0;
//					}

//				}
//				break;
				
//				case 31:  //弥补误差
//				{
//					offset_error_xy(0,0,cal_X_2,cal_Y_2);					
//					if(time_dly_cnt_ms < 3000)
//					{
//						time_dly_cnt_ms += 20;        //3 位置弥补
//						if(time_dly_cnt_ms - 20==0)    Horizontal_Move(offset_Distance,3,offset_Angle);    
//					}
//					else 
//					{
//						mission_step = 10;
//						time_dly_cnt_ms = 0;
//					}					
//				}
//				break;
//				
//				case 10:                                               //角度校准
//				{                                     
//					if(time_dly_cnt_ms<5500)
//					{
//						time_dly_cnt_ms += 20;               
//						if(time_dly_cnt_ms - 500 == 0)  
//						{
//							angle_bis = fc_att.st_data.yaw - start_yaw;
//							Rotate((-angle_bis),1);
//						}
//					}
//					else 
//					{
//						mission_step++;
//						time_dly_cnt_ms = 0;
//					}
//				}
//				break;
				
//				case 11:  //前进到 10       
//				{   					
//					Quadrant_anl(Num_1,Num_2);                    
//					if(time_dly_cnt_ms < ((Second_Distance/FLYSPEED)+2)*1000)                                                      
//					{
//						time_dly_cnt_ms += 20;                                                  
//						if(time_dly_cnt_ms - 500==0)    Horizontal_Move(Second_Distance,FLYSPEED,Second_angle);						//230cm 162
//						
//						frame_send_8(rate,0xF4);
//						frame_send_16(Second_Distance,0xF5);
//						frame_send_16(Second_angle,0xF6);
//					}
//					else 
//					{
//						mission_step = 40;
//						time_dly_cnt_ms = 0;
//					}
//				}
//				break;
//				
//				case 40:
//				{
//					mission_step = 12;
//				}
//				break;
//				
//				case 12:
//				{
//					if(time_dly_cnt_ms < 10000)
//					{
//						time_dly_cnt_ms += 20;
//						if(time_dly_cnt_ms - 20==0)        DOWN_(50,20);
//						else if(time_dly_cnt_ms - 6000==0)   UP_(50,20);
//					}
//					else 
//					{
//						mission_step = 41;
//						time_dly_cnt_ms = 0;
//					}

//				}
//				break;
//				
//				case 41:  //弥补误差
//				{
//					offset_error_xy(0,0,cal_X_3,cal_Y_3);					
//					if(time_dly_cnt_ms < 3000)
//					{
//						time_dly_cnt_ms += 20;        //3 位置弥补
//						if(time_dly_cnt_ms - 20==0)    Horizontal_Move(offset_Distance,3,offset_Angle);    
//					}
//					else 
//					{
//						mission_step = 13;
//						time_dly_cnt_ms = 0;
//					}					
//				}
//				break;
//				
//				case 13:                                               //角度校准
//				{                                     
//					if(time_dly_cnt_ms<5500)
//					{
//						time_dly_cnt_ms += 20;               
//						if(time_dly_cnt_ms - 500 == 0)  
//						{
//							angle_bis = fc_att.st_data.yaw - start_yaw;
//							Rotate((-angle_bis),1);
//						}
//					}
//					else 
//					{
//						mission_step++;
//						time_dly_cnt_ms = 0;
//					}
//				}
//				break;
//				
//				case 14: 
//				{                          //前进到 P                                  
//					if(time_dly_cnt_ms < ((position_dis_GoHome[Num_2]/FLYSPEED)+2)*1000) 
//					{
//						time_dly_cnt_ms += 20;
//						if(time_dly_cnt_ms - 500==0)    Horizontal_Move(position_dis_GoHome[Num_2],FLYSPEED,position_ang_GoHome[Num_2]);  
//					}
//					else 
//					{
//						mission_step = 17;
//						time_dly_cnt_ms = 0;
//					}
//				}
//				break;
				
				
					case 18:
				{
					//等2秒
					if(time_dly_cnt_ms<2000)
					{
						time_dly_cnt_ms+=20;//ms
					}
					else
					{
						time_dly_cnt_ms = 0;
						mission_step ++;
					}
				}
				break;
				
				case 19:
				{
					//执行一键降落
					OneKey_Land();	
				}
				break;				
				default:break;
			}
		}
		else
		{
			mission_step = 0;
		}
	}
}


void frame_send_8(u8 a,u8 num)
{
u8 DateSendBuffer[100];
u8 cnt=0;
u8 sumcheck = 0;
u8 addcheck = 0;
		DateSendBuffer[cnt++]=0xAA;
		DateSendBuffer[cnt++]=0xFF;
		DateSendBuffer[cnt++]=num;
		DateSendBuffer[cnt++]=1;
		DateSendBuffer[cnt++]=a;
for(u8 i=0; i < (DateSendBuffer[3]+4); i++)
{
sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
}
		DateSendBuffer[cnt++]=sumcheck;
		DateSendBuffer[cnt++]=addcheck;
	
	
DrvUart5SendBuf(DateSendBuffer,cnt);
}

void frame_send_16(s16 a,u8 num)
{
u8 DateSendBuffer[100];
u8 cnt=0;
u8 sumcheck = 0;
u8 addcheck = 0;
		DateSendBuffer[cnt++]=0xAA;
		DateSendBuffer[cnt++]=0xFF;
		DateSendBuffer[cnt++]=num;
		DateSendBuffer[cnt++]=2;
		DateSendBuffer[cnt++]=BYTE0(a);
	   DateSendBuffer[cnt++]=BYTE1(a);
	

for(u8 i=0; i < (DateSendBuffer[3]+4); i++)
{
sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
}
		DateSendBuffer[cnt++]=sumcheck;
		DateSendBuffer[cnt++]=addcheck;
	
	
DrvUart5SendBuf(DateSendBuffer,cnt);
}

void frame_send_float(float a,u8 num)
{
	u8 DateSendBuffer[100];
	u8 cnt=0;
	u8 sumcheck = 0;
	u8 addcheck = 0;
			DateSendBuffer[cnt++]=0xAA;
			DateSendBuffer[cnt++]=0xFF;
			DateSendBuffer[cnt++]=num;
			DateSendBuffer[cnt++]=4;
			DateSendBuffer[cnt++]=BYTE0(a);
			DateSendBuffer[cnt++]=BYTE1(a);
			DateSendBuffer[cnt++]=BYTE2(a);
			DateSendBuffer[cnt++]=BYTE3(a);
		

	for(u8 i=0; i < (DateSendBuffer[3]+4); i++)
	{
	sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
	addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
	}
			DateSendBuffer[cnt++]=sumcheck;
			DateSendBuffer[cnt++]=addcheck;
		
		
	DrvUart5SendBuf(DateSendBuffer,cnt);
}

void frame_send_32(s32 a,u8 num)
{
	u8 DateSendBuffer[100];
	u8 cnt=0;
	u8 sumcheck = 0;
	u8 addcheck = 0;
	
	DateSendBuffer[cnt++]=0xAA;
	DateSendBuffer[cnt++]=0xFF;
	DateSendBuffer[cnt++]=num;
	DateSendBuffer[cnt++]=4;
	DateSendBuffer[cnt++]=BYTE0(a);
	DateSendBuffer[cnt++]=BYTE1(a);
	DateSendBuffer[cnt++]=BYTE2(a);
	DateSendBuffer[cnt++]=BYTE3(a);
	
	for(u8 i=0; i < (DateSendBuffer[3]+4); i++)
	{
		sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
	}
	
	DateSendBuffer[cnt++]=sumcheck;
	DateSendBuffer[cnt++]=addcheck;

   DrvUart5SendBuf(DateSendBuffer,cnt);
}

//OPMV数据 发送上位机
void Userdata_send_OPENMV()
{
	static u8 User_send_buffer[20];
	u8 _cnt = 0;
	User_send_buffer[_cnt++] = 0XAA;
	User_send_buffer[_cnt++] = 0XFF;
	if(openmv.mode_sta==1)
	{
		User_send_buffer[_cnt++] = 0Xf2;
		User_send_buffer[_cnt++] = 0;
		User_send_buffer[_cnt++] = openmv.mode_sta;
		User_send_buffer[_cnt++] = openmv.cb.color_flag;
		User_send_buffer[_cnt++] = openmv.cb.sta;
		User_send_buffer[_cnt++] = BYTE0(openmv.cb.pos_x);
		User_send_buffer[_cnt++] = BYTE1(openmv.cb.pos_x);
		User_send_buffer[_cnt++] = BYTE0(openmv.cb.pos_y);
		User_send_buffer[_cnt++] = BYTE1(openmv.cb.pos_y);
		User_send_buffer[_cnt++] = openmv.cb.dT_ms;
		User_send_buffer[3] =  _cnt - 4;
	}
	else if(openmv.mode_sta==2)
	{
		User_send_buffer[_cnt++] = 0Xf3;
		User_send_buffer[_cnt++] = 0;
		User_send_buffer[_cnt++] = openmv.mode_sta;
		User_send_buffer[_cnt++] = openmv.lt.sta;
		User_send_buffer[_cnt++] = BYTE0(openmv.lt.angle);
		User_send_buffer[_cnt++] = BYTE1(openmv.lt.angle);
		User_send_buffer[_cnt++] = BYTE0(openmv.lt.deviation);
		User_send_buffer[_cnt++] = BYTE1(openmv.lt.deviation);
		User_send_buffer[_cnt++] = openmv.lt.p_flag;
		User_send_buffer[_cnt++] = BYTE0(openmv.lt.pos_x);
		User_send_buffer[_cnt++] = BYTE1(openmv.lt.pos_x);
		User_send_buffer[_cnt++] = BYTE0(openmv.lt.pos_y);
		User_send_buffer[_cnt++] = BYTE1(openmv.lt.pos_y);
		User_send_buffer[_cnt++] = openmv.lt.dT_ms;
		User_send_buffer[3] =  _cnt - 4;
	}
	else if(openmv.mode_sta==3)
	{
		User_send_buffer[_cnt++] = 0XF4;
		User_send_buffer[_cnt++] = 0;
		User_send_buffer[_cnt++] = openmv.state;
		User_send_buffer[_cnt++] = openmv.posX;
		User_send_buffer[_cnt++] = openmv.posY;
		User_send_buffer[_cnt++] = openmv.angle;
		
			Dcx = openmv.posX - 70;                         //终点 (80，43)
			Dcy = openmv.posY - 30;
			if(Dcx>=0&&Dcy<=0)openmv.angle_out = openmv.angle;
				else if(Dcx>0&&Dcy>0)openmv.angle_out = 180 - openmv.angle;
				else if(Dcx<0&&Dcy>0)openmv.angle_out = openmv.angle + 180;
				else if(Dcx<0&&Dcy<0)openmv.angle_out = 360 - openmv.angle;
		
		User_send_buffer[_cnt++] = BYTE0(openmv.angle_out);
		User_send_buffer[_cnt++] = BYTE1(openmv.angle_out);
		
//		User_send_buffer[_cnt++] = openmv.posX_A;
//		User_send_buffer[_cnt++] = openmv.posY_A;
		
//			Dcx_A = openmv.posX_A - 80;
//			Dcy_A = openmv.posY_A - 60;
//			if(Dcx_A>=0&&Dcy_A<=0)openmv.angle_A_out = openmv.angle_A;
//				else if(Dcx_A>0&&Dcy_A>0)openmv.angle_A_out = 180 - openmv.angle_A;
//				else if(Dcx_A<0&&Dcy_A>0)openmv.angle_A_out = openmv.angle_A + 180;
//				else if(Dcx_A<0&&Dcy_A<0)openmv.angle_A_out = 360 - openmv.angle_A;
//				else openmv.angle_A_out = 0;
				

		
//		User_send_buffer[_cnt++] = openmv.angle_A;
//		User_send_buffer[_cnt++] = BYTE0(openmv.angle_A_out);
//		User_send_buffer[_cnt++] = BYTE1(openmv.angle_A_out);

		User_send_buffer[3] =  _cnt - 4;
	}
//校验
	u8 check_sum1 = 0, check_sum2 = 0;
	for (u8 i = 0; i < _cnt; i++)
	{
		check_sum1 += User_send_buffer[i];
		check_sum2 += check_sum1;
	}
	User_send_buffer[_cnt++] =  check_sum1;
	User_send_buffer[_cnt++] =  check_sum2;
	
	//
	DrvUart5SendBuf(User_send_buffer,_cnt);		
}

/* 
	位置误差弥补函数
   输出：弥补距离--offset_Distance
			弥补角度--offset_Angle

*/
void offset_error_xy(s16 target_x,s16 target_y,s16 real_x,s16 real_y)
{	
	s16 error_x,error_y;
	error_x = (real_x - target_x);
	error_y = (real_y - target_y);
	offset_Distance = (u16) (my_sqrt((error_x*error_x)+(error_y*error_y)));
	if(error_x>0&&error_y>0)//1象限
	{
	offset_Angle = 360-fast_atan2(abs(error_y),abs(error_x));
	}
	else	if(error_x<0&&error_y>0)//2象限
	{
	offset_Angle = 360-(180-fast_atan2(abs(error_y),abs(error_x)));
	}
	else	if(error_x<0&&error_y<0)//3象限
	{
	offset_Angle =360-(180+fast_atan2(abs(error_y),abs(error_x)));
	}
	else	if(error_x>0&&error_y<0)//4象限
	{
	offset_Angle = fast_atan2(abs(error_y),abs(error_x));
	}
}

/*
	Second_angle角度计算
	return：Second_angle
*/

s16 Angle_cal(s16 bis_X,s16 bis_Y,u8 xiangxian)
{

	s16 angle;
	rate = ABS(bis_X)*100/ABS(bis_Y);
	if (rate == 300)
	{
		switch (xiangxian)
		{
			case 1:
				angle = 72;
				break;
			
			case 2:
				angle = 360 - 72;
				break;
			
			case 3:
				angle = 180 + 72;
				break;
			
			case 4:
				angle = 180 - 72;
				break;
			default:
				break;
		}
	}
	else if (rate == 200)
	{
		switch (xiangxian)
		{
			case 1:
				angle = 63;
				break;
			
			case 2:
				angle = 360 - 63;
				break;
			
			case 3:
				angle = 180 + 63;
				break;
			
			case 4:
				angle = 180 - 63;
				break;
			default:
				break;
		}
	}
	else if (rate == 100)
	{
		switch (xiangxian)
		{
			case 1:
				angle = 45;
				break;
			
			case 2:
				angle = 360 - 45;
				break;
			
			case 3:
				angle = 180 + 45;
				break;
			
			case 4:
				angle = 180 - 45;
				break;
			default:
				break;
		}
	}
	else if (rate == 50)
	{
		switch (xiangxian)
		{
			case 1:
				angle = 27;
				break;
			
			case 2:
				angle = 360 - 27;
				break;
			
			case 3:
				angle = 180 + 27;
				break;
			
			case 4:
				angle = 180 - 27;
				break;
			default:
				break;
		}
	}
	else if (rate < 40 && rate > 0)
	{
		switch (xiangxian)
		{
			case 1:
				angle = 18;
				break;
			
			case 2:
				angle = 360 - 18;
				break;
			
			case 3:
				angle = 180 + 18-2;
				break;
			
			case 4:
				angle = 180 - 18;
				break;
			default:
				break;
		}
	}
	
	return angle;	
}

/*
	象限分析
	可以得出 Second_angle，Second_Distance
*/
//花椒算法 撰写人：
void Quadrant_anl(u8 num_1,u8 num_2)   
{
	s16 bis_X,bis_Y;
	u8 xiangxian;
	s16 distance;

	
	bis_X = position_X[num_2] - position_X[num_1];
	bis_Y = position_Y[num_2] - position_Y[num_1];
	
	
	if(bis_X>0 && bis_Y>0)
	{
		xiangxian=1;
		Second_angle = Angle_cal(bis_X,bis_Y,xiangxian);
	}
	else if(bis_X<0 && bis_Y>0)
	{
		xiangxian=2;
		Second_angle = Angle_cal(bis_X,bis_Y,xiangxian);
	}
	else if(bis_X<0 && bis_Y<0)
	{
		xiangxian=3;
		Second_angle = Angle_cal(bis_X,bis_Y,xiangxian);
	}
	else if(bis_X>0 && bis_Y<0)
	{
		xiangxian=4;
		Second_angle = Angle_cal(bis_X,bis_Y,xiangxian);
	}
	else if(bis_X>0 && bis_Y==0)
	{
		xiangxian=5;
		Second_angle = 90;
	}
	else if(bis_X==0 && bis_Y>0)
	{
		xiangxian=6;
		Second_angle = 0;
	}
	else if(bis_X<0 && bis_Y==0)
	{
		xiangxian=7;
		Second_angle = 270;
	}
	else if(bis_X==0 && bis_Y<0)
	{
		xiangxian=8;
		Second_angle = 180;
	}
	
	distance = my_sqrt((bis_X*bis_X)+(bis_Y*bis_Y));
	
	if(distance < 116 && distance > 96)
	{
		Second_Distance=106;
	}
	else if(distance<160&&distance>140)
	{
		Second_Distance=150;
	}
		else if(distance<222&&distance>202)
	{
		Second_Distance=212;
	}
		else if(distance<247&&distance>210 )
	{
		Second_Distance=210;
	}
		else if(distance<310&&distance>290)
	{
		Second_Distance=300;
	}
		else if(distance<328&&distance>308)
	{
		Second_Distance=318;
	}
		else if(distance<345&&distance>325)
	{
		Second_Distance=335;
	}
		else if(distance<434&&distance>414)
	{
		Second_Distance=424;
	}
	
}

