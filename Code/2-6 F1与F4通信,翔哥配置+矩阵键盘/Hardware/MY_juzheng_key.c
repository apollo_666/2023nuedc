#include "stm32f10x.h"                  // Device header
#include "Delay.h"
//A4-A7接R1-R4  A0-A3接C1-C4

void Key_Init1(void){
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	//PD0,1,2,3作为输出，PD4,5,6,7作为输入，且上拉，默认为1
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	//0,1,2,3置低电位，即0
	GPIO_ResetBits(GPIOA,GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3);
	
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
}

void Key_Init2(void){
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	//PD0,1,2,3作为输入，且上拉，默认为1；PD4,5,6,7作为输出，且置0
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	GPIO_ResetBits(GPIOA,GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7);
}

unsigned int Key_Scan(void){
	uint16_t row,col,res;//row为行值，col为列值，res=row|col,用于主函数判断
	Key_Init1();//此时PD7...PD0为0x11110000，即0xf0
	col=GPIO_ReadInputData(GPIOA);	//读取上面的0xf0
	col=col&0xf0;//没有任何按键按下那么col必然与0xf0相等
	if(col!=0xf0)//说明可能有按键按下，需要消除抖动，进一步确定
	{
		Delay_ms(20);//消除抖动
		col=GPIO_ReadInputData(GPIOA);
		col=col&0xf0;
		if(col!=0xf0)//若还是不相等，表示必然有按键按下
		{
			col=GPIO_ReadInputData(GPIOA);
			col=col&0xf0;//如果S1按下,那此处就是0xe0&0xf0=0xe0
		}
		Key_Init2();//此时PD7...PD0为0x00001111，即0x0f
		row=GPIO_ReadInputData(GPIOA);//读取上面的0x0f
		row=row&0x0f;//没有任何按键按下那么row必然与0x0f相等
		if(row!=0x0f)//说明可能有按键按下，需要消除抖动，进一步确定
		{
			Delay_ms(20);//消除抖动
			row=GPIO_ReadInputData(GPIOA);
			row=row&0x0f;
			if(row!=0x0f)//若还是不相等，表示必然有按键按下
			{
				row=GPIO_ReadInputData(GPIOA);
				row=row&0x0f;//若S1按下，那此处就是0x0e&0x0f=0x0e
			}
			res=row|col;//0xe0|0x0e=0xee，表示按键S1对应的键值为0xee
		}
	}		
		return res;
}

