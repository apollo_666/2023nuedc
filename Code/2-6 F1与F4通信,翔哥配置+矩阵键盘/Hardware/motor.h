#ifndef __MOTOR_H
#define __MOTOR_H

void motor_Init(void);
void A_motor_stop(void);
void B_motor_stop(void);
void C_motor_stop(void);
void A_motor_wait(void);
void B_motor_wait(void);
void C_motor_wait(void);
void A_motor_1p2d(void);
void A_motor_1d2p(void);
void B_motor_1p2d(void);
void B_motor_1d2p(void);
void C_motor_1p2d(void);
void C_motor_1d2p(void);

#endif
