# Untitled - By: Thinkpad - 周四 11月 4 2021

import sensor,time,pyb,math,sys,image
from pyb import Pin, Timer, LED, UART
from image import SEARCH_EX, SEARCH_DS
sensor.reset()
sensor.set_pixformat(sensor.RGB565) # 灰度更快RGB565
sensor.set_framesize(sensor.QQVGA)#-2Q
#sensor.set_hmirror(True)
#sensor.set_vflip(True)
sensor.skip_frames(time = 2000)

sensor.set_contrast(1)
sensor.set_gainceiling(16)
# Max resolution for template matching with SEARCH_EX is QQVGA

clock = time.clock()
thresholds = [(0, 23, -34, 32, -8, 14),     #黑色
              (0, 30, 0, 64, -128, 0),      #蓝色
              (30, 100, 15, 127, 0, 127)]  #红色
#black =(0, 23, -34, 32, -8, 14)   #(44, 255)
#blue = (9, 61, -48, 48, -53, -10)
ROI=(45,20,70,100)


#发送数据
uart_buf1 = bytearray([0xAA,0xFF, 0XCC, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])

#串口三配置
uart = UART(1,500000)
uart.init(500000, bits=8, parity=None, stop=1)

def uartsend(timer):
    uart.write(uart_buf1)
    print(uart_buf1)

tim = Timer(2, freq=20)
tim.callback(uartsend)

atan_A = 0
while(True):
    clock.tick()
    img = sensor.snapshot()
    STA = 0x00
    a = 0
    atan = 0
    cx1 = 0     #圆x
    cy1 = 0     #圆y
    cx2=0       #三角形x
    cy2=0       #三角形y
    fx = 80
    fy = 60
    cx3 = 0
    cy3 = 0
    for c in img.find_circles(threshold = 4500, x_margin = 10, y_margin = 10, r_margin = 10,r_min = 2, r_max = 100, r_step = 2):
        img.draw_circle(c.x(),c.y(),c.r(), color = (255, 0, 0))
        img.draw_cross(c.x(),c.y(), color = (255, 0, 0))
        cx = c.x()-c.r()
        cx1 = c.x()
        cy = c.y()-c.r()
        cy1 = c.y()
        cr = 2*c.r()
        print("x:",c.x(),"y:",c.y())
        roi1 = (cx,cy,cr,cr)
        for blobs in img.find_blobs(thresholds,roi=roi1, x_stride=5, y_stride=5,area_threshold=20, merge=True):
        #if blobs:
            a = blobs.code()
            if a == 1:
                STA = 0xFF          #起点（黑色圆）
            elif a == 2:
                STA = 0x01          #蓝色圆
            elif a == 4:
                STA = 0x02          #红色圆
            #else:
               # STA=0x00
            #if blob.density()>0.40 and blob.density()<0.60:
                #img.draw_cross(blob.cx(), blob.cy())
                #if blob.code() == 1:
                 #   STA = 0x00          #无
                #elif blob.code() == 2:
                #    STA = 0x03          #蓝色三角形
                #elif blob.code() == 4:
                #    STA = 0x04          #红色三角形
                #else:
        #else:
            #STA = 0x00
            #cx1 = 80
            #cy1 = 60

        if STA == 0xff or STA == 0x01 or STA == 0x02:
            atan = int((180/3.1416)*(math.atan2(abs(fx-cx1),abs(fy-cy1))))
            uart_buf1 = bytearray([0xAA,0xFF, 0XCC, 0x00, STA, cx1, cy1, atan, 0x00,0x00])

    for blob1 in img.find_blobs(thresholds,pixels_threshold=200,area_threshold=200):
        if blob1.density()>0.30 and blob1.density()<0.60:
          img.draw_cross(blob1.cx(), blob1.cy())
          if blob1.code() == 1:
             STA = 0xff          #起点（黑色圆）
          elif blob1.code() == 2:
             STA = 0x03          #蓝色三角形
          elif blob1.code() == 4:
             STA = 0x04          #红色三角形
          cx2 = blob1.x()
          cy2 = blob1.y()
          if STA == 0x03 or STA == 0x04:
             atan = int((180/3.1416)*(math.atan2(abs(fx-cx1),abs(fy-cy1))))
             uart_buf1 = bytearray([0xAA,0xFF, 0XCC, 0x00, STA, cx2, cy2, atan, 0x00,0x00])

        #if blob1.density()>0.75:
         #   img.draw_cross(blob1.cx(),blob1.cy())
          #  if blob1.code() == 1:
           #     STA = 0xff
            #elif blob1.code() == 2:
             #   STA = 0x05
           # elif blob1.code() == 4:
            #    STA = 0x06
            #cx2 = blob1.x()
            #cy2 = blob1.y()
            #if STA == 0x05 or STA == 0x06:
             #   atan = int((180/3.1416)*(math.atan2(abs(fx-cx1),abs(fy-cy1))))
              #  uart_buf1 = bytearray([0xAA,0xFF, 0XCC, 0x00, STA, cx2, cy2, atan, 0x00,0x00])
               # print("x:",cx2,"y:",cy2)
    for r in img.find_rects(threshold=25000):

        roi2 = (r.x(), r.y(), r.w(), r.h())
        cx3 = r.x()
        cy3 = r.y()
        for blob2 in img.find_blobs(thresholds, x_stride=5, y_stride=5,area_threshold=20, merge=True):
            img.draw_rectangle(r.rect(), color=(0, 0, 255))
            img.draw_cross(r.x(),r.y(),color = (0,0,255))
            if blob2.code() == 1:
                STA = 0xff
            elif blob2.code() == 2:
                STA = 0x05          #蓝色正方形
            elif blob2.code() == 4:
                STA = 0x06          #红色正方形
        if STA == 0x05 or STA == 0x06:
          atan = int((180/3.1416)*(math.atan2(abs(fx-cx1),abs(fy-cy1))))
          uart_buf1 = bytearray([0xAA,0xFF, 0XCC, 0x00, STA, cx3, cy3, atan, 0x00,0x00])
          print("x:",cx3,"y:",cy3)
    print("STA:",STA)
    lens = len(uart_buf1)#数据包大小
    uart_buf1[3] = lens-6;#有效数据个数
    i = 0
    sum = 0
    sum1 = 0
    #和校验
    while i<(lens-2):
        sum = sum + uart_buf1[i]
        sum1 = sum1 + sum
        i = i+1
    uart_buf1[lens-2] = sum;
    uart_buf1[lens-1] = sum1;


