#ifndef __USER_TASK_H
#define __USER_TASK_H

#include "SysConfig.h"

#define FLYSPEED 30
#define CONFIRM_TIMES     10     //重确认特征判断的次数，确认次数越多，越能避免误检测，但是也可能导致检测不到。
#define YAW_PAL_DPS       15    //航向角速度，度每秒

extern float offset_Angle;
extern u8 cmd_send_state;
extern s16 PLANE_YAW;
extern s16 CMDvalue[4];
extern s32 POS_X,POS_Y;

void UserTask_OneKeyCmd(void);
void frame_send_8(u8 a,u8 num);
void frame_send_16(s16 a,u8 num);
void frame_send_float(float a,u8 num);
void frame_send_32(s32 a,u8 num);
void offset_error_xy(s16 target_x,s16 target_y,s16 real_x,s16 real_y);
void Quadrant_anl(u8 num_1,u8 num_2);
s16 target_yaw_cal(s16 yaw_start,s16 angle);

void Userdata_send_OPENMV();
void Userdata_send_OPENMV_UP();

s16 Angle_cal(s16 bis_X,s16 bis_Y,u8 xiangxian);
void My_Followlane(void);

extern u8 mission_step;
extern float start_yaw;

typedef struct
{
			s16 Vxpcm;
			s16 Vypcm;
			s16 Vzpcm;
			s16 Vdps;
			s16 ERR_PIX[4];
			s16 ERR_D_PIX[4];
}Speed;

#endif
