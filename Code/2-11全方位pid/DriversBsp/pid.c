/**
  ******************************************************************************
  * Copyright (c) 2018,北京中科浩电科技有限公司
  * All rights reserved.
  * 文件名称：pid.c
  * 摘    要：
  *
  * 当前版本：V1.0
  * 作    者：北京中科浩电科技有限公司研发部 
  * 完成日期：    
  * 修改说明：
  * 
  *
  * 历史版本：
  *
  *
  *******************************************************************************/

/*==============================================================================
                         ##### How to use this driver #####
==============================================================================
PID驱动使用方法如下：
1.构建一个PIDInfo_t结构体，将所需要控制的数据存放进去；
2.调UpdatePID函数，计算PID输出结果
3.可以直接调用ClacCascadePID直接计算串级PID


*/
//外部文件引用
#include "pid.h"
#include "Ano_Math.h"

//#include "myMath.h"    


//宏定义区



//Extern引用



//私有函数区



//私有变量区
/*PID工程变量*/
PIDInfo_t SpeedPID_X;
PIDInfo_t SpeedPID_Z;
PIDInfo_t	SpeedPID_X_Down;
PIDInfo_t	SpeedPID_Y_Down;
PIDInfo_t SpeedPID_ALL;

/******************************************************************************
  * 函数名称：ResetPID
  * 函数描述：复位PID
  * 输    入：PID结构体指针
  * 输    出：void
  * 返    回：void
  * 备    注：null
  *    
  *
******************************************************************************/

void reset_i(PIDInfo_t* pid)
{
    pid->integ = 0;
}

/******************************************************************************
  * 函数名称：UpdatePID
  * 函数描述：计算PID相关值
  * 输    入：PIDInfo_t* pid：要计算的PID结构体指针
              float dt：单位运行时间
  * 输    出：void
  * 返    回：void
  * 备    注：null
  *    
  *
******************************************************************************/
void UpdatePID(PIDInfo_t* pid, const float dt)
{
    float deriv;
    
    pid->Err = pid->desired - pid->measured ; //误差=期望-测量值

    
    if(pid->Err < pid->DeathArea && pid->Err > -pid->DeathArea)//如果误差在死区则误差为0
    {
        pid->Err = 0;
    }
    
    if(pid->Err_LimitHigh != 0 && pid->Err_LimitLow != 0)//给误差限幅
    {
        pid->Err = LIMIT(pid->Err, pid->Err_LimitLow, pid->Err_LimitHigh);
    }
    
    pid->integ += pid->Err * dt;  //pid积分值为误差*dt 
    
    if(pid->IntegLimitHigh != 0 && pid->IntegLimitLow != 0)//积分限幅
    {
        pid->integ = LIMIT(pid->integ, pid->IntegLimitLow, pid->IntegLimitHigh);
    }
    
    //deriv = (pid->Err - pid->prevError)/dt;  
    deriv = -(pid->measured - pid->prevError)/dt;//微分值为测量值-先前的误差值
		
    pid->out = pid->kp * pid->Err + pid->ki * pid->integ + pid->kd * deriv;//PID输出
    
    if(pid->OutLimitHigh != 0 && pid->OutLimitLow != 0)//输出限幅
    {
        pid->out = LIMIT(pid->out, pid->OutLimitLow, pid->OutLimitHigh);
    }
    
    pid->prevError = pid->measured;//pid->Err;  微分先行（变式）用法
}

/******************************************************************************
  * 函数名称：ClacCascadePID
  * 函数描述：计算串级PID
  * 输    入：PIDInfo_t* pidRate：PID速度环
              PIDInfo_t* pidAngE：PID角度环
              const float dt：单位运行时间
  * 输    出：void
  * 返    回：void
  * 备    注：null    
  *    
  *
******************************************************************************/
void ClacCascadePID(PIDInfo_t* pidRate, PIDInfo_t* pidAngE, const float dt)  //串级PID
{     
    UpdatePID(pidAngE, dt);    //先计算外环
    pidRate->desired = pidAngE->out;
    UpdatePID(pidRate, dt);   
}

/******************* (C) 版权所有 2018 北京中科浩电科技有限公司 *******************/
//1.pid.c pid.h定义 2.user_task.c out与Userdata_send_OPENMV_UP()的desire从openmv那里面来的 
//3.AnoAno_Scheduler.c进行upPID与PID_measured的赋值xy轴的速度是从"Drv_AnoOf.h"文件 Of2_Fix_Y = ano_of.of2_dy_fix;
//高度速度是从 "ANO_DT_LX.h"文件SPEED_Z来的
void PID_Init(void)
{
	//上视openmv
    SpeedPID_X.kp   = 0.3f;
    SpeedPID_X.ki   = 0.1f;					
    SpeedPID_X.kd   = 0.02f;
    SpeedPID_X.IntegLimitHigh = 10;
    SpeedPID_X.IntegLimitLow = -10;
		SpeedPID_X.Err_LimitHigh=100;
		SpeedPID_X.Err_LimitLow=-100;
    SpeedPID_X.OutLimitHigh = 20;
		SpeedPID_X.OutLimitLow = -20;
		SpeedPID_X.prevError=0;
		SpeedPID_X.DeathArea=2;
	
		SpeedPID_Z.kp   = 0.3f;
    SpeedPID_Z.ki   = 0.1f;					
    SpeedPID_Z.kd   = 0.02f;
    SpeedPID_Z.IntegLimitHigh = 10;
    SpeedPID_Z.IntegLimitLow = -10;
		SpeedPID_Z.Err_LimitHigh=100;
		SpeedPID_Z.Err_LimitLow=-100;
    SpeedPID_Z.OutLimitHigh = 20;
		SpeedPID_Z.OutLimitLow = -20;
		SpeedPID_Z.prevError=0;
		SpeedPID_Z.DeathArea=2;

//下视openmv
		SpeedPID_X_Down.kp   = 0.3f;
    SpeedPID_X_Down.ki   = 0.1f;					
    SpeedPID_X_Down.kd   = 0.02f;
    SpeedPID_X_Down.IntegLimitHigh = 10;
    SpeedPID_X_Down.IntegLimitLow = -10;
		SpeedPID_X_Down.Err_LimitHigh=100;
		SpeedPID_X_Down.Err_LimitLow=-100;
    SpeedPID_X_Down.OutLimitHigh = 20;
		SpeedPID_X_Down.OutLimitLow = -20;
		SpeedPID_X_Down.prevError=0;
		SpeedPID_X_Down.DeathArea=2;
	
		SpeedPID_Y_Down.kp   = 0.3f;
    SpeedPID_Y_Down.ki   = 0.1f;					
    SpeedPID_Y_Down.kd   = 0.02f;
    SpeedPID_Y_Down.IntegLimitHigh = 10;
    SpeedPID_Y_Down.IntegLimitLow = -10;
		SpeedPID_Y_Down.Err_LimitHigh=100;
		SpeedPID_Y_Down.Err_LimitLow=-100;
    SpeedPID_Y_Down.OutLimitHigh = 20;
		SpeedPID_Y_Down.OutLimitLow = -20;
		SpeedPID_Y_Down.prevError=0;
		SpeedPID_Y_Down.DeathArea=2;
		

		SpeedPID_ALL.kp   =0.3f;
    SpeedPID_ALL.ki   =0.1f;				
    SpeedPID_ALL.kd   =0.02f;
    SpeedPID_ALL.IntegLimitHigh = 10;
    SpeedPID_ALL.IntegLimitLow = -10;
		SpeedPID_ALL.Err_LimitHigh=100;
		SpeedPID_ALL.Err_LimitLow=-100;
    SpeedPID_ALL.OutLimitHigh = 20;
		SpeedPID_ALL.OutLimitLow = -20;
		SpeedPID_ALL.prevError=0;
		SpeedPID_ALL.DeathArea=2;
}
