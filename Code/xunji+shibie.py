import sensor, image,lcd,time
import utime
from machine import UART #串口库函数
from fpioa_manager import fm # GPIO重定向函数
import KPU as kpu
import gc, sys

fm.register(7, fm.fpioa.UART2_TX, force=True)
fm.register(6, fm.fpioa.UART2_RX, force=True)
uart_B = UART(UART.UART2, 115200 ,8, 1, 0, timeout=200, read_buf_len=100)
#red_potion = (20, 53, 8, 57, 2, 53)     #  红色
red_potion=(13, 48, 13, 65, 5, 47)
roi1            = [0,100,320,24]       #巡线敏感区
roi2            = [0,180,320,8]        #关键点敏感区
expectedValue   = 160                  #巡线位置期望
Flag            = 1                   #用于关键点标志
flag_cnt        = 0
shuzi_cnt       = 0

labels = ['8', '7', '5', '1', '6', '3', '2', '4']
anchors = [1.62, 1.66, 1.88, 2.22, 2.12, 2.62, 2.03, 2.59, 2.06, 2.72]
label_mapping = {'8': 8, '7': 7, '5': 5, '1': 1, '6': 6, '3': 3, '2': 2, '4': 4}
lcd.init()
sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA) # 320x240
sensor.set_hmirror(0)  # 水平翻转图像
sensor.run(1)



#######################
def shibie(anchors, labels = None, model_addr="/sd/m.kmodel", sensor_window=(224, 224), lcd_rotation=0, sensor_hmirror=False, sensor_vflip=False):
    global Flag  # 声明 Flag 为全局变量
    sensor.set_windowing(sensor_window)
    sensor.run(1)

    lcd.init(type=1)
    lcd.rotation(lcd_rotation)
    lcd.clear(lcd.WHITE)

    try:
        task = None
        task = kpu.load(model_addr)
        kpu.init_yolo2(task, 0.65, 0.3, 5, anchors) # threshold:[0,1], nms_value: [0, 1]
        while(True):
            img = sensor.snapshot()
            t = time.ticks_ms()
            objects = kpu.run_yolo2(task, img)
            t = time.ticks_ms() - t
            if objects:
                for obj in objects:
                    pos = obj.rect()
                    img.draw_rectangle(pos)
                    img.draw_string(pos[0], pos[1], "%s : %.2f" %(labels[obj.classid()], obj.value()), scale=2, color=(255, 0, 0))
                    uart_B.write("A1%01d%03dD" % (label_mapping[labels[obj.classid()]], int(pos[0])))
            lcd.display(img)
            time.sleep(0.001)
            data = uart_B.readline()
            if data is not None and data.strip() == b"END":
                Flag=3
                kpu.deinit(task)
                return
            if data is not None and data.strip() == b"XUN":
                Flag=0
                kpu.deinit(task)
                return
    except Exception as e:
        raise e
    finally:
        if not task is None:
            kpu.deinit(task)

#######################
def xunji():
    global Flag  # 声明 Flag 为全局变量
    X=0
    Y=0
    flag_cnt=0
    while True:
        img=sensor.snapshot()
        statistics1 = img.find_blobs([red_potion],roi=roi1,area_threshold=200,merge=True)
        time.sleep(0.001)
        statistics2 = img.find_blobs([red_potion],roi=roi2,area_threshold=120,merge=True,margin=120)
        if statistics1:
            for b in statistics1:
                X=X+b[5]
                tmp=img.draw_rectangle(b[0:4])
                tmp=img.draw_cross(b[5], b[6])
                Y=Y+1
                if b[2] >100:
                       Flag = 2#识别到十字路口
                       return
        if statistics2:
            for b in statistics2:
                X=X+b[5]
                tmp=img.draw_rectangle(b[0:4])
                tmp=img.draw_cross(b[5], b[6])
                Y=Y+1
        if Y>0:
                X=X/Y
                uart_B.write("H%03dD"%(X))#
                flag_ting=0
        elif Y==0:
            flag_cnt=flag_cnt+1
            if flag_cnt>3:
                uart_B.write(b"H000D")#停止

        X=0
        Y=0
        lcd.display(img)



########################
while True:
    if   Flag == 0 :
            sensor.set_windowing((320,240))
            xunji()
    elif Flag == 1 :
            shibie(anchors = anchors, labels=labels, model_addr="/sd/model-66391.kmodel")
    elif Flag == 2 :
        uart_B.write(b"H999D")
        time.sleep(0.01)
        data = uart_B.readline()
        if data is not None and data.strip() == b"XUN":
           Flag=0
        if data is not None and data.strip() == b"SHU":
           Flag=1
    elif Flag == 3 :
         time.sleep(0.01)
         data = uart_B.readline()
         if data is not None and data.strip() == b"XUN":
            Flag=0
         if data is not None and data.strip() == b"SHU":
            Flag=1
