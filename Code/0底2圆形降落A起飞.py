# Untitled - By: Thinkpad - 周四 11月 4 2021

import sensor,time,pyb,math,sys,image
from pyb import Pin, Timer, LED, UART
from image import SEARCH_EX, SEARCH_DS
sensor.reset()
sensor.set_pixformat(sensor.RGB565) # 灰度更快RGB565
sensor.set_framesize(sensor.QQVGA)#-2Q
sensor.set_hmirror(True)
sensor.set_vflip(True)
sensor.skip_frames(time = 2000)

sensor.set_contrast(1)
sensor.set_gainceiling(16)
# Max resolution for template matching with SEARCH_EX is QQVGA

clock = time.clock()
black =(0, 23, -34, 32, -8, 14)   #(44, 255)

ROI=(45,20,70,100)


#xy平面误差数据
err_x = 0
err_y = 0

#发送数据
uart_buf1 = bytearray([0xAA,0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])

#串口三配置
uart = UART(3,500000)
uart.init(500000, bits=8, parity=None, stop=1)

def uartsend(timer):
    uart.write(uart_buf1)
    print(uart_buf1)

tim = Timer(2, freq=20)
tim.callback(uartsend)

atan_A = 0
while(True):
    clock.tick()
    img = sensor.snapshot()
    STA = 0x00
    STA2 = 0x00
    cx1 = 0
    cy1 = 0
    cx2=0
    cy2=0
    fx = 80
    fy = 60
    for c in img.find_circles(threshold = 4500, x_margin = 10, y_margin = 10, r_margin = 10,r_min = 2, r_max = 100, r_step = 2):
        img.draw_circle(c.x(),c.y(),c.r(), color = (255, 0, 0))
        cx = c.x()-c.r()
        cx1 = c.x()
        cy = c.y()-c.r()
        cy1 = c.y()
        cr = 2*c.r()
        cx2=c.x()
        cy2=c.y()
        print("x:",c.x(),"y:",c.y())
        roi1 = (cx,cy,cr,cr)
        blobs = img.find_blobs([black], roi=roi1, x_stride=5, y_stride=5,area_threshold=20)
        if blobs:
            most_pixels = 0
            largest_blob = 0
            STA = 0xFF#跟随
            #print("cir",cx1,cy1)
            for i in range(len(blobs)):
                if blobs[i].pixels() > most_pixels:
                    most_pixels = blobs[i].pixels()
                    largest_blob = i
            img.draw_cross(blobs[largest_blob].cx(),blobs[largest_blob].cy())
            img.draw_rectangle(blobs[largest_blob].rect())
        else:
            STA = 0x00
            cx2 = 80
            cy2 = 60
    print(STA,cx2,cy2)
    uart_buf1 = bytearray([0xAA,0xFF, 0x00, STA, cx2,cy2, 0x00,0x00])
    lens = len(uart_buf1)#数据包大小
    uart_buf1[2] = lens-5;#有效数据个数
    i = 0
    sum = 0
    sum1 = 0
    #和校验
    while i<(lens-2):
        sum = sum + uart_buf1[i]
        sum1 = sum1 + sum
        i = i+1
    uart_buf1[lens-2] = sum;
    uart_buf1[lens-1] = sum1;


