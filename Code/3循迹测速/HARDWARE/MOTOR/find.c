#include "find.h"
#include "bsp_timer.h"
#include "bsp_sys.h"
#include "bsp_usart.h"
#include "bsp_usart2.h"
#include "delay.h"

unsigned int Lor;    //停车标志位，Lor=3停车
unsigned int Num;
unsigned int Lor2;   //位置标志位，Lor2=0直走、Lor2=1左转、Lor2=2右转
unsigned int Num2;   //数字标志位，表示k210识别到的数字



//循迹IO初始化
//寻迹传感器从左到右依次O1 O2 O3 O4 05
//硬件连接 O1-PA4，O2-PA5，O3-PA6，O4-PA7，
//要初始化为输入模式
void Find_IO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	//开启GPIO时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8| GPIO_Pin_11;//选择IO端口
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;//模式配置为上拉输入
	GPIO_Init(GPIOA, &GPIO_InitStructure);//根据GPIO_InitStructure中指定的参数初始化外设GPIOD寄存器
} 

//循迹模块 黑线输出高电平1  白线低电平0
//小车最左端是O1----最右端是O5
//循迹路面：白色路面黑色引导线，即寻黑线。
//黑线传感器输出1，白线输出0

void weitiao(void)
{
	//发车点及直走的情况
				if((Find_O1 == 0)&&(Find_O2 == 0)&&(Find_O3 == 1)&&(Find_O4 == 0)&&(Find_O5 == 0))
				{
						Car_Go();
				}
				if((Find_O1 == 0)&&(Find_O2 == 0)&&(Find_O3 == 0)&&(Find_O4 == 0)&&(Find_O5 == 0) &&(Find_O6 == 0))//放上了药品
				{
						Car_Go();
				}
				if((Find_O1 == 0)&&(Find_O2 == 0)&&(Find_O3 == 0)&&(Find_O4 == 0)&&(Find_O5 == 0) &&(Find_O6 == 1))//还没放药品
				{
						Car_Stop();
				}
				 
				//右转的情况
				if((Find_O2 == 0)&&(Find_O3 == 0)&&(Find_O4 == 1))
				{
						Car_Right();
						//Delay_ms(50);
				}
				if((Find_O2 == 0)&&(Find_O3 == 1)&&(Find_O4 == 1))
				{
						Car_Right();
						//Delay_ms(150);
				}
				
				//左转的情况
				if((Find_O2 == 1)&&(Find_O3 == 0)&&(Find_O4 == 0))
				{
						Car_Left();
						//Delay_ms(150);
				}
				if((Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 0))
				{
						Car_Left();
						//Delay_ms(150);
				}
}

void zhijiao_left(void)
{
				//十字路口左转
				if(((Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 0)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1) &&(Find_O6 == 0)))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Left90();	
						Delay_ms(480);
				}
}

void zhijiao_right(void)
{
				//十字路口右转
				if(((Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 0)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1) &&(Find_O6 == 0)))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Right90();	
						Delay_ms(480);
				}
}

void stop_diaotou(void)
{
				while(Lor == 3)//识别到停车的虚线
				{					
						//停车及掉头
						if((Find_O1 == 1)|| (Find_O5 == 1))//到达药房
						{ 
								Car_Stop();
								Delay_ms(10);
						}
						if((Find_O1 == 0)&&(Find_O2 == 0)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 0))
						{
								Car_Right();
								Delay_ms(100);
						}
						if((Find_O1 == 0)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 0)&&(Find_O5 == 0))
						{
								Car_Left();
								Delay_ms(100);
						}
						if(Find_O6 == 1)//停车后药品拿下
						{
								Delay_ms(500);										
								Car_Right90();	
								Delay_ms(1000);
								Lor=0;
								Car_Go();
								Delay_ms(100);
						}
				}
//				//掉头后直线的全白情况
//				if((Find_O1 == 0)&&(Find_O2 == 0)&&(Find_O3 == 0)&&(Find_O4 == 0)&&(Find_O5 == 0) &&(Find_O6 == 1)&&(FindCount == 1))
//				{
//						Car_Go();
//				}
}



/*-------------------------------------------近端病房1---------------------------------------------*/
void find_1(void)
{
				weitiao();
				stop_diaotou();
				//十字路口左转
				if(((Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 0)&&(FindCount[0] == 0)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1) &&(Find_O6 == 0)&&(FindCount[0] == 0)))
				{
						FindCount[0]++;
						zhijiao_left();
				}
								
				//十字路口右转
				if(((Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 1)&&(FindCount[0] == 1)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1) &&(Find_O6 == 1)&&(FindCount[0] == 1)))
				{
						FindCount[0]=0;
						zhijiao_right();
				}
}
/*-------------------------------------------近端病房2---------------------------------------------*/
void find_2(void)
{
				weitiao();
				stop_diaotou();
				//十字路口右转
				if(((Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 0)&&(FindCount[0] == 0)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1) &&(Find_O6 == 0)&&(FindCount[0] == 0)))
				{
						FindCount[0]++;
						zhijiao_right();
				}			
				//十字路口左转
				if(((Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 1)&&(FindCount[0] == 1)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1) &&(Find_O6 == 1)&&(FindCount[0] == 1)))
				{
						FindCount[0]=0;
						zhijiao_left();
				}		
}
/*----------------------------------------中远端病房3,4,5,6,7,8------------------------------------------*/
void find_else(void)
{
				u8 i=0;
				weitiao();
				stop_diaotou();
				/*------------------------------------前进中的情况--------------------------------------*/
				//不是所给数字的十字路口，直走
				if(((Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 0)&&(Lor2 == 0)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1) &&(Find_O6 == 0)&&(Lor2 == 0)))
				{							    
						Car_Go();
						FindCount[i] = Lor2;
						Delay_ms(100);
					  i++;
				}
				
				//药房判定在左边，十字路口左转
				if(((Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1)&&(Lor2 == 1)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Lor2 == 1)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1)&&(Lor2 == 1)))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Left90();	
						Delay_ms(480);
					
						FindCount[i] = Lor2;
						Delay_ms(100);
					  i++;
				}
								
				//药房判定在右边，十字路口右转
				if((Find_O2 == 1)&&(Find_O4 == 1) &&(Find_O6 == 0)&&(Lor == 2))
				{
						//Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Right90();
						Delay_ms(480);
					
						FindCount[i] = Lor;
						Delay_ms(100);
					  i++;
				}
				
				//k210没识别到，默认左转
				if(((Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 0)&&(Lor2 == 0)&&(i == 2)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1) &&(Find_O6 == 0)&&(Lor2 == 0)&&(i == 2)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 0)&&(Lor2 == 0)&&(i == 2)))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Left90();	
						Delay_ms(480);
					
						FindCount[i] = Lor2;
						Delay_ms(100);
					  i++;
				}
				
				/*------------------------------------返程中的情况--------------------------------------*/
				//返程中的所有直走情况
				if(((Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 1)&&(i == 0)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1) &&(Find_O6 == 1)&&(i == 0)))
				{							    
						Car_Go();
						Delay_ms(100);
				}
				
				//在3药房返程
				if(((Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 1)&&(Lor == 2)&&(i == 2)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1) &&(Find_O6 == 1)&&(Lor == 2)&&(i == 2)))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Left90();	
						Delay_ms(480);
						i=0;
				}
				//在4药房返程
				if(((Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 1)&&(Lor2 == 1)&&(i == 2)) || ((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 1) &&(Find_O6 == 1)&&(Lor2 == 1)&&(i == 2)))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Right90();	
						Delay_ms(480);
						i=0;
				}
				
				//在5药房返程第一段
				if((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 0)&&(Find_O5 == 0) &&(Find_O6 == 1)&&(FindCount[3] == 2)&&(i == 3))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Left90();	
						Delay_ms(480);
					
						Car_Go();
						Delay_ms(100);
				}
				//在5药房返程第二段
				if((Find_O1 == 0)&&(Find_O2 == 0)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 1)&&(FindCount[2] == 1)&&(i == 3))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Right90();	
						Delay_ms(480);
					
						Car_Go();
						Delay_ms(100);
						i=0;
				}
				
				//在6药房返程第一段
				if((Find_O1 == 0)&&(Find_O2 == 0)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 1)&&(FindCount[3] == 1)&&(i == 3))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Right90();	
						Delay_ms(480);
					
						Car_Go();
						Delay_ms(100);
				}
				//在6药房返程第二段
				if((Find_O1 == 0)&&(Find_O2 == 0)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 1)&&(FindCount[2] == 1)&&(i == 3))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Right90();	
						Delay_ms(480);
					
						Car_Go();
						Delay_ms(100);
						i=0;
				}
				
				//在7药房返程第一段
				if((Find_O1 == 0)&&(Find_O2 == 0)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 1) &&(Find_O6 == 1)&&(FindCount[3] == 1)&&(i == 3))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Right90();	
						Delay_ms(480);
					
						Car_Go();
						Delay_ms(100);
				}
				//在7药房返程第二段
				if((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 0)&&(Find_O5 == 0) &&(Find_O6 == 1)&&(FindCount[2] == 2)&&(i == 3))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Left90();	
						Delay_ms(480);
					
						Car_Go();
						Delay_ms(100);
						i=0;
				}
				
				//在8药房返程第一段
				if((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 0)&&(Find_O5 == 0) &&(Find_O6 == 1)&&(FindCount[3] == 2)&&(i == 3))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Left90();	
						Delay_ms(480);
					
						Car_Go();
						Delay_ms(100);
				}
				//在8药房返程第二段
				if((Find_O1 == 1)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 0)&&(Find_O5 == 0) &&(Find_O6 == 1)&&(FindCount[2] == 2)&&(i == 3))
				{
						Delay_ms(35);							    
						Car_Stop();
						Delay_ms(800);
						Car_Left90();	
						Delay_ms(480);
					
						Car_Go();
						Delay_ms(100);
						i=0;
				}
				
				/*------------------------------------停车中的情况--------------------------------------*/
				while(Lor == 3)//识别到停车的虚线
				{					
						//停车及掉头
						if((Find_O1 == 1) || (Find_O5 == 1))//到达药房
						{ 
								Car_Stop();
								Delay_ms(10);
						}
						if((Find_O1 == 0)&&(Find_O2 == 0)&&(Find_O3 == 1)&&(Find_O4 == 1)&&(Find_O5 == 0))
						{
								Car_Right();
								Delay_ms(100);
						}
						if((Find_O1 == 0)&&(Find_O2 == 1)&&(Find_O3 == 1)&&(Find_O4 == 0)&&(Find_O5 == 0))
						{
								Car_Left();
								Delay_ms(100);
						}
						if(Find_O6 == 1)//停车后药品拿下
						{
								Delay_ms(500);										
								Car_Right90();	
								Delay_ms(950);
								Lor=0;
								Car_Go();
								Delay_ms(100);
						}
				}	
}

void Find(void)
{
	switch(Num2)
	{
		case 1:	find_1();break;
		case 2: find_2();break;
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8: find_else();break;
		default: Car_Stop();
	}
}
