#ifndef __USART2_H
#define	__USART2_H

#include "stm32f10x.h"
#include <stdio.h>

extern unsigned int Lor2;
extern unsigned int Num2;
//extern u16 RxBuffer2[4];
//extern u8 RxFlag2;

void USART2_Init(u32 bound);
void NVIC_Configuration(void);
int fputc2(int ch, FILE *f);
int fgetc2(FILE *f);
void USART2_Send(unsigned char *tx_buf, int len);
void USART2_Receive(unsigned char *rx_buf, int len);
void USART2_IRQHandler(void);

#endif /* __USART2_H */
