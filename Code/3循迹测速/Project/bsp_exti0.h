#ifndef __BSP_EXTI0_H
#define	__BSP_EXTI0_H

#include "stm32f10x.h"
#include <stdio.h>


void EXTI0_IRQHandler();


#endif /* __BSP_EXTI0_H */