/*
********************************************************************************************************
文件名：bsp_usart2.c
功  能：串口函数
备  注：
淘  宝：https://shop60670850.taobao.com
作  者：lilianhe
日  期: 2017-2-8
*********************************************************************************************************
*/

#include "bsp_usart2.h"
#include <stdio.h>

volatile unsigned char *rx_address2;
volatile unsigned int rx_count2;
volatile unsigned int rx_length2;
u16 RxBuffer2[4]={0,0,0,0};
u8 RxFlag2 = 0;


/*
********************************************************************************************************
函数名称：void USART2_Init(u32 bound)
函数功能：串口2初始化函数
硬件连接：PA2----TXD，PA3----RXD
备    注：bound为设置波特率
日    期:  2017-2-8
*********************************************************************************************************
*/
void USART2_Init(u32 bound)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	/* config USART1 clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2 | RCC_APB2Periph_GPIOA, ENABLE);
	
	/* USART1 GPIO config */
	/* Configure USART1 Tx (PA.02) as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);  
  
	/* Configure USART1 Rx (PA.03) as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/* USART1 mode config */
	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART2, &USART_InitStructure);
	
	/* 使能串口1接收中断 */
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	
	USART_Cmd(USART2, ENABLE);
	
	//NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_InitStructure.NVIC_IRQChannel=USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=3;
	NVIC_Init(&NVIC_InitStructure);

}



/// 重定向c库函数printf到USART2
int fputc2(int ch, FILE *f)
{
		/* 发送一个字节数据到USART2 */
		USART_SendData(USART2, (uint8_t) ch);
		
		/* 等待发送完毕 */
		while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);		
	
		return (ch);
}

/// 重定向c库函数scanf到USART1
int fgetc2(FILE *f)
{
		/* 等待串口1输入数据 */
		while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE) == RESET);

		return (int)USART_ReceiveData(USART2);
}
/*
********************************************************************************************************
函数名称：void USART2_Send(unsigned char *tx_buf, int len)
函数功能：串口1发送函数
硬件连接：PA2----TXD，PA3----RXD
备    注：
日    期:  2017-2-8
*********************************************************************************************************
*/
void USART2_Send(unsigned char *tx_buf, int len)
{
		USART_ClearFlag(USART2, USART_FLAG_TC);
		USART_ClearITPendingBit(USART2, USART_FLAG_TXE);
		while(len--)
		{
			USART_SendData(USART2, *tx_buf);
			while(USART_GetFlagStatus(USART2, USART_FLAG_TC) != 1);
			USART_ClearFlag(USART2, USART_FLAG_TC);
			USART_ClearITPendingBit(USART2, USART_FLAG_TXE);
			tx_buf++;
		}
}

/*
********************************************************************************************************
函数名称：void USART2_Receive(unsigned char *rx_buf, int len)
函数功能：串口1接收函数
硬件连接：PA2----TXD，PA3----RXD
备    注：
日    期:  2017-2-8
*********************************************************************************************************
*/
void USART2_Receive(unsigned char *rx_buf, int len)
{
	rx_count2 = 0;
	rx_length2 = len;
	rx_address2 = rx_buf;
}
/*
********************************************************************************************************
函数名称：void USART2_IRQHandler(void)
函数功能：串口1中断服务函数
硬件连接：PA2----TXD，PA3----RXD
备    注：
日    期:  2017-2-8
*********************************************************************************************************
*/
//void USART2_IRQHandler(void)
//{
//	unsigned char Res;
//	//u8 Res;
//	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)  //接收中断(接收到的数据必须是0x0d 0x0a结尾)
//	{
//		
//		Res = USART2_ReceiveData(USART2);
//	  //printf("[USART2_IRQHandler],Rec_data = %x\r\n", Res);

//		if(rx_length > rx_count)
//		{
//			*rx_address = Res;
//			rx_address++;
//			rx_count++;	
//		}
//			USART_ClearITPendingBit(USART2, USART_IT_RXNE); 
//   } 	
//}



//USART1 全局中断服务函数，用于和k210之间进行收发数据的通信
void USART2_IRQHandler(void)			 
{
		u8 com_data2; 
		u8 i;
		static u8 RxCounter2=0;
		static u8 RxState2 = 0;
//		static u16 RxBuffer2[4]={0};
//		static u8 RxFlag2 = 0;
		
		if( USART_GetITStatus(USART2,USART_IT_RXNE)!=RESET)  //接收中断  
		{
				USART_ClearITPendingBit(USART2,USART_IT_RXNE);   //清除中断标志
				com_data2 = USART_ReceiveData(USART2);
			
				if(RxState2==0&&com_data2==0x2C)  //0x2c帧头
				{
					RxBuffer2[RxCounter2++]=com_data2;
					RxState2=1;
				}
				else if(RxState2==1&&com_data2==0x12)  //0x12帧头
				{
					RxBuffer2[RxCounter2++]=com_data2;
					RxState2=2;
				}
				else if(RxState2==2)
				{
					RxBuffer2[RxCounter2++]=com_data2;

					if(RxCounter2>=4||com_data2 == 0x5B)       //RxBuffer2接受满了,接收数据结束
					{
						Lor2=RxBuffer2[RxCounter2-1];//Lor位
						Num2=RxBuffer2[RxCounter2-2];//Num位
						RxFlag2=1;
						RxState2=3;
					}
				}
				else if(RxState2==3)		//检测是否接受到结束标志
				{
						if(RxBuffer2[RxCounter2] == 0x5B)  //0x5B帧头
						{
									USART_ITConfig(USART2,USART_IT_RXNE,DISABLE);//关闭DTSABLE中断
									if(RxFlag2)
									{
									//OLED_ShowNum(48,7,Cy,1,12);
									}
									RxFlag2 = 0;
									RxCounter2 = 0;
									USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);
									RxState2 = 0;
						}
						else   //接收错误
						{
									RxCounter2=0;
									for(i=0;i<5;i++)
									{
											RxBuffer2[i]=0x00;      //将存放数据数组清零
									}
									RxState2 = 0;
						}
				} 
				else   //接收异常
				{
						RxCounter2=0;
						for(i=0;i<5;i++)
						{
								RxBuffer2[i]=0x00;      //将存放数据数组清零
						}
						RxState2 = 0;
				}
		}
}
