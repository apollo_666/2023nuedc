/******************** (C) COPYRIGHT 2017 ANO Tech ********************************
 * 作者    ：匿名科创
 * 官网    ：www.anotc.com
 * 淘宝    ：anotc.taobao.com
 * 技术Q群 ：190169595
 * 描述    ：任务调度
**********************************************************************************/
#include "Ano_Scheduler.h"
#include "User_Task.h"
#include "Drv_AnoOf.h"
#include "ANO_LX.h"
#include "pid.h"
#include "ANO_DT_LX.h"
#include "Ano_Math.h"
float start_yaw;
float offset_Angle;
u8 mission_step;

s16 Of2_Fix_X,Of2_Fix_Y,JF_X,JF_Y;
//////////////////////////////////////////////////////////////////////
//用户程序调度器
//////////////////////////////////////////////////////////////////////
static s16 start_error_X,start_error_Y;

extern s16 cal_X,cal_Y;
extern s16 cal_X_2,cal_Y_2;
extern s16 cal_X_3,cal_Y_3;
extern s16 check_point_1;
extern s16 check_point_3;
extern u8 yaw_target_cnt;

static void Loop_1000Hz(void) //1ms执行一次
{
	//////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////
}

static void Loop_500Hz(void) //2ms执行一次
{
	//////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////
}

static void Loop_200Hz(void) //5ms执行一次
{
	//////////////////////////////////////////////////////////////////////
	//数据读取
	if (mission_step == 4 || mission_step == 41 || mission_step == 45||mission_step == 51)
	{
		start_error_X = JF_X;
		start_error_Y = JF_Y;
		
		start_yaw = fc_att.st_data.yaw;		
	}
	
	//////////////////////////////////////////////////////////////////////
}

static void Loop_100Hz(void) //10ms执行一次
{
	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////
}

static void Loop_50Hz(void) //20ms执行一次
{
	//////////////////////////////////////////////////////////////////////
	UserTask_OneKeyCmd();

	cal_X = JF_X-start_error_X*0.85;
	cal_Y = JF_Y-start_error_Y*0.9;
	
	cal_X_2 = JF_X-start_error_X;
	cal_Y_2 = JF_Y-start_error_Y;
	



	
	frame_send_8(mission_step,0xF1); 
//	frame_send_8(yaw_target_cnt,0xF8);
	Userdata_send_OPENMV();    //上传OPMV下视数据
	Userdata_send_OPENMV_UP(); //上传OPMV前视数据
	
//	SpeedPID_X.measured=Of2_Fix_Y;
//	SpeedPID_Z.measured=SPEED_Z;
//	SpeedPID_X_Down.measured=Of2_Fix_Y;
//	SpeedPID_Y_Down.measured=Of2_Fix_X;
		SpeedPID_ALL.measured=my_sqrt(ABS(Of2_Fix_Y*Of2_Fix_Y+Of2_Fix_X*Of2_Fix_X));
//	
//	UpdatePID(&SpeedPID_X,20);
//	UpdatePID(&SpeedPID_Z,20);
//	UpdatePID(&SpeedPID_X_Down,20);
//	UpdatePID(&SpeedPID_Y_Down,20);	
	UpdatePID(&SpeedPID_ALL,20);	
	frame_send_16(Of2_Fix_Y,0xF7);
	frame_send_16(Of2_Fix_X,0xF8);
	frame_send_float(SpeedPID_ALL.measured,0xF9);
	frame_send_float(SpeedPID_ALL.desired,0xFA);
	frame_send_float(SpeedPID_ALL.out,0xF2);
	//////////////////////////////////////////////////////////////////////
}

static void Loop_20Hz(void) //50ms执行一次
{
	//////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////
}

static void Loop_2Hz(void) //500ms执行一次
{
	
}
//////////////////////////////////////////////////////////////////////
//调度器初始化
//////////////////////////////////////////////////////////////////////
//系统任务配置，创建不同执行频率的“线程”
static sched_task_t sched_tasks[] =
	{
		{Loop_1000Hz, 1000, 0, 0},
		{Loop_500Hz, 500, 0, 0},
		{Loop_200Hz, 200, 0, 0},
		{Loop_100Hz, 100, 0, 0},
		{Loop_50Hz, 50, 0, 0},
		{Loop_20Hz, 20, 0, 0},
		{Loop_2Hz, 2, 0, 0},
};
//根据数组长度，判断线程数量
#define TASK_NUM (sizeof(sched_tasks) / sizeof(sched_task_t))

void Scheduler_Setup(void)
{
	uint8_t index = 0;
	//初始化任务表
	for (index = 0; index < TASK_NUM; index++)
	{
		//计算每个任务的延时周期数
		sched_tasks[index].interval_ticks = TICK_PER_SECOND / sched_tasks[index].rate_hz;
		//最短周期为1，也就是1ms
		if (sched_tasks[index].interval_ticks < 1)
		{
			sched_tasks[index].interval_ticks = 1;
		}
	}
}
//这个函数放到main函数的while(1)中，不停判断是否有线程应该执行
void Scheduler_Run(void)
{
	uint8_t index = 0;
	//循环判断所有线程，是否应该执行

	for (index = 0; index < TASK_NUM; index++)
	{
		//获取系统当前时间，单位MS
		uint32_t tnow = GetSysRunTimeMs();
		//进行判断，如果当前时间减去上一次执行的时间，大于等于该线程的执行周期，则执行线程
		if (tnow - sched_tasks[index].last_run >= sched_tasks[index].interval_ticks)
		{

			//更新线程的执行时间，用于下一次判断
			sched_tasks[index].last_run = tnow;
			//执行线程函数，使用的是函数指针
			sched_tasks[index].task_func();
		}
	}
}

/******************* (C) COPYRIGHT 2014 ANO TECH *****END OF FILE************/
