#ifndef __MY_CAR_H
#define __MY_CAR_H

//==����
#include "SysConfig.h"
#include "My_Car.h"

typedef struct
{
	s16 car_data[100];
	
	u8 car_sta;
	
	s16 car_a;
	s16 car_b;
	
	s16 car_x;
	s16 car_y;
	
} car_st;

void F407_Send_car(s16 a,s16 b,s16 x,s16 y,u8 num);
void Car_GetOneByte(uint8_t data);
static void Car_DataAnl(uint8_t *data, uint8_t len1);

extern car_st my_car;

#endif

