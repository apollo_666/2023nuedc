#ifndef __MY_MOTOR_H__
#define __MY_MOTOR_H__

#include "stm32f10x.h"
/**
		 TIM4-CH4-PB9  ----  PWMA(�ҵ��)
		 TIM4-CH3-PB8  ----  PWMB��������
*/
#define My_Motor_GPIO    GPIOB

#define PWMA_Pin       GPIO_Pin_9
#define PWMB_Pin       GPIO_Pin_8


#define AIN1(a)    if (a) GPIO_SetBits(My_Motor_GPIO,AIN1_Pin);\
						 else   GPIO_ResetBits(My_Motor_GPIO,AIN1_Pin)
						 
#define AIN2(a)    if (a) GPIO_SetBits(My_Motor_GPIO,AIN2_Pin);\
						 else   GPIO_ResetBits(My_Motor_GPIO,AIN2_Pin)

#define BIN1(a)    if (a) GPIO_SetBits(My_Motor_GPIO,BIN1_Pin);\
						 else   GPIO_ResetBits(My_Motor_GPIO,BIN1_Pin)
						 
#define BIN2(a)    if (a) GPIO_SetBits(My_Motor_GPIO,BIN2_Pin);\
						 else   GPIO_ResetBits(My_Motor_GPIO,BIN2_Pin)
							 
void My_Motor_Init(void);
void My_Motor_PWM_Init(uint16_t arr, int psc);
void My_Motor_PWM_GPIO_Cinfig(void);
void My_Motor_PWM_Mode_Config(uint16_t arr, int psc);
void My_Motor_PWM_Out(uint16_t Moto_L,uint16_t Moto_R);

						 
#endif

