#include "My_Motor.h"

void My_Motor_Init(void)
{
	My_Motor_PWM_Init(7200,10);
}


//***************************PWM频率及占空比初始化***************************//
//=====初始化PWM 20KHZ 高频可以防止电机低频时的尖叫声
// ARR= 3599 时频率为20Khz
//PB0控制PWMA--left motor，PB1控制PWMB--right motor。STBY直接拉高
//arr：自动重装寄存器，psc分频系数
//PWM的频率 = 72MHz/ARR/PCS 例如  1K = 72M/7200/10 =  1K

void My_Motor_PWM_Init(uint16_t arr, int psc)
{
	My_Motor_PWM_GPIO_Cinfig();
	My_Motor_PWM_Mode_Config(arr,psc);
}

void My_Motor_PWM_GPIO_Cinfig(void)
{
	//配置pwm输出端口
	GPIO_InitTypeDef    GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	GPIO_InitStructure.GPIO_Pin = PWMA_Pin|PWMB_Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;//复用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB,&GPIO_InitStructure);
}

void My_Motor_PWM_Mode_Config(uint16_t arr, int psc)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitSructure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE);
	
	/*-----------------------TimeBaseInit-------------------*/
			
	TIM_TimeBaseStructure.TIM_Period = arr-1;                   //自动重新装载寄存器周期的值澹ㄥ计数值澹)
	TIM_TimeBaseStructure.TIM_Prescaler = psc-1;                  //时钟分频系数
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;     //对外部时钟进行采样的时钟分频
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; //向上计数
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM4,&TIM_TimeBaseStructure); 	//参数初始化
	
	TIM_ClearFlag(TIM4, TIM_FLAG_Update);
	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
	
	/*------------------------OCInit--------------------------*/
	//ch3
	TIM_OCInitSructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitSructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitSructure.TIM_Pulse = 0;
	TIM_OCInitSructure.TIM_OCPolarity = TIM_OCPolarity_High;//当定时器计数值小于CCR1_Val时为高电平
	
	TIM_OC3Init(TIM4,&TIM_OCInitSructure);
	TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);           //开始输出pwm
	
	TIM_OCInitSructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitSructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitSructure.TIM_Pulse = 0;
	TIM_OCInitSructure.TIM_OCPolarity = TIM_OCPolarity_High;//当定时器计数值小于CCR1_Val时为高电平
	
	TIM_OC4Init(TIM4,&TIM_OCInitSructure);
	TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);           //开始输出pwm
	
	TIM_ARRPreloadConfig(TIM4,ENABLE);
	TIM_Cmd(TIM4,ENABLE);
}




//***************************占空比调节***************************//
//占空比 = TIMx_CCRx / TIMx_ARR
//Moto_R：右轮电机，moto_L：左轮电机.   数值 0-3600

void My_Motor_PWM_Out(uint16_t Moto_L,uint16_t Moto_R)
{
	TIM_OCInitTypeDef TIM_OCInitSructure;
	
	TIM_OCInitSructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitSructure.TIM_OutputState = TIM_OutputState_Enable;
	//CH3 Left
	TIM_OCInitSructure.TIM_Pulse = Moto_L;
	TIM_OC3Init(TIM4,&TIM_OCInitSructure);
	TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);           //开始输出pwm
	//CH4 Right
	TIM_OCInitSructure.TIM_Pulse = Moto_R;
	TIM_OC4Init(TIM4,&TIM_OCInitSructure);
	TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);           //开始输出pwm
	
	TIM_ARRPreloadConfig(TIM4,ENABLE);
}









