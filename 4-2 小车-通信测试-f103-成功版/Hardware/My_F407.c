#include "My_F407.h"



//全局变量
static uint8_t _datatemp[50];
car_st my_car;


void Car_Send(s16 a,s16 b,s16 x,s16 y)
{
	u8 DateSendBuffer[100];
	u8 cnt=0;
	u8 sumcheck = 0;
	u8 addcheck = 0;
	
	DateSendBuffer[cnt++]=0xAA;
	DateSendBuffer[cnt++]=0xFF;
	DateSendBuffer[cnt++]=0XAA;
	DateSendBuffer[cnt++]=8;
	
	
	DateSendBuffer[cnt++]=BYTE0(a);
	DateSendBuffer[cnt++]=BYTE1(a);
	
	DateSendBuffer[cnt++]=BYTE0(b);
	DateSendBuffer[cnt++]=BYTE1(b);
	
	DateSendBuffer[cnt++]=BYTE0(x);
	DateSendBuffer[cnt++]=BYTE1(x);
	
	DateSendBuffer[cnt++]=BYTE0(y);
	DateSendBuffer[cnt++]=BYTE1(y);
	

	
	for(u8 i=0; i < DateSendBuffer[3]+4; i++)
	{
		sumcheck += DateSendBuffer[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
	}
	
	DateSendBuffer[cnt++]=sumcheck;
	DateSendBuffer[cnt++]=addcheck;
				
	DrvUart1SendBuf(DateSendBuffer,cnt);
}

//	串口2 OPENMV_UP
void Car_Receive(uint8_t data)
{
	static u8 _data_len = 0, _data_cnt = 0;
	static u8 rxstate = 0;

	if (rxstate == 0 && data == 0xAA)
	{
		rxstate = 1;
		_datatemp[0] = data;
	}
	else if (rxstate == 1 && data == 0XFF)   //OXFF
	{
		rxstate = 2;
		_datatemp[1] = data;
	}
	else if (rxstate == 2)
	{
		rxstate = 3;
		_datatemp[2] = data;
	}
	else if (rxstate == 3 && data < 250)
	{
		rxstate = 4;
		_datatemp[3] = data;
		_data_len = data;
		_data_cnt = 0;
	}
	else if (rxstate == 4 && _data_len > 0)
	{
		_data_len--;
		_datatemp[4 + _data_cnt++] = data;
		if (_data_len == 0)
			rxstate = 5;
	}
	else if (rxstate == 5)
	{
		rxstate = 6;
		_datatemp[4 + _data_cnt++] = data;
	}
	else if (rxstate == 6)
	{
		rxstate = 0;
		_datatemp[4 + _data_cnt] = data;
		Car_DataAnl(_datatemp, _data_cnt + 5); //
	}
	else
	{
		rxstate = 0;
	}	

}

//OPENMV_UP数据处理
static void Car_DataAnl(uint8_t *data, uint8_t len1)
{
	u8 check_sum5 = 0, check_sum6 = 0;
	if (*(data + 3) != (len1 - 6)) //判断数据长度是否正确
		return;
	for (u8 i = 0; i < len1 - 2; i++)
	{
		check_sum5 += *(data + i);
		check_sum6 += check_sum5;
	}
	if ((check_sum5 != *(data + len1 - 2)) || (check_sum6 != *(data + len1 - 1))) //判断sum校验
		return;
	//================================================================================	

	if (*(data + 2) == 0XAA) //OPENMV 信息
	{

		my_car.car_a = (s16)((*(data+5)<<8)|*(data+4));
		my_car.car_b = (s16)((*(data+7)<<8)|*(data+6));
		my_car.car_x = (s16)((*(data+9)<<8)|*(data+8));
		my_car.car_y = (s16)((*(data+11)<<8)|*(data+10));
			
		my_car.car_sta= 1;            //模式
	}
	if (*(data + 2) == 0XBB) //OPENMV 信息
	{

		
		my_car.car_sta= 2;            //模式
	}
	
}
