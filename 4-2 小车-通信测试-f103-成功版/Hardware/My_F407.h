#ifndef __MY_F407_H
#define __MY_F407_H

#define BYTE0(dwTemp) (*((char *)(&dwTemp)))
#define BYTE1(dwTemp) (*((char *)(&dwTemp) + 1))
#define BYTE2(dwTemp) (*((char *)(&dwTemp) + 2))
#define BYTE3(dwTemp) (*((char *)(&dwTemp) + 3))

//==����
#include "stm32f10x.h" 
#include "Serial.h"

typedef struct
{
	s16 car_data[100];
	
	u8 car_sta;
	
	s16 car_a;
	s16 car_b;
	
	s16 car_x;
	s16 car_y;
	
} car_st;

void Car_Send(s16 a,s16 b,s16 x,s16 y);
void Car_Receive(u8 data);
static void Car_DataAnl(uint8_t *data, uint8_t len1);

extern car_st my_car;

#endif

